# README #

SharpDC is a portable, lightweight, open-source .NET standard library for implementing IEEE 11073 SDC Family devices.

## Introduction ##

SharpDC implements essential parts of a communication protocol for point-of-care (PoC) medical devices. The protocol is specified by
the IEEE 11073 service-oriented device connectivity (SDC) standard documents.

The focus of the library is to provide an API for device implementation (SDC Provider), a client implementation (SDC Consumer) is currently under development and will be available soon.

### Features ###

* Targets Linux and Windows systems (truly portable .NET standard library).
* Easy-to-use API
* Small footprint
* Supports IPv4 and IPv6
* Supports TLS 1.3 encryption standard

### Design choices ###

* Async-await. This library uses the Task-based Asynchronous Pattern (see coding guidelines below) and every __await__ call of this library does not continue on the captured context. We're using timeouts and cancellation tokens.
* Efficiency. Lazy evaluation / streaming / LINQ have been used where possible.
* Nullable reference types. API calls will never return __null__. Instead you can rely on the documented exceptions.
* SOLID code. The code follows important design principles and provides a high level of abstraction and generics.
* Minimal dependencies. No external 3rd party packages have been referenced, besides necessary references as .NET standard 2.1 extensions:
      * Microsoft.Bcl.AsyncInterfaces,
      * Microsoft.Extensions.Logging and
      * System.Threading.Dataflow
* Thread-safety. Completely thread-safe API and Mdib interaction. Mdib objects returned to client code are always deep clones.
* Architectural mapping of Mdpws and Biceps distinctions which simplifies development of new transport layers.

### Intended use and regulatory affairs ###

This library is under development (public beta testing) and should currently be used for research and testing purposes only.

This library is not a medical product and never will be since it has no intended medical use besides connecting arbitrary medical devices.
We're however planning to provide excerpts of documentation for medical device software that will ease the approval process in the future.

### License ###

SharpDC is licensed under the GNU General Public License version 3 [(GPL3)](https://www.gnu.org/licenses/gpl-3.0.de.html) with an intended linking exception as additional permission to the GPL (version 3, section 7)
to be available once a stable version has been released.

### How to build ###

Install [.NET (Core) SDK](https://dotnet.microsoft.com/download) and a suitable IDE (Jetbrains Rider or Visual Studio). Then build the solution.
Building from the console is also possible, for this you can use the [dotnet build](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-build) command.

## Get started ##

Get started using SharpDC.

### Nuget ###

A ready-to-use nuget package will be available soon.

### Coding guidelines ###

SharpDC makes heavy use of the [Task-Based Asynchronous Pattern (TAP)](https://docs.microsoft.com/en-us/dotnet/standard/asynchronous-programming-patterns/task-based-asynchronous-pattern-tap), which means dependent code must use it as well.
If you're not familiar with async code, please read these [best practices](https://docs.microsoft.com/en-us/archive/msdn-magazine/2013/march/async-await-best-practices-in-asynchronous-programming). Pay special attention to the rule of __never blocking in async code__.
That means, always use __await__ in async methods and encapsulate blocking code with __await Task.Run(() => { ... })__. Additionally, prefer __ConfigureAwait(false)__ if you don't need to return to the UI context.

Another useful source for calling methods can be found [here](https://docs.microsoft.com/en-us/archive/msdn-magazine/2015/july/async-programming-brownfield-async-development). If needed, try to transform your existing code to async-await.
There are some techniques for synchronously calling async API methods, but we strongly advise against it.

### DPWS Explorer (WS4D) not compatible ###

Please note that due to a known bug in the DPWS Explorer, this tool cannot be used for testing since the XML responses are not parsed correctly in some cases.

## Examples ##

The major component of this library to interface from client code is the SdcProvider.
The SdcProvider is the device part representing values and functions of the actual medical device (e.g. sensor values or settings).

### Global log level and schema validation ###

The only global settings are schema validation and the log level. These settings are optional.

```csharp
// Set the log level to trace (useful for debugging)
SharpDc.Instance.Logger.Level = LogLevel.Trace;
// Enable schema validation for provider and consumer
SharpDc.Instance.SetSchemaValidationMode(SchemaValidationMode.All);
```

### SDC provider instantiation and configuration ###

The first step is to create and configure an instance of SdcProvider. A unique endpoint reference must be provided.

```csharp
// The usual program flow: create, configure & start the provider
await using var provider = new SdcProvider("SharpDcTestEpr");

// TODO: Provider & Mdib configuration (see following sections)

// Start provider
try
{
	await provider.Startup();
}
catch (Exception e)
{
	Console.WriteLine(e.StackTrace);
}
```

The Sdc provider's device binding settings determine the network configuration.

```csharp
provider.Settings.BindIp = MdpwsDeviceBindingSettings.Ipv4All;
var httpConfig = provider.Settings.ServerConfig;
httpConfig.Port = 6464;
httpConfig.UseHttps = false;
httpConfig.UseIpv6 = false;
```

Alternatively, instead of __Ipv4All__ an IP address of a network adapter can be provided, e.g. "192.168.100.1".
To use Ipv6, use MdpwsDeviceBindingSettings.Ipv6All (or provide a valid Ipv6 address) and set __UseIpv6__ to __true__.

To use encryption, you have to set __UseHttps__ to __true__  and provide some __SslAuthenticationOptions__ for the provider's http server and if needed, to the client.
The client's configuration is needed for sending event messages back to a consumer.

```csharp
var httpConfig = provider.Settings.ServerConfig;
httpConfig.UseHttps = true;
httpConfig.SslAuthenticationOptions = new SslAuthenticationOptions()
{
    Protocols = SslProtocols.Tls13
};
httpConfig.SslAuthenticationOptions.Certificates.Add(HttpTools.ImportCertificate("CertServer.p12", "Password"));
var clientConfig = provider.Settings.ClientConfig;
clientConfig.UseHttps = true;
clientConfig.SslAuthenticationOptions = new SslAuthenticationOptions()
{
    Protocols = SslProtocols.Tls13
};
```

Multiple protocols can be enabled as well.

```csharp
httpConfig.SslAuthenticationOptions = new SslAuthenticationOptions()
{
    Protocols = SslProtocols.Tls11 | SslProtocols.Tls12 | SslProtocols.Tls13
};
```

Depending on your target framework, the __SslProtocols.Tls13__ enum value might not be available. In that case use SharpDC's __SslProtocolsExtensions.Tls13__.

#### Prerequisites for TLS 1.3 ####

* On a Windows system, you might have to create some additional [registry entries](https://docs.microsoft.com/de-de/windows-server/security/tls/tls-registry-settings#tls-12). The name of the key must be __TLS 1.3__.
* On a Linux system OpenSSL version 1.1.1 (or later) must be installed.

The default setting for outgoing client requests to an eventing sink is to trust all certificates. You can add valid certificate(s) to the client config to redefine this behavior.

```csharp
clientConfig.SslAuthenticationOptions.Certificates.Add(HttpTools.ImportCertificate("CertClient.p12", "Password"));
```

### SDC provider Mdib ###

The Mdib contains all information which describes the medical device statically (descriptor), and dynamically (states containing mostly values, and operations).
An Mdib defining descriptors and (optional) states can be imported from an XML file that includes an GetMdibResponse. The actual Mds descriptor can be extracted and added to the provider.

```csharp
var xmlText = await File.ReadAllTextAsync("mdib.xml");  // Optionally, pass cancellation token here
var mds = GetMdibResponse.Deserialize(xmlText).Mdib.MdDescription.Mds.First();
provider.AddMds(mds);
```

Descriptors can also be created programmatically (see the __ExampleProvider__ project).

Every descriptor of an Mdib must have a unique descriptor handle and must have a corresponding state, referencing the descriptor handle. States must be created programmatically.
Suppose the Mds descriptor contains the handle 'mds', then a minimal state would have to look like the following.

```csharp
var mdsState = new MdsState
{
    DescriptorHandle = "mds", ActivationState = ComponentActivation.On, OperatingJurisdiction = null
};
provider.AddState(mdsState);
```

### Measurements ###

Measurements have a descriptor configured as measurement and a corresponding state for providing measurement values.
For example a pulse (BPM) descriptor of a pulse oximeter device can be programmatically initialized as shown next (an XML definition as mentioned before is also possible).

#### The measurement descriptor ####

```csharp
var nmdPulse = new NumericMetricDescriptor
{
    Handle = "HandleMetricPulse",
    MetricCategory = MetricCategory.Msrmt,  // The actual definition as measurement
    MetricAvailability = MetricAvailability.Cont,
    Type = SharpDcToolbox.GenerateCodedValue(2, 18458, "MDC_PULS_OXIM_PULS_RATE"),
    Unit = SharpDcToolbox.GenerateCodedValue(4, 2720, "MDC_DIM_BEAT_PER_MIN"),
    Resolution = Decimal.One
};
// Add metric to channel
chnPulse.Metric.Add(nmdPulse);
```

Coded values with the right nomenclature can easily be created by the __GenerateCodedValue__ function.
The last line adds the descriptor to a channel which in turn is part of a Vmd node (for more info see the __ExampleProvider__ project).

#### The measurement state ####

The measurement state's programmatically definition references the descriptor handle and must have a default value.

```csharp
var statePulse = SharpDcToolbox.CreateNumericState("HandleMetricPulse", 100);  // Handle and initial value
provider.AddState(statePulse);
```

Updating a state's actual value can be achived by the provider's __UpdateValue__ method.

```csharp
await provider.UpdateValue("HandleMetricPulse", 60);
```

### Settable states ###

Settable states can be altered by an SDC consumer. The Mdib must have an operation descriptor and an operation state to allow this action.

#### The setting descriptor ####

```csharp
var setDescr = new NumericMetricDescriptor()
{
    setDescr.Handle = "HandleSetter",
    setDescr.MetricCategory = MetricCategory.Set,  // The actual definition as setting
    // ...
}
```

#### The setting state ####

```csharp
var settableState = SharpDcToolbox.CreateNumericState("HandleSetter", 0);  // Handle and initial value
provider.AddState(settableState);
```

A convenience function can be called to automatically generate operation descriptors and states.

```csharp
provider.CreateSetOperations<SetValue>(mds, new [] 
{ 
    setDescr  // Multiple entries are possible here
});
```

The descriptor can also be retrieved from the provider using the handle.

```csharp
provider.CreateSetOperations<SetValue>(mds, new [] 
{ 
    await provider.GetMetricDescriptor<NumericMetricDescriptor>("HandleSetter")
});
```

To be able to process incoming change requests, a handler and a callback function must be registered for the handle and the specific type of set operation.

```csharp
provider.AddHandler("HandleSetter", typeof(SetValue), TestHandler);  // Handler registration

private static async Task<InvocationState> TestHandler(SetRequest request)
{
    // Get the value transmitted by a consumer
    var requestedValue = request.Get<SetValue>().RequestedNumericValue;
    
    // Perform additional tasks here, e.g. update device's state.
    
    // Update the provider's Mdib state
    await request.Endpoint.UpdateValue(request.Context.OperationTarget, requestedValue).ConfigureAwait(false);
    return InvocationState.FinMod;  // Result of change request (success case Fin or FinMod)
}
```

Note that the TestHandler method must not block the current thread. In case you have blocking code, encapsulate with __await Task.Run(() => { ... })__.

The following operations are available.

* SetValue
* SetString
* SetMetricState
* SetContextState
* SetAlertState
* Activate

Another way of handling change requests for different operation types is to use a whitecard operator for handling only the type of operation (instead of both the type and handle).

```csharp
provider.AddHandler("*", typeof(SetValue), TestHandler);  // Handler registration for all handles
```

The cases for specific handles must be implemented in the handler callback method.

### Advanced concepts ###

Examples of other possible uses of e.g. alarms and streaming will be published soon.

## Contact ##

For questions please contact info@besting-it.de
