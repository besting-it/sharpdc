﻿// file:	ISdcEndpoint.cs
//
// summary:	Declares the ISdcEndpoint interface
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SharpDC.Binding.Device;

namespace SharpDC
{
    
    /// <summary>   Interface for an Sdc endpoint (provider or consumer). </summary>
    public interface ISdcEndpoint
    {
        /// <summary>   Returns a clone of the current description. </summary>
        ///
        /// <returns>   The description. </returns>
        Task<MdDescription> GetDescription();

        /// <summary>   Returns a clone of the states. </summary>
        ///
        /// <param name="descriptorHandles">    Enumeration of handles (optional) </param>
        ///
        /// <returns>   The states. </returns>
        Task<MdState> GetStates(IEnumerable<string> descriptorHandles);

        /// <summary>
        /// Updates states, i.e. replaces states which have the same descriptor handle. Calling this
        /// function on a provider will updates states locally whereas calling this function on a
        /// consumer will updates states remotely, i.e. on the remote provider.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">    Passed when updating of one or more states failed. </exception>
        ///
        /// <param name="states">   Enumerable of states to update. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task UpdateStates(IEnumerable<AbstractState> states);
        
        /// <summary>
        /// Updates state's value, i.e. sets the numeric metric value of the state with the same
        /// descriptor handle. Calling this function on a provider will updates states locally whereas
        /// calling this function on a consumer will updates states remotely, i.e. on the remote provider.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">    Passed when updating of the state's value failed. </exception>
        ///
        /// <param name="descriptorHandle"> The descriptor handle. </param>
        /// <param name="value">            The new value. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task UpdateValue(string descriptorHandle, decimal value);
        
        /// <summary>
        /// Updates state's string, i.e. sets the numeric string value of the state with the same
        /// descriptor handle. Calling this function on a provider will updates states locally whereas
        /// calling this function on a consumer will updates states remotely, i.e. on the remote provider.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">    Passed when updating of the state's value failed. </exception>
        ///
        /// <param name="descriptorHandle"> The descriptor handle. </param>
        /// <param name="value">            The new string. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task UpdateString(string descriptorHandle, string value);          
        
        /// <summary>   Returns the endpoint reference. </summary>
        ///
        /// <value> The endpoint reference. </value>
        string EndpointReference { get; }

        /// <summary>   Returns the Mdib version. </summary>
        ///
        /// <value> The mdib version. </value>
        long MdibVersion { get; }

        /// <summary>   Returns the sequence Id. </summary>
        ///
        /// <value> The identifier of the sequence. </value>
        string SequenceId { get; }

        /// <summary>   Returns the device binding base. </summary>
        ///
        /// <value> The device binding. </value>
        IDeviceBindingBase DeviceBinding { get; }
    }

}