﻿// file:	SharpDc.cs
//
// summary:	Main entry point.
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using Microsoft.Extensions.Logging;
using SharpDC.Model.Schema;

namespace SharpDC
{
    /// <summary>   Values that represent schema validation modes. </summary>
    public enum SchemaValidationMode
    {
        /// <summary>   An enum constant representing the none option. </summary>
        None,
        /// <summary>   An enum constant representing the provider option. </summary>
        Provider,
        /// <summary>   An enum constant representing the consumer option. </summary>
        Consumer,
        /// <summary>   An enum constant representing all option. </summary>
        All
    }
    
    /// <summary>   Library configuration class. </summary>
    public class SharpDc
    {
        /// <summary>   The version. </summary>
        private const string Version = "0.9.7 Beta";
        
        /// <summary>   Gets or sets the logger. </summary>
        ///
        /// <value> The logger. </value>
        public ISdcLogger<SharpDc> Logger { get; private set; }

        /// <summary>   Gets or sets the provider schema validator. </summary>
        ///
        /// <value> The provider schema validator. </value>
        public Validator? ProviderSchemaValidator { get; private set; }
        
        /// <summary>   Gets or sets the consumer schema validator. </summary>
        ///
        /// <value> The consumer schema validator. </value>
        public Validator? ConsumerSchemaValidator { get; private set; }
        
        /// <summary>   The validator. </summary>
        private Validator? validator;

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        private SharpDc()
        {
            Logger = SdcLogFactory.Create<SharpDc>();
            Logger.Level = LogLevel.Information;            
            Startup();
        }

        /// <summary>   Gets the instance. </summary>
        ///
        /// <value> The instance. </value>
        public static SharpDc Instance { get; } = new SharpDc();

        /// <summary>   Prepares this object for use. </summary>
        private void Startup()
        {
            Logger.LogInformation($"SharpDC {Version} - IEEE 11073 SDC Family Stack");
            Logger.LogInformation("Copyright (C) Besting-IT. Contact: info@besting-it.de");
            Logger.LogInformation("Loading schema validator.");
            validator = new Validator {Logger = Logger};
        }

        /// <summary>   Set the schema validation mode. </summary>
        ///
        /// <param name="mode"> The mode. </param>
        public void SetSchemaValidationMode(SchemaValidationMode mode)
        {
            switch (mode)
            {
                case SchemaValidationMode.All:
                    ConsumerSchemaValidator = validator;
                    ProviderSchemaValidator = validator;
                    break;
                case SchemaValidationMode.Consumer:
                    ConsumerSchemaValidator = validator;
                    ProviderSchemaValidator = null;
                    break;
                case SchemaValidationMode.Provider:
                    ConsumerSchemaValidator = null;
                    ProviderSchemaValidator = validator;
                    break;
                case SchemaValidationMode.None:
                    ConsumerSchemaValidator = null;
                    ProviderSchemaValidator = null;                    
                    break;                
            }
        }        
        
    }
}
