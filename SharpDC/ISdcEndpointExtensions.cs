﻿// file:	ISdcEndpointExtensions.cs
//
// summary:	Declares the ISdcEndpointExtensions interface
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SharpDC
{
    /// <summary>   A sdc endpoint extensions. </summary>
    public static class ISdcEndpointExtensions
    {
        /// <summary>   Returns a clone of the Mdib. </summary>
        ///
        /// <param name="endpoint"> The endpoint. </param>
        ///
        /// <returns>   The Mdib. </returns>
        public static async Task<Mdib> GetMdib(this ISdcEndpoint endpoint)
        {
            var mdib = new Mdib
            {
                MdibVersion = (ulong)endpoint.MdibVersion,
                MdDescription = await endpoint.GetDescription().ConfigureAwait(false),
                MdState = await endpoint.GetStates().ConfigureAwait(false),
                SequenceId = endpoint.SequenceId
            };
            return mdib;
        }
        
        /// <summary>   Returns a clone of the states. </summary>
        ///
        /// <returns>   The states. </returns>
        public static async Task<MdState> GetStates(this ISdcEndpoint endpoint)
        {
            return await endpoint.GetStates(Enumerable.Empty<string>()).ConfigureAwait(false);
        }        

        /// <summary>   Returns a clone of a single state. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="endpoint">         The endpoint. </param>
        /// <param name="descriptorHandle"> The state's descriptor handle. </param>
        ///
        /// <exception cref="InvalidOperationException">      Passed when no match found. </exception>        
        /// 
        /// <returns>   The state </returns>
        public static async Task<T> GetState<T>(this ISdcEndpoint endpoint, string descriptorHandle) where T : AbstractState
        {
            var states = await endpoint.GetStates(new[] { descriptorHandle }).ConfigureAwait(false);
            return (T)states.State.First();
        }
        
        /// <summary>   Returns a clone of a metric descriptor. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="endpoint"> The endpoint. </param>
        /// <param name="handle">   The state's descriptor handle. </param>
        ///
        /// <exception cref="InvalidOperationException">      Passed when no match found. </exception>
        ///
        /// <returns>   The state. </returns>
        public static async Task<T> GetMetricDescriptor<T>(this ISdcEndpoint endpoint,
            string handle) where T : AbstractMetricDescriptor
            => (T)(await endpoint.GetDescription().ConfigureAwait(false)).Mds.SelectMany(mds =>
                mds.Vmd.SelectMany(vmd =>
                    vmd.Channel.SelectMany(ch => 
                        ch.Metric))).First(m => m.Handle == handle);

        /// <summary>   Returns a clone of a context descriptor. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="endpoint"> The endpoint. </param>
        /// <param name="handle">   The state's descriptor handle. </param>
        ///
        /// <exception cref="InvalidOperationException">      Passed when no match found. </exception>
        ///
        /// <returns>   The state. </returns>
        public static async Task<T> GetContextDescriptor<T>(this ISdcEndpoint endpoint,
            string handle) where T : AbstractContextDescriptor
            => (T)(await endpoint.GetDescription().ConfigureAwait(false)).Mds.SelectMany(mds =>
                    mds.SystemContext.EnsembleContext.Cast<AbstractContextDescriptor>().
                        Append(mds.SystemContext.LocationContext).
                        Concat(mds.SystemContext.MeansContext).
                        Concat(mds.SystemContext.OperatorContext).
                        Concat(mds.SystemContext.WorkflowContext).
                        Append(mds.SystemContext.PatientContext))
                .First(acd => acd.Handle == handle);

        /// <summary>   Returns a clone of all alert descriptors. </summary>
        ///
        /// <param name="endpoint">                 The endpoint. </param>
        /// 
        /// <returns>   The descriptors. </returns>
        public static async Task<IEnumerable<AbstractAlertDescriptor>> GetAlertDescriptors(this ISdcEndpoint endpoint)
        {
            var mds = (await endpoint.GetDescription().ConfigureAwait(false)).Mds;
            return mds.SelectMany(descriptor =>
            {
                if (descriptor.AlertSystem == null) return Enumerable.Empty<AbstractAlertDescriptor>();
                return descriptor.AlertSystem.AlertCondition.Concat(
                    descriptor.AlertSystem.AlertSignal.Cast<AbstractAlertDescriptor>());
            }).Concat(mds.SelectMany(m =>
                m.Vmd.SelectMany(descriptor =>
                {
                    if (descriptor.AlertSystem == null) return Enumerable.Empty<AbstractAlertDescriptor>();
                    return descriptor.AlertSystem.AlertCondition.Concat(
                        descriptor.AlertSystem.AlertSignal.Cast<AbstractAlertDescriptor>());
                })));
        }          
        
        /// <summary>   Returns a clone of all alert descriptors. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="endpoint">                 The endpoint. </param>
        /// <param name="mdsDescriptorProvider">    Returns list of descriptors for mds alert system. </param>
        /// <param name="vmdDescriptorProvider">    Returns list of descriptors for vmd alert system. </param>
        /// 
        /// <returns>   The descriptors. </returns>
        private static async Task<IEnumerable<T>> GetAlertDescriptors<T>(this ISdcEndpoint endpoint, Func<MdsDescriptor, 
            List<T>> mdsDescriptorProvider, Func<VmdDescriptor, List<T>> vmdDescriptorProvider) where T : AbstractAlertDescriptor
        {
            var mds = (await endpoint.GetDescription().ConfigureAwait(false)).Mds;
            return mds.SelectMany(mdsDescriptorProvider).Concat(mds.SelectMany(m =>
                m.Vmd.SelectMany(vmdDescriptorProvider)));
        }        
        
        /// <summary>   Returns a clone of an alert descriptor. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="endpoint">                 The endpoint. </param>
        /// <param name="handle">                   The state's descriptor handle. </param>
        /// <param name="mdsDescriptorProvider">    Returns list of descriptors for mds alert system. </param>
        /// <param name="vmdDescriptorProvider">    Returns list of descriptors for vmd alert system. </param>
        ///
        /// <exception cref="InvalidOperationException">      Passed when no match found. </exception>
        /// 
        /// <returns>   The descriptor. </returns>
        public static async Task<T> GetAlertDescriptor<T>(this ISdcEndpoint endpoint,
            string handle, Func<MdsDescriptor, List<T>> mdsDescriptorProvider, Func<VmdDescriptor
                , List<T>> vmdDescriptorProvider) where T : AbstractAlertDescriptor
            => (await endpoint.GetAlertDescriptors(mdsDescriptorProvider, vmdDescriptorProvider).ConfigureAwait(false)).
                First(a => a.Handle == handle);

        /// <summary>   Returns a clone of an alert signal descriptor. </summary>
        ///
        /// <param name="endpoint"> The endpoint. </param>
        /// <param name="handle">   The state's descriptor handle. </param>
        ///
        /// <exception cref="InvalidOperationException">      Passed when no match found. </exception>
        /// 
        /// <returns>   The descriptor. </returns>
        public static async Task<AlertSignalDescriptor> GetAlertSignalDescriptor(this ISdcEndpoint endpoint,
            string handle)
        {
            return await GetAlertDescriptor(endpoint, handle,
                descriptor => descriptor.AlertSystem != null? 
                    descriptor.AlertSystem.AlertSignal : new List<AlertSignalDescriptor>(), 
                descriptor => descriptor.AlertSystem != null? 
                    descriptor.AlertSystem.AlertSignal : new List<AlertSignalDescriptor>()).ConfigureAwait(false);
        }

        /// <summary>   Returns a clone of an alert condition descriptor. </summary>
        ///
        /// <param name="endpoint"> The endpoint. </param>
        /// <param name="handle">   The state's descriptor handle. </param>
        ///
        /// <exception cref="InvalidOperationException">      Passed when no match found. </exception>
        /// 
        /// <returns>   The descriptor. </returns>
        public static async Task<AlertConditionDescriptor> GetAlertConditionDescriptor(this ISdcEndpoint endpoint,
            string handle)
        {
            return await GetAlertDescriptor(endpoint, handle,
                descriptor => descriptor.AlertSystem != null? 
                    descriptor.AlertSystem.AlertCondition : new List<AlertConditionDescriptor>(), 
                descriptor => descriptor.AlertSystem != null? 
                    descriptor.AlertSystem.AlertCondition : new List<AlertConditionDescriptor>()).ConfigureAwait(false);
        }
        
        /// <summary>   Gets the operation target for an operation handle. </summary>
        ///
        /// <param name="endpoint">         The endpoint. </param>
        /// <param name="operationHandle">  The operation's handle. </param>
        ///
        /// <exception cref="InvalidOperationException">      Passed when no match found. </exception>
        /// 
        /// <returns>   The operation target. </returns>
        public static async Task<string> GetOperationTarget(this ISdcEndpoint endpoint, string operationHandle)
        {
            var op = (await endpoint.GetOperationDescriptors().ConfigureAwait(false)).
                First(o => o.Handle == operationHandle);
            return op.OperationTarget;
        }

        /// <summary>   Gets all operation descriptors (mds and vmd). </summary>
        /// 
        /// <param name="endpoint">         The endpoint. </param>
        /// 
        /// <returns>   List of all operation descriptors. </returns>
        public static async Task<IEnumerable<AbstractOperationDescriptor>> GetOperationDescriptors(
            this ISdcEndpoint endpoint)
        {
            var mds = (await endpoint.GetDescription().ConfigureAwait(false)).Mds;
            return mds.SelectMany(m => m.Sco.Operation)
                .Concat(mds.SelectMany(m => m.Vmd).SelectMany(v => v.Sco.Operation));
        }
        
        /// <summary>   Gets the operation handles for an operation target. </summary>
        ///
        /// <param name="endpoint">         The endpoint. </param>
        /// <param name="operationTarget">  The operation's target. </param>
        ///
        /// <returns>   List of matching operation handles. </returns>
        public static async Task<IEnumerable<string>> GetOperationHandles(this ISdcEndpoint endpoint, string operationTarget)
            => (await endpoint.GetOperationDescriptors().ConfigureAwait(false)).
                Where(o => o.OperationTarget == operationTarget).
                    Select(o => o.Handle);

        /// <summary>
        /// Updates a state, i.e. replaces the state which has the same descriptor handle. Calling this
        /// function on a provider will updates the state locally whereas calling this function on a
        /// consumer will updates the state remotely, i.e. on the remote provider.
        /// </summary>
        ///
        /// <param name="endpoint"> The endpoint. </param>
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        ///
        /// <exception cref="InvalidOperationException">    Passed when updating failed. </exception>
        public static async Task UpdateState(this ISdcEndpoint endpoint, AbstractState state) =>
            await endpoint.UpdateStates(new[] {state}).ConfigureAwait(false);

    }
}