﻿// file:	SharpDcToolbox.cs
//
// summary:	Implements the toolbox class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;

namespace SharpDC
{
    /// <summary>   A toolbox. </summary>
    public static class SharpDcToolbox
    {
        /// <summary>   Generate coded value conform with ISO-IEC 11073-10101. </summary>
        ///
        /// <param name="blockNumber">      The block number.
        ///                                 1 = Infrastructure (e.g. MDS)
        ///                                 2 = SCADA (physiological IDs, e.g. metrics)
        ///                                 3 = Events and alerts
        ///                                 4 = Dimension (e.g. units) </param>
        /// <param name="termCode">         The context-sensitive term code. </param>
        /// <param name="symbolicCodeName"> (Optional) The symbolic code name, optional. </param>
        ///
        /// <returns>   The coded value. </returns>
        ///
        /// ### <param name="codingSystem"> The coding system, optional, ISO-IEC 11073-10101. </param>
        ///
        /// ### <param name="codingSystemVersion">  The coding system version, optional, ISO-IEC 11073-
        ///                                         10101:2004. </param>
        public static CodedValue GenerateCodedValue(int blockNumber = 0, int termCode = 0, string symbolicCodeName = "N/A", 
            string codingSystem = "urn:oid:1.2.840.10004.1.1.1.0.0.1", string codingSystemVersion = "20041215") 
        {
            var cv = new CodedValue
            {
                Code = (blockNumber * (int) Math.Pow(2, 16) + termCode).ToString(),
                SymbolicCodeName = symbolicCodeName,
                CodingSystem = codingSystem,
                CodingSystemVersion = codingSystemVersion
            };
            return cv;
        }    
        
        /// <summary>   Create a default numeric metric state. </summary>
        ///
        /// <param name="handle">   The descriptor handle. </param>
        /// <param name="value">    The value. </param>
        ///
        /// <returns>   The state. </returns>
        public static NumericMetricState CreateNumericState(string handle, decimal value) {
            var nms = new NumericMetricState { ActivationState = ComponentActivation.On, DescriptorHandle = handle };
            var nv = new NumericMetricValue { Value = value};
            var amq = new AbstractMetricValueMetricQuality { Validity = MeasurementValidity.NA };
            nv.MetricQuality = amq;
            nms.MetricValue = nv;
            return nms;
        }
        
        /// <summary>   Create a default string metric state. </summary>
        ///
        /// <param name="handle">   The descriptor handle. </param>
        /// <param name="value">    The value. </param>
        ///
        /// <returns>   The state. </returns>
        public static StringMetricState CreateStringState(string handle, string value) {
            var sms = new StringMetricState { ActivationState = ComponentActivation.On, DescriptorHandle = handle };
            var sv = new StringMetricValue();
            var amq = new AbstractMetricValueMetricQuality { Validity = MeasurementValidity.NA };
            sv.MetricQuality = amq;
            sv.Value = value;            
            sms.MetricValue = sv;
            return sms;
        }  
    }
}