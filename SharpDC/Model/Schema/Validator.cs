﻿// file:	Model\Schema\Validator.cs
//
// summary:	Implements the validator class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using Microsoft.Extensions.Logging;
using SharpDC.Misc;
using SharpDC.Properties;

namespace SharpDC.Model.Schema
{
    /// <summary>   A validator. </summary>
    public class Validator
    {
        /// <summary>   The schemaset. </summary>
        readonly XmlSchemaSet schemaset;
        /// <summary>   Gets or sets the logger. </summary>
        ///
        /// <value> The logger. </value>
        public ISdcLogger<SharpDc>? Logger { get; set; }
        /// <summary>   Gets or sets a value indicating whether the warning as errors. </summary>
        ///
        /// <value> True if warning as errors, false if not. </value>
        public bool WarningAsErrors { get; set; } = false;
        /// <summary>   Event queue for all listeners interested in ValidationResult events. </summary>
        public event EventHandler<ValidationEventArgs>? ValidationResult;

        /// <summary>   The locker. </summary>
        static readonly object locker = new Object();

        /// <summary>   Default constructor. </summary>
        public Validator()
        {
            schemaset = new XmlSchemaSet();
            try
            {
                using (var input = StringTools.GenerateStreamFromString(Resources.BICEPS_MessageModel))
                {
                    schemaset.Add(XmlSchema.Read(XmlReader.Create(input), OnValidate));
                }
                using (var input1 = StringTools.GenerateStreamFromString(Resources.BICEPS_ParticipantModel))
                {
                    schemaset.Add(XmlSchema.Read(XmlReader.Create(input1), OnValidate));
                }
                using (var input2 = StringTools.GenerateStreamFromString(Resources.ExtensionPoint))
                {
                    schemaset.Add(XmlSchema.Read(XmlReader.Create(input2), OnValidate));
                }
                using (var input3 = StringTools.GenerateStreamFromString(Resources.MDPWS))
                {
                    schemaset.Add(XmlSchema.Read(XmlReader.Create(input3), OnValidate));
                }
            }
            catch(XmlException e)
            {
                Logger.LogError(e, e.Message);
            }
        }

        /// <summary>   Raises the validation event. </summary>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnValidate(object sender, ValidationEventArgs e)
        {
            ValidationResult?.Invoke(sender, e);
            Log(e);
        }

        /// <summary>   Logs the given e. </summary>
        ///
        /// <param name="e">    Validation event information. </param>
        private void Log(ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Error || WarningAsErrors)
                Logger.LogError(e.Message);
            else
                Logger.LogWarning(e.Message);
        }

        /// <summary>   Validates. </summary>
        ///
        /// <exception cref="XmlException"> Thrown when an XML error condition occurs. </exception>
        ///
        /// <param name="str">      The string. </param>
        /// <param name="rethrow">  (Optional) True to rethrow. </param>
        public void Validate(string str, bool rethrow = true)
        {
            lock (locker)
            {
                var settings = new XmlReaderSettings { CloseInput = true };
                settings.ValidationEventHandler += OnValidate; 
                settings.ValidationType = ValidationType.Schema;
                settings.Schemas.Add(schemaset);
                settings.ValidationFlags = XmlSchemaValidationFlags.ReportValidationWarnings | 
                   XmlSchemaValidationFlags.ProcessIdentityConstraints | 
                   XmlSchemaValidationFlags.ProcessInlineSchema | 
                   XmlSchemaValidationFlags.ProcessSchemaLocation;
                try
                {
                    using var reader = new StringReader(str);
                    using var validatingReader = XmlReader.Create(reader, settings);
                    while (validatingReader.Read())
                    { 
                        // NOOP 
                    }
                }
                catch (XmlException e)
                {
                    Logger.LogError(e, e.Message);
                    throw;
                }
            }
        }

    }
}
