﻿// file:	Logging.cs
//
// summary:	Implements the logging class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Globalization;
using System.Text;
using Microsoft.Extensions.Logging;

namespace SharpDC
{
    /// <summary>   Interface for sdc logger. </summary>
    ///
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    public interface ISdcLogger<T> : ILogger<T>, IDisposable
    {
        /// <summary>   Gets or sets the level. </summary>
        ///
        /// <value> The level. </value>
        LogLevel Level { get; set; }

        /// <summary>   Gets or sets the output. </summary>
        ///
        /// <value> The output. </value>
        Action<string> Output { get; set; }
    }

    /// <summary>   A sdc log factory. </summary>
    public static class SdcLogFactory
    {
        /// <summary>   Creates a new ISdcLogger&lt;T&gt; </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        ///
        /// <returns>   An ISdcLogger&lt;T&gt; </returns>
        public static ISdcLogger<T> Create<T>()
        {
            return Create<T>(Console.WriteLine);
        }

        /// <summary>   Creates a new ISdcLogger&lt;T&gt; </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="action">   The action. </param>
        ///
        /// <returns>   An ISdcLogger&lt;T&gt; </returns>
        public static ISdcLogger<T> Create<T>(Action<string> action)
        {
            var logger = new SdcLogger<T>()
            {
                Output = action
            };
            return logger;
        }

    }

    /// <summary>   A sdc logger. </summary>
    ///
    /// <typeparam name="T">    Generic type parameter. </typeparam>
    public class SdcLogger<T> : ISdcLogger<T>
    {
        /// <summary>   Gets or sets the level. </summary>
        ///
        /// <value> The level. </value>
        public LogLevel Level { get; set; } = LogLevel.Information;
        /// <summary>   Gets or sets the output. </summary>
        ///
        /// <value> The output. </value>
        public Action<string> Output { get; set; } = Console.WriteLine;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>   Logs. </summary>
        ///
        /// <typeparam name="TState">   Type of the state. </typeparam>
        /// <param name="logLevel">     The log level. </param>
        /// <param name="eventId">      Identifier for the event. </param>
        /// <param name="state">        The state. </param>
        /// <param name="exception">    The exception. </param>
        /// <param name="formatter">    The formatter. </param>
        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (IsEnabled(logLevel))
            {
                DoLog(logLevel, state, exception, formatter);
            }
        }

        /// <summary>   Executes the log operation. </summary>
        ///
        /// <typeparam name="TState">   Type of the state. </typeparam>
        /// <param name="logLevel">     The log level. </param>
        /// <param name="state">        The state. </param>
        /// <param name="exception">    The exception. </param>
        /// <param name="formatter">    The formatter. </param>
        public virtual void DoLog<TState>(LogLevel logLevel, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            var sb = new StringBuilder();
            sb.Append("[");
            sb.Append(DateTime.Now.ToString("yyyy/MM/dd H:mm:ss:fff", CultureInfo.InvariantCulture));
            sb.Append(" | ");
            sb.Append(logLevel);
            sb.Append("] ");
            sb.Append(formatter(state, exception));
            var str = sb.ToString();
            Output(str);
        }

        /// <summary>   Query if 'logLevel' is enabled. </summary>
        ///
        /// <param name="logLevel"> The log level. </param>
        ///
        /// <returns>   True if enabled, false if not. </returns>
        public bool IsEnabled(LogLevel logLevel) => logLevel >= Level;

        /// <summary>   Begins a scope. </summary>
        ///
        /// <typeparam name="TState">   Type of the state. </typeparam>
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An IDisposable. </returns>
        public IDisposable BeginScope<TState>(TState state) => this;
    }
}
