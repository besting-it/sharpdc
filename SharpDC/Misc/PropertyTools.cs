﻿// file:	Misc\PropertyTools.cs
//
// summary:	Implements the property tools class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace SharpDC.Misc
{
    /// <summary>   A default property changed. </summary>
    public class DefaultPropertyChanged : INotifyPropertyChanged
    {
        /// <summary>   Occurs when a property value changes. </summary>
        public event PropertyChangedEventHandler? PropertyChanged;

        /// <summary>   Executes the property changed action. </summary>
        ///
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        internal void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
    
    /// <summary>   A property tools default property changed extensions. </summary>
    public static class PropertyToolsDefaultPropertyChangedExtensions
    {
        /// <summary>   A DefaultPropertyChanged extension method that sets. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="pc">           The PC to act on. </param>
        /// <param name="from">         Source for the. </param>
        /// <param name="to">           [in,out] to. </param>
        /// <param name="propertyName"> (Optional) Name of the property. </param>
        public static void Set<T>(this DefaultPropertyChanged pc, T from, ref T to, [CallerMemberName] string? propertyName = null)
        {
            if (to != null && to.Equals(from))
                return;
            to = from;
            pc.OnPropertyChanged(propertyName);
        }
    }
}