﻿// file:	Misc\ExceptionTools.cs
//
// summary:	Implements the exception tools class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Threading.Tasks;

namespace SharpDC.Misc
{
    /// <summary>   An exception tools. </summary>
    public class ExceptionTools
    {
        /// <summary>   Exception trap. </summary>
        ///
        /// <param name="task"> The task. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public static async Task ExceptionTrap(Func<Task> task)
        {
            try { await task().ConfigureAwait(false); } catch { }
        }
    }
}