﻿// file:	Misc\StringTools.cs
//
// summary:	Implements the string tools class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SharpDC.Misc
{
    /// <summary>   A string tools. </summary>
    internal static class StringTools
    {
        /// <summary>   Generates a stream from string. </summary>
        ///
        /// <param name="value">    The value. </param>
        ///
        /// <returns>   The stream from string. </returns>
        internal static MemoryStream GenerateStreamFromString(string value)
        {
            return new MemoryStream(Encoding.ASCII.GetBytes(value ?? ""));
        }

        /// <summary>
        /// Removes all carriage returns & line feeds from a string
        /// </summary>
        /// <param name="str">The input string</param>
        /// <returns>The cleaned string</returns>
        internal static string RemoveCrLf(string str)
        {
            var sb = new StringBuilder();
            using var reader = new StringReader(str);
            string? next;
            while ((next = reader.ReadLine()) != null)
            {
                sb.Append(next.Trim());
            }
            return sb.ToString();
        }
    }
}
