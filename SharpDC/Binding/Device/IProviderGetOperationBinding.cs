﻿// file:	Binding\Device\IProviderGetOperationBinding.cs
//
// summary:	Declares the IProviderGetOperationBinding interface
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SharpDC.Binding.Device
{
    /// <summary>   Interface for provider get operation binding. </summary>
    public interface IProviderGetOperationBinding
    {
        /// <summary>   Executes the get mdib action. </summary>
        ///
        /// <param name="request">  The request. </param>
        ///
        /// <returns>   An asynchronous result that yields a GetMdibResponse. </returns>
        Task<GetMdibResponse> OnGetMdib(GetMdib request);
        /// <summary>   Executes the get md description action. </summary>
        ///
        /// <param name="request">  The request. </param>
        ///
        /// <returns>   An asynchronous result that yields a GetMdDescriptionResponse. </returns>
        Task<GetMdDescriptionResponse> OnGetMdDescription(GetMdDescription request);
        /// <summary>   Executes the get md state action. </summary>
        ///
        /// <param name="request">  The request. </param>
        ///
        /// <returns>   An asynchronous result that yields a GetMdStateResponse. </returns>
        Task<GetMdStateResponse> OnGetMdState(GetMdState request);
        /// <summary>   Executes the get context states action. </summary>
        ///
        /// <param name="request">  The request. </param>
        ///
        /// <returns>   An asynchronous result that yields a GetContextStatesResponse. </returns>
        Task<GetContextStatesResponse> OnGetContextStates(GetContextStates request);
    }
}
