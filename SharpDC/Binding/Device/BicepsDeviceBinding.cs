﻿// file:	Binding\Device\BicepsDeviceBinding.cs
//
// summary:	Implements the biceps device binding class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Threading.Tasks;
using SharpDC.Provider;

namespace SharpDC.Binding.Device
{
    /// <summary>   The biceps device binding. </summary>
    ///
    /// <typeparam name="SettingsType"> Type of the settings type. </typeparam>
    public abstract class BicepsDeviceBinding<SettingsType> : IDeviceBinding<SettingsType> where SettingsType : IDeviceBindingSettings
    {
        /// <summary>   Gets or sets the get binding. </summary>
        ///
        /// <value> The get binding. </value>
        public IProviderGetOperationBinding GetBinding { get; set; } = null!;

        /// <summary>   Gets or sets the event binding. </summary>
        ///
        /// <value> The event binding. </value>
        public abstract IProviderEventSourceBinding EventBinding { get; set; }
        
        /// <summary>   Gets or sets the set binding. </summary>
        ///
        /// <value> The set binding. </value>
        public IProviderSetOperationBinding SetBinding { get; set; } = null!;

        /// <summary>   Gets or sets a value indicating whether this object is initialized. </summary>
        ///
        /// <value> True if this object is initialized, false if not. </value>
        public bool IsInitialized { get; private set; }
        
        /// <summary>   Gets or sets options for controlling the operation. </summary>
        ///
        /// <value> The settings. </value>
        public SettingsType Settings { get; private set; } = default!;

        /// <summary>   Gets or sets the data. </summary>
        ///
        /// <value> The data. </value>
        protected SdcProviderData Data { get; private set; } = null!;

        /// <summary>   Gets or sets the endpoint. </summary>
        ///
        /// <value> The endpoint. </value>
        protected ISdcEndpoint Endpoint { get; private set; } = null!;

        /// <summary>   De initialize. </summary>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task DeInitialize()
        {
            IsInitialized = false;
            await Stop();
        }

        /// <summary>   Initializes this object. </summary>
        ///
        /// <param name="settings"> Options for controlling the operation. </param>
        /// <param name="endpoint"> The endpoint. </param>
        /// <param name="data">     The data. </param>
        /// <param name="epr">      The epr. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task Initialize(SettingsType settings, ISdcEndpoint endpoint, SdcProviderData data, string epr)
        {
            GetBinding = new BicepsProviderGetOperationBinding(endpoint);
            SetBinding = new BicepsProviderSetOperationBinding(endpoint);
            IsInitialized = true;
            Settings = settings;
            Data = data;
            Endpoint = endpoint;
            await Start().ConfigureAwait(false);
        }

        /// <summary>   Start. </summary>
        ///
        /// <returns>   An asynchronous result. </returns>
        protected abstract Task Start();

        /// <summary>   Stop. </summary>
        ///
        /// <returns>   An asynchronous result. </returns>
        protected abstract Task Stop();
    }
}
