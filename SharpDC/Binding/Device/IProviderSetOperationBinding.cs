﻿// file:	Binding\Device\IProviderSetOperationBinding.cs
//
// summary:	Declares the IProviderSetOperationBinding interface
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SharpDC.Binding.Device
{
    /// <summary>   Interface for provider set operation binding. </summary>
    public interface IProviderSetOperationBinding
    {
        /// <summary>   Executes the set state action. </summary>
        ///
        /// <typeparam name="RequestType">  Type of the request type. </typeparam>
        /// <typeparam name="ResponseType"> Type of the response type. </typeparam>
        /// <param name="request">  The request. </param>
        /// <param name="si">       The SI. </param>
        ///
        /// <returns>   An asynchronous result that yields a ResponseType. </returns>
        Task<ResponseType> OnSetState<RequestType, ResponseType>(RequestType request, SafetyInfoType? si) where RequestType : AbstractSet where ResponseType : AbstractSetResponse;

        /// <summary>   Closes this object. </summary>
        void Close();
    }
}
