﻿// file:	Binding\Device\BicepsProviderSetOperationBinding.cs
//
// summary:	Implements the biceps provider set operation binding class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Microsoft.Extensions.Logging;
using SharpDC.Provider;

namespace SharpDC.Binding.Device
{
    /// <summary>   A set request. </summary>
    public class SetRequest
    {
        /// <summary>
        /// Constructor. 
        /// </summary>
        public SetRequest(OperationInvocationContext context, ISdcEndpoint endpoint, AbstractSet request)
        {
            Context = context;
            Endpoint = endpoint;
            Request = request;
        }

        /// <summary>   Gets or sets the request. </summary>
        ///
        /// <value> The request. </value>
        public AbstractSet Request { get; }
        /// <summary>   Gets the typed request. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        ///
        /// <returns>   The typed request. </returns>
        public T Get<T>() where T : AbstractSet
        {
            return (T) Request;
        }
        
        /// <summary>   Gets or sets the context. </summary>
        ///
        /// <value> The context. </value>
        public OperationInvocationContext Context { get; }
        /// <summary>   Gets or sets the endpoint. </summary>
        ///
        /// <value> The endpoint. </value>
        public ISdcEndpoint Endpoint { get; }
    }
    
    /// <summary>   The biceps provider set operation binding. </summary>
    public class BicepsProviderSetOperationBinding : IProviderSetOperationBinding
    {
        /// <summary>   The endpoint. </summary>
        private readonly ISdcEndpoint endpoint;
        /// <summary>   The queue. </summary>
        private readonly BufferBlock<SetRequest> queue = new BufferBlock<SetRequest>();
        /// <summary>   The cts. </summary>
        private readonly CancellationTokenSource cts = new CancellationTokenSource();
        /// <summary>   Identifier for the transaction. </summary>
        private long transactionId;
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="endpoint"> The endpoint. </param>
        public BicepsProviderSetOperationBinding(ISdcEndpoint endpoint)
        {
            this.endpoint = endpoint;
            Task.Run(async () =>
            {
                while (!cts.IsCancellationRequested)
                {
                    try
                    {
                        var next = await queue.ReceiveAsync(cts.Token).ConfigureAwait(false);
                        var handlerTask = next.Request switch
                        {
                            SetMetricState set => HandleSetRequest(next, next.Endpoint, typeof(SetMetricState)),
                            SetValue set => HandleSetRequest(next, next.Endpoint, typeof(SetValue)),
                            SetString set => HandleSetRequest(next, next.Endpoint, typeof(SetString)),
                            SetContextState set => HandleSetRequest(next, next.Endpoint, typeof(SetContextState)),
                            SetAlertState set => HandleSetRequest(next, next.Endpoint, typeof(SetAlertState)),
                            Activate set => HandleSetRequest(next, next.Endpoint, typeof(Activate)),
                            _ => HandleFault(next, next.Endpoint)
                        };
                        await handlerTask.ConfigureAwait(false);
                    }
                    catch (OperationCanceledException)
                    {
                    }
                    catch (Exception e)
                    {
                        SharpDc.Instance.Logger.LogCritical(
                            $"Exception in biceps set operation binding: {e.Message}.");
                    }
                }
            });
        }
        
        /// <summary>   Handles the set request. </summary>
        ///
        /// <param name="request">      The request. </param>
        /// <param name="provider">     The provider. </param>
        /// <param name="handlerType">  Type of the handler. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private static async Task HandleSetRequest(SetRequest request, 
            ISdcEndpoint provider, Type handlerType)
        {
            SharpDc.Instance.Logger.LogTrace($"Invoking handler for handle '{request.Context.OperationTarget}' and type '{handlerType}'");
            try
            {
                var handler = await GetHandler(request, provider, handlerType).ConfigureAwait(false);
                await provider.DeviceBinding.EventBinding.HandleOperationInvokedEvent(request.Context, InvocationState.Start, null).ConfigureAwait(false);
                var invState = await handler(request).ConfigureAwait(false);
                await provider.DeviceBinding.EventBinding.HandleOperationInvokedEvent(request.Context, invState, null).ConfigureAwait(false);
            }
            catch (InvalidOperationException)
            {
                // Handler not found
            }
        }

        /// <summary>   Tries to get a handler for the set request. </summary>
        ///
        /// <param name="request">      The request. </param>
        /// <param name="provider">     The provider. </param>
        /// <param name="requestType">  Type of the request. </param>
        ///
        /// <exception cref="InvalidOperationException">    Passed when handler not available. </exception>
        ///
        /// <returns>   An asynchronous result that yields the handler. </returns>
        private static async Task<Func<SetRequest, Task<InvocationState>>> GetHandler(SetRequest request, 
            ISdcEndpoint provider, Type requestType)
        {
            var handlers = ((IGenericSdcProvider)provider).Handlers.
                Where(t => t.RequestType == requestType && (t.Handle == request.Context.OperationTarget || t.Handle == "*")).ToList();
            if (handlers.Count > 1)
            {
                SharpDc.Instance.Logger.LogWarning($"Multiple handlers found for handle '{request.Context.OperationTarget}' and type '{requestType}'");
                await provider.DeviceBinding.EventBinding.HandleOperationInvokedEvent(request.Context, InvocationState.Fail,
                        $"Multiple handlers found for handle '{request.Context.OperationTarget}' and type '{requestType}'")
                    .ConfigureAwait(false);
                throw new InvalidOperationException();
            }
            // Success
            if (handlers.Count == 1) return handlers.First().Handler;
            // No handler found
            SharpDc.Instance.Logger.LogWarning($"No handler found for handle '{request.Context.OperationTarget}' and type '{requestType}'");
            await provider.DeviceBinding.EventBinding.HandleOperationInvokedEvent(request.Context, InvocationState.Fail,
                    $"No handler found for handle '{request.Context.OperationTarget}' and type '{requestType}'")
                .ConfigureAwait(false);
            throw new InvalidOperationException();
        }

        /// <summary>   Handles the fault. </summary>
        ///
        /// <param name="request">  The request. </param>
        /// <param name="provider"> The provider. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private static async Task HandleFault(SetRequest request,
            ISdcEndpoint provider)
        {
            await provider.DeviceBinding.EventBinding.HandleOperationInvokedEvent(request.Context, InvocationState.Fail, 
                $"No handler support for handle '{request.Context.OperationTarget}' and type '{request.Request.GetType()}'").ConfigureAwait(false);
        }

        /// <summary>   Closes this object. </summary>
        public void Close()
        {
            cts.Cancel();
            cts.Dispose();
        }
        
        /// <summary>   Executes the set state action. </summary>
        ///
        /// <typeparam name="RequestType">  Type of the request type. </typeparam>
        /// <typeparam name="ResponseType"> Type of the response type. </typeparam>
        /// <param name="request">  The request. </param>
        /// <param name="si">       The SI. </param>
        ///
        /// <returns>   An asynchronous result that yields a ResponseType. </returns>
        public async Task<ResponseType> OnSetState<RequestType, ResponseType>(RequestType request, SafetyInfoType? si) where RequestType : AbstractSet where ResponseType : AbstractSetResponse
        {
            SharpDc.Instance.Logger.LogTrace($"Processing set request for handle: {request.OperationHandleRef}.");
            var response = Activator.CreateInstance<ResponseType>();
            var info = new InvocationInfo
            {
                TransactionId = (uint) Interlocked.Increment(ref transactionId),
                InvocationState = InvocationState.Wait
            };
            response.InvocationInfo = info;
            response.MdibVersion = (ulong)endpoint.MdibVersion;
            response.SequenceId = endpoint.SequenceId;
            var eventSource = endpoint.DeviceBinding.EventBinding;
            try
            {
                // Resolve operation target (in case of Activate, already given)
                var operationTarget = request is Activate? request.OperationHandleRef : await endpoint.GetOperationTarget(request.OperationHandleRef).ConfigureAwait(false);
                var context = new OperationInvocationContext(request.OperationHandleRef, operationTarget, info.TransactionId);
                await queue.SendAsync(new SetRequest(context, endpoint, request)).ConfigureAwait(false);
                await eventSource.HandleOperationInvokedEvent(context, InvocationState.Wait, null).ConfigureAwait(false);                
            }
            catch (InvalidOperationException)
            {
                SharpDc.Instance.Logger.LogWarning($"Operation target not found for operation handle '{request.OperationHandleRef}'");
                await eventSource.HandleOperationInvokedEvent(new OperationInvocationContext(request.OperationHandleRef, string.Empty, info.TransactionId), 
                    InvocationState.Fail, 
                    $"Operation target not found for operation handle '{request.OperationHandleRef}'").ConfigureAwait(false);
                response.InvocationInfo.InvocationState = InvocationState.Fail;
                return response;      
            }
            return response;
        }
    }
}