﻿// file:	Binding\Device\BicepsProviderGetOperationBinding.cs
//
// summary:	Implements the biceps provider get operation binding class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Linq;
using System.Threading.Tasks;
using SharpDC.Provider;

namespace SharpDC.Binding.Device
{
    /// <summary>   The biceps provider get operation binding. </summary>
    public class BicepsProviderGetOperationBinding : IProviderGetOperationBinding
    {
        /// <summary>   The endpoint. </summary>
        private readonly ISdcEndpoint endpoint;
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="endpoint"> The endpoint. </param>
        public BicepsProviderGetOperationBinding(ISdcEndpoint endpoint)
        {
            this.endpoint = endpoint;
        }

        /// <summary>   Executes the get mdib action. </summary>
        ///
        /// <param name="request">  The request. </param>
        ///
        /// <returns>   An asynchronous result that yields a GetMdibResponse. </returns>
        public async Task<GetMdibResponse> OnGetMdib(GetMdib request)
        {
            var response = new GetMdibResponse();
            response.Mdib = await endpoint.GetMdib();
            response.MdibVersion = (ulong)endpoint.MdibVersion;
            response.SequenceId = endpoint.SequenceId;
            return response;
        }

        /// <summary>   Executes the get md description action. </summary>
        ///
        /// <param name="request">  The request. </param>
        ///
        /// <returns>   An asynchronous result that yields a GetMdDescriptionResponse. </returns>
        public async Task<GetMdDescriptionResponse> OnGetMdDescription(GetMdDescription request)
        {
            var response = new GetMdDescriptionResponse();
            response.MdDescription = await endpoint.GetDescription();
            response.MdibVersion = (ulong)endpoint.MdibVersion;
            response.SequenceId = endpoint.SequenceId;
            return response;
        }

        /// <summary>   Executes the get md state action. </summary>
        ///
        /// <param name="request">  The request. </param>
        ///
        /// <returns>   An asynchronous result that yields a GetMdStateResponse. </returns>
        public async Task<GetMdStateResponse> OnGetMdState(GetMdState request)
        {
            var response = new GetMdStateResponse();
            var states = await endpoint.GetStates();
            var mdStates = states.State.Where(s => !(s is AbstractContextState));
            if (request.HandleRef != null && request.HandleRef.Count > 0)
            {
                response.MdState.State.AddRange(mdStates.Where(s => request.HandleRef.Any(r => s.DescriptorHandle == r)));
            }
            else
            {
                response.MdState.State.AddRange(mdStates);
            }
            response.MdibVersion = (ulong)endpoint.MdibVersion;
            response.SequenceId = endpoint.SequenceId;
            return response;
        }

        /// <summary>   Executes the get context states action. </summary>
        ///
        /// <param name="request">  The request. </param>
        ///
        /// <returns>   An asynchronous result that yields a GetContextStatesResponse. </returns>
        public async Task<GetContextStatesResponse> OnGetContextStates(GetContextStates request)
        {
            var response = new GetContextStatesResponse();
            var states = await endpoint.GetStates();
            var contextStates = states.State.OfType<AbstractContextState>();
            var abstractContextStates = contextStates as AbstractContextState[] ?? contextStates.ToArray();
            if (request.HandleRef != null && request.HandleRef.Count > 0)
            {
                response.ContextState.AddRange(abstractContextStates.Where(s => request.HandleRef.Any(r => s.DescriptorHandle == r)));
            }
            else
            {
                response.ContextState.AddRange(abstractContextStates);
            }
            response.MdibVersion = (ulong)endpoint.MdibVersion;
            response.SequenceId = endpoint.SequenceId;
            return response;
        }
    }
}