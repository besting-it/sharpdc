﻿// file:	Binding\Device\BicepsProviderEventSourceBinding.cs
//
// summary:	Implements the biceps provider event source binding class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Threading.Tasks;
using SharpDC.Binding.Device.Mdpws.Managers;
using SharpDC.Provider;

namespace SharpDC.Binding.Device
{
    /// <summary>   The biceps provider event source binding. </summary>
    public abstract class BicepsProviderEventSourceBinding : IProviderEventSourceBinding
    {
        /// <summary>   The endpoint. </summary>
        private readonly ISdcEndpoint endpoint;
        /// <summary>   The data. </summary>
        private SdcProviderData data;
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="endpoint"> The endpoint. </param>
        /// <param name="data">     The data. </param>
        public BicepsProviderEventSourceBinding(ISdcEndpoint endpoint, SdcProviderData data)
        {
            this.endpoint = endpoint;
            this.data = data;
        }

        /// <summary>   Handles the episodic metric event described by state. </summary>
        ///
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task HandleEpisodicMetricEvent(AbstractMetricState state)
        {
            await FireEpisodicMetricEventReport(GetMetricReport<EpisodicMetricReport>(state)).ConfigureAwait(false);
        }

        /// <summary>   Handles the periodic metric event described by state. </summary>
        ///
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task HandlePeriodicMetricEvent(AbstractMetricState state)
        {
            await FirePeriodicMetricEventReport(GetMetricReport<PeriodicMetricReport>(state)).ConfigureAwait(false);
        }

        /// <summary>   Gets metric report. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   The metric report. </returns>
        public T GetMetricReport<T>(AbstractMetricState state) where T : AbstractMetricReport
        {
            var report = Activator.CreateInstance<T>();
            var part = new AbstractMetricReportReportPart();
            part.MetricState.Add(state);
            report.ReportPart.Add(part);
            report.MdibVersion = (ulong) endpoint.MdibVersion;
            report.SequenceId = endpoint.SequenceId;
            return report;
        }

        /// <summary>   Handles the episodic alert event described by state. </summary>
        ///
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task HandleEpisodicAlertEvent(AbstractAlertState state)
        {
            await FireEpisodicAlertEventReport(GetAlertReport<EpisodicAlertReport>(state)).ConfigureAwait(false);
        }

        /// <summary>   Handles the periodic alert event described by state. </summary>
        ///
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task HandlePeriodicAlertEvent(AbstractAlertState state)
        {
            await FirePeriodicAlertEventReport(GetAlertReport<PeriodicAlertReport>(state)).ConfigureAwait(false);
        }

        /// <summary>   Gets alert report. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   The alert report. </returns>
        public T GetAlertReport<T>(AbstractAlertState state) where T : AbstractAlertReport
        {
            var report = Activator.CreateInstance<T>();
            var part = new AbstractAlertReportReportPart();
            part.AlertState.Add(state);
            report.ReportPart.Add(part);
            report.MdibVersion = (ulong) endpoint.MdibVersion;
            report.SequenceId = endpoint.SequenceId;
            return report;
        }

        /// <summary>   Handles the operation invoked event. </summary>
        ///
        /// <param name="oic">              The oic. </param>
        /// <param name="invocationState">  State of the invocation. </param>
        /// <param name="errorMsg">         Message describing the error. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task HandleOperationInvokedEvent(OperationInvocationContext oic, InvocationState invocationState,
            string? errorMsg)
        {
            var reportPart = new OperationInvokedReportReportPart();
            var info = new InvocationInfo
            {
                TransactionId = (uint) oic.TransactionId,
                InvocationState = invocationState
            };
            reportPart.InvocationInfo = info;
            reportPart.InvocationSource = new InstanceIdentifier();
            reportPart.OperationTarget = oic.OperationTarget;
            reportPart.OperationHandleRef = oic.OperationHandle;
            reportPart.InvocationSource.Type = null;
            if (!string.IsNullOrEmpty(errorMsg))
            {
                info.InvocationError = InvocationError.Oth;
                var text = new LocalizedText()
                {
                    Lang = "en-US",
                    Value = errorMsg
                };
                info.InvocationErrorMessage.Add(text);
            }
            var report = new OperationInvokedReport();
            report.ReportPart.Add(reportPart);
            report.MdibVersion = (ulong) endpoint.MdibVersion;
            report.SequenceId = endpoint.SequenceId;
            await FireOperationInvokedEventReport(report).ConfigureAwait(false);
        }

        /// <summary>   Handles the episodic context described by state. </summary>
        ///
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task HandleEpisodicContext(AbstractContextState state)
        {
            await FireEpisodicContextEventReport(GetContextReport<EpisodicContextReport>(state)).ConfigureAwait(false);
        }

        /// <summary>   Handles the periodic context described by state. </summary>
        ///
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task HandlePeriodicContext(AbstractContextState state)
        {
            await FirePeriodicContextEventReport(GetContextReport<PeriodicContextReport>(state)).ConfigureAwait(false);
        }

        /// <summary>   Gets context report. </summary>
        ///
        /// <typeparam name="T">    Generic type parameter. </typeparam>
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   The context report. </returns>
        public T GetContextReport<T>(AbstractContextState state) where T : AbstractContextReport
        {
            var report = Activator.CreateInstance<T>();
            var part = new AbstractContextReportReportPart();
            part.ContextState.Add(state);
            report.ReportPart.Add(part);
            report.MdibVersion = (ulong) endpoint.MdibVersion;
            report.SequenceId = endpoint.SequenceId;
            return report;
        }

        /// <summary>   Handles the stream described by state. </summary>
        ///
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task HandleStream(RealTimeSampleArrayMetricState state)
        {
            var stream = new WaveformStream();
            stream.State.Add(state);
            stream.SequenceId = endpoint.SequenceId;
            await SendStream(stream).ConfigureAwait(false);
        }
        
        /// <summary>   Fire episodic metric event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public abstract Task FireEpisodicMetricEventReport(EpisodicMetricReport report);
        /// <summary>   Fire episodic context event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public abstract Task FireEpisodicContextEventReport(EpisodicContextReport report);
        /// <summary>   Fire episodic alert event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public abstract Task FireEpisodicAlertEventReport(EpisodicAlertReport report);
        /// <summary>   Fire operation invoked event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public abstract Task FireOperationInvokedEventReport(OperationInvokedReport report);
        /// <summary>   Fire periodic metric event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public abstract Task FirePeriodicMetricEventReport(PeriodicMetricReport report);
        /// <summary>   Fire periodic context event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public abstract Task FirePeriodicContextEventReport(PeriodicContextReport report);
        /// <summary>   Fire periodic alert event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public abstract Task FirePeriodicAlertEventReport(PeriodicAlertReport report);
        /// <summary>   Sends a stream. </summary>
        ///
        /// <param name="stream">   The stream. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public abstract Task SendStream(WaveformStream stream);
    }
}