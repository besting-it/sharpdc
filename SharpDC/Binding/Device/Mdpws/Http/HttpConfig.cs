﻿// file:	Binding\Device\Mdpws\Http\HttpConfig.cs
//
// summary:	Implements the HTTP configuration class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;

namespace SharpDC.Binding.Device.Mdpws.Http
{
    /// <summary>   A base configuration. </summary>
    public class BaseConfig
    {
        /// <summary>   Gets or sets a value indicating whether this object use HTTPS. </summary>
        ///
        /// <value> True if use https, false if not. </value>
        public bool UseHttps { get; set; } = false;
        
        /// <summary>   Gets or sets the read write timeout milliseconds. </summary>
        ///
        /// <value> The read write timeout milliseconds. </value>
        public int ReadWriteTimeoutMs { get; set; } = 3000;
    }
    
    /// <summary>   A HTTP server configuration. </summary>
    public class HttpServerConfig : BaseConfig
    {
        /// <summary>   Gets or sets the port. </summary>
        ///
        /// <value> The port. </value>
        public int Port { get; set; }

        /// <summary>   Gets or sets the local endpoint filter. </summary>
        ///
        /// <value> The local endpoint filter. </value>
        public List<string> LocalEndpointFilter { get; } = new List<string>();
        
        /// <summary>   Gets or sets a value indicating whether this object use IPv6. </summary>
        ///
        /// <value> True if use ipv6, false if not. </value>

        public bool UseIpv6 { get; set; } = false;
        /// <summary>   Gets or sets the connection backlog. </summary>
        ///
        /// <value> The connection backlog. </value>

        public int ConnectionBacklog { get; set; } = 64;
        /// <summary>   Gets or sets the keep alive timeout milliseconds. </summary>
        ///
        /// <value> The keep alive timeout milliseconds. </value>
        /// 
        public int KeepAliveTimeoutMs { get; set; } = 7000;
        
        /// <summary>   Gets or sets options for controlling the ssl authentication. </summary>
        ///
        /// <value> Options that control the ssl authentication. </value>
        public SslAuthenticationOptions? SslAuthenticationOptions { get; set; }
    }
    
    /// <summary>   A HTTP client configuration. </summary>
    public class HttpClientConfig : BaseConfig
    {
        /// <summary>   Gets or sets options for controlling the ssl authentication. </summary>
        ///
        /// <value> Options that control the ssl authentication. </value>
        public SslAuthenticationOptions? SslAuthenticationOptions { get; set; }

        /// <summary>   Gets or sets the certificate validation callback. </summary>
        ///
        /// <value> The certificate validation callback. </value>
        public RemoteCertificateValidationCallback
            CertificateValidationCallback
        {
            get;
            set;
        } = OnValidationCallback;

        /// <summary>   Executes the validation callback action. </summary>
        ///
        /// <param name="sender">           Source of the event. </param>
        /// <param name="certificate">      The certificate. </param>
        /// <param name="chain">            The chain. </param>
        /// <param name="sslPolicyErrors">  The ssl policy errors. </param>
        ///
        /// <returns>   True if it succeeds, false if it fails. </returns>
        private static bool OnValidationCallback(object sender, X509Certificate certificate, X509Chain chain,
            SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
    }    
    
    /// <summary>   A ssl protocols extensions. </summary>
    public static class SslProtocolsExtensions
    {
        /// <summary>   The TLS 13. </summary>
        public const SslProtocols Tls13 = (SslProtocols)0x00003000;
    }

    /// <summary>   A ssl authentication options. </summary>
    public class SslAuthenticationOptions
    {
        /// <summary>   Gets or sets the certificates. </summary>
        ///
        /// <value> The certificates. </value>
        public List<X509Certificate2> Certificates { get; set; } = new List<X509Certificate2>();
        /// <summary>
        /// Gets or sets a value indicating whether this object is client certificate required.
        /// </summary>
        ///
        /// <value> True if this object is client certificate required, false if not. </value>
        public bool IsClientCertificateRequired { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this object is check certificate revocation.
        /// </summary>
        ///
        /// <value> True if this object is check certificate revocation, false if not. </value>
        public bool IsCheckCertificateRevocation { get; set; }
        /// <summary>   Gets or sets the protocols. </summary>
        ///
        /// <value> The protocols. </value>
        public SslProtocols Protocols { get; set; } = SslProtocols.None;
    }
}