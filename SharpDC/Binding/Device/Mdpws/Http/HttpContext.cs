﻿// file:	Binding\Device\Mdpws\Http\HttpContext.cs
//
// summary:	Implements the HTTP context class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace SharpDC.Binding.Device.Mdpws.Http
{
    /// <summary>   An HTTP context. </summary>
    public class HttpContext
    {
        /// <summary>   The client. </summary>
        private readonly TcpClient client;
        /// <summary>   The client stream. </summary>
        private Stream? clientStream;
        /// <summary>   True if is closed, false if not. </summary>
        private bool isClosed;
        /// <summary>   The timeout. </summary>
        private int timeout;

        /// <summary>   Gets or sets the request. </summary>
        ///
        /// <value> The request. </value>
        public HttpRequest Request { get; set; } = new HttpRequest();

        /// <summary>   Gets or sets the response. </summary>
        ///
        /// <value> The response. </value>
        public HttpResponse Response { get; set; } = new HttpResponse();

        /// <summary>   Gets the client. </summary>
        ///
        /// <value> The client. </value>
        private TcpClient Client => client;

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="client">   The client. </param>
        public HttpContext(TcpClient client)
        {
            this.client = client;
        }

        /// <summary>   Initializes this object. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="serverConfig"> The server configuration. </param>
        /// <param name="token">        A token that allows processing to be cancelled. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        internal async Task Init(HttpServerConfig serverConfig, CancellationToken token)
        {
            timeout = serverConfig.ReadWriteTimeoutMs;
            if (serverConfig.SslAuthenticationOptions?.Certificates.FirstOrDefault() != null && serverConfig.UseHttps)
            {
                var opt = serverConfig.SslAuthenticationOptions;
                if (opt.Certificates.Count > 1)
                    throw new InvalidOperationException("Server config can only have one certificate!");
                var sslStream = new SslStream(client.GetStream(), false);
                await sslStream.AuthenticateAsServerAsync(opt.Certificates.FirstOrDefault(), opt.IsClientCertificateRequired, opt.Protocols, 
                    opt.IsCheckCertificateRevocation).ConfigureAwait(false);
                clientStream = sslStream;
            }
            else
            {
                clientStream = client.GetStream();
            }

            Request = new HttpRequest()
            {
                LocalEndpoint = (IPEndPoint) Client.Client.LocalEndPoint
            };
            var requestInputStream = new HttpInputStream(Request, Client.Client, clientStream, token);
            Request.InputStream = requestInputStream;

            try
            {
                Request.RemoteEndpoint = (IPEndPoint) Client.Client.RemoteEndPoint;
            }
            catch (NullReferenceException)
            {
                Request.LocalEndpoint = new IPEndPoint(IPAddress.None, 0);
            }

            Response = new HttpResponse();
            var responseOutputStream = new HttpOutputStream(Response, clientStream);
            Response.OutputStream = responseOutputStream;
            Response.ResetStream = () =>
            {
                responseOutputStream.Reset();
            };

            client.SendTimeout = serverConfig.ReadWriteTimeoutMs;
            client.Client.SendTimeout = serverConfig.ReadWriteTimeoutMs;
            
            requestInputStream.RequestReady += InitResponse;
            await requestInputStream.Init(token, serverConfig.ReadWriteTimeoutMs, serverConfig.KeepAliveTimeoutMs, 
                false).ConfigureAwait(false);
        }

        /// <summary>   Initializes the response. </summary>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information. </param>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        private void InitResponse(object sender, EventArgs e)
        {
            Response.Reset();
            if (Request.Headers.ContainsKey(HttpConstants.KeyConnection) && 
                Request.Headers[HttpConstants.KeyConnection].ToLowerInvariant() == HttpConstants.ValueConnectionClose)
            {
                Response.IsKeepAlive = false;
            }
        }

        /// <summary>   Closes streams and client. </summary>
        ///
        /// <param name="flush">    (Optional) True to flush. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task Close(bool flush = true)
        {
            if (isClosed)
                return;
            if (flush && Response.OutputStream != null)
                await Response.OutputStream.FlushAsync().ConfigureAwait(false);
            clientStream?.Close();
            Response.OutputStream?.Close();
            Request.InputStream?.Close();
            Client.Close();
            isClosed = true;
        }

        /// <summary>   Gets a value indicating whether this context is keep alive. </summary>
        ///
        /// <value> True if this object is keep alive, false if not. </value>
        public bool IsKeepAlive => Request.IsKeepAlive && Response.IsKeepAlive;

        /// <summary>   Continues. </summary>
        ///
        /// <param name="token">                A token that allows processing to be cancelled. </param>
        /// <param name="continueTimeoutMs">    The continue timeout in milliseconds. </param>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <returns>   An asynchronous result. </returns>
        public async Task Continue(CancellationToken token, int continueTimeoutMs)
        {
            if (Request.InputStream == null || Response.OutputStream == null)
                throw new InvalidOperationException("Request or response not initialized.");            
            await Response.OutputStream.FlushAsync(token).ConfigureAwait(false);
            Request.Reset();
            await ((HttpInputStream)Request.InputStream).Init(token, timeout, 
                continueTimeoutMs, true).ConfigureAwait(false);
        }
        
    }
    
}