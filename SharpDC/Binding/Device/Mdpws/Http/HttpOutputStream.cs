﻿// file:	Binding\Device\Mdpws\Http\HttpOutputStream.cs
//
// summary:	Implements the HTTP output stream class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace SharpDC.Binding.Device.Mdpws.Http
{
    /// <summary>   A HTTP output stream. </summary>
    public class HttpOutputStream : Stream
    {
        /// <summary>   The default chunk size. </summary>
        public const int DefaultChunkSize = 4096;
        
        /// <summary>   The HTTP out. </summary>
        private readonly HttpOutput httpOut;
        /// <summary>   The stream. </summary>
        private readonly Stream stream;
        /// <summary>   True if is header written, false if not. </summary>
        private bool isHeaderWritten;
        /// <summary>   True if is closed, false if not. </summary>
        private bool isClosed;
        /// <summary>   True if is flushed, false if not. </summary>
        private bool isFlushed;

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="httpOut">  The HTTP out. </param>
        /// <param name="stream">   The stream. </param>
        public HttpOutputStream(HttpOutput httpOut, Stream stream)
        {
            this.httpOut = httpOut;
            this.stream = stream;
        }
        
        /// <summary>
        /// When overridden in a derived class, writes a sequence of bytes to the current stream and
        /// advances the current position within this stream by the number of bytes written.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="buffer">   An array of bytes. This method copies <paramref name="count" /> bytes
        ///                         from <paramref name="buffer" /> to the current stream. </param>
        /// <param name="offset">   The zero-based byte offset in <paramref name="buffer" /> at which to
        ///                         begin copying bytes to the current stream. </param>
        /// <param name="count">    The number of bytes to be written to the current stream. </param>
        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new InvalidOperationException("Only async methods are supported. Use WriteAsync instead.");
        }

        /// <summary>
        /// Asynchronously writes a sequence of bytes to the current stream, advances the current
        /// position within this stream by the number of bytes written, and monitors cancellation
        /// requests.
        /// </summary>
        ///
        /// <param name="buffer">   The buffer to write data from. </param>
        /// <param name="offset">   The zero-based byte offset in <paramref name="buffer" /> from which
        ///                         to begin copying bytes to the stream. </param>
        /// <param name="count">    The maximum number of bytes to write. </param>
        /// <param name="token">    The token to monitor for cancellation requests. The default value is
        ///                         <see cref="P:System.Threading.CancellationToken.None" />. </param>
        ///
        /// <returns>   A task that represents the asynchronous write operation. </returns>
        public override async Task WriteAsync(byte[] buffer, int offset, int count, CancellationToken token)
        {
            await CheckHeader(token).ConfigureAwait(false);
            if (httpOut.IsChunked)
            {
                if (count < DefaultChunkSize)
                {
                    await WriteStringCrLf("\r\n" + count.ToString("X"), token).ConfigureAwait(false);
                    await stream.WriteAsync(buffer, offset, count, token).ConfigureAwait(false);
                    Position += count;
                    return;
                }
                var source = new byte[count];
                Array.Copy(buffer, offset, source, 0, count);
                for (int i = 0; i < count; i += DefaultChunkSize)
                {
                    int targetSize = Math.Min(DefaultChunkSize, count - i);
                    var chunk = new byte[targetSize];
                    Array.Copy(source, i, chunk, 0, targetSize);
                    await WriteStringCrLf("\r\n" + targetSize.ToString("X"), token).ConfigureAwait(false);
                    await stream.WriteAsync(chunk, 0, targetSize, token).ConfigureAwait(false);
                    Position += targetSize;
                }
            }
            else
            {
                await stream.WriteAsync(buffer, offset, count, token).ConfigureAwait(false);
                Position += count;                
            }
        }

        /// <summary>   Check header. </summary>
        ///
        /// <param name="token">    (Optional) The token to monitor for cancellation requests. The
        ///                         default value is
        ///                         <see cref="P:System.Threading.CancellationToken.None" />. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task CheckHeader(CancellationToken token = default)
        {
            SetChunkedStatus();
            if (!isHeaderWritten)
            {
                Position += await WriteTopHeader(token);
                var finalHeaders = CreateFinalHeaders();
                foreach (var next in finalHeaders)
                    Position += await WriteHeaderField(next.Key.ToLowerInvariant(), next.Value, token).ConfigureAwait(false);
                // Separate header from body. In chunked case there's already a newline before the chunk length.
                if (!httpOut.IsChunked)
                    Position += await WriteStringCrLf(string.Empty, token).ConfigureAwait(false);
                isHeaderWritten = true;
            }
        }

        /// <summary>   Writes a top header. </summary>
        ///
        /// <param name="token">    The token to monitor for cancellation requests. The default value is
        ///                         <see cref="P:System.Threading.CancellationToken.None" />. </param>
        ///
        /// <returns>   An asynchronous result that yields an int. </returns>
        protected virtual async Task<int> WriteTopHeader(CancellationToken token)
        {
            var resp = httpOut as HttpResponse;
            if (resp == null)
                throw new InvalidOperationException("Response not available.");            
            return await WriteStringCrLf(HttpConstants.HttpProtocol + " " + resp.StatusCode + " " +
                resp.StatusDescription, token).ConfigureAwait(false);
        }

        /// <summary>   Sets chunked status. </summary>
        private void SetChunkedStatus()
        {
            if (!(httpOut is HttpResponse resp)) return;
            if (resp.StatusCode != 200)
                httpOut.IsChunked = false;
        }

        /// <summary>   Creates final headers. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <returns>   The new final headers. </returns>
        private Dictionary<string, string> CreateFinalHeaders()
        {
            var finalHeaders = new Dictionary<string, string>();
            if (httpOut.Headers.ContainsKey(HttpConstants.KeyTransferEncoding))
                throw new InvalidOperationException("Custom transfer encoding header field not allowed!");
            if (httpOut.Headers.ContainsKey(HttpConstants.KeyContentLength))
                throw new InvalidOperationException("Custom content length header field not allowed!");  
            
            if (httpOut.IsChunked)
                finalHeaders.Add(HttpConstants.KeyTransferEncoding, HttpConstants.ValueTransferEncodingChunked);
            else if (httpOut.ContentLength > 0)
                finalHeaders.Add(HttpConstants.KeyContentLength, httpOut.ContentLength.ToString());            
            
            if (httpOut.Headers.ContainsKey(HttpConstants.KeyContentType))
                throw new InvalidOperationException("Custom content type header field not allowed!");
            if (!string.IsNullOrEmpty(httpOut.ContentType) && (httpOut.ContentLength > 0 || httpOut.IsChunked))
                finalHeaders.Add(HttpConstants.KeyContentType, httpOut.ContentType);             
            if (httpOut.Headers.ContainsKey(HttpConstants.KeyConnection))
                throw new InvalidOperationException("Custom connection header field not allowed!");
            // When the connection field is missing, default is 'keepalive'.
            if (!httpOut.IsKeepAlive)
                finalHeaders.Add(HttpConstants.KeyConnection, HttpConstants.ValueConnectionClose);

            foreach (var next in httpOut.Headers)
                finalHeaders.Add(next.Key.ToLowerInvariant(), next.Value);
            
            return finalHeaders;
        }

        /// <summary>   Writes a header field. </summary>
        ///
        /// <param name="key">      The key. </param>
        /// <param name="value">    The desired length of the current stream in bytes. </param>
        /// <param name="token">    The token to monitor for cancellation requests. The default value is
        ///                         <see cref="P:System.Threading.CancellationToken.None" />. </param>
        ///
        /// <returns>   An asynchronous result that yields an int. </returns>
        private async Task<int> WriteHeaderField(string key, string value, CancellationToken token) 
            => await WriteStringCrLf(key + ": " + value, token).ConfigureAwait(false);

        /// <summary>   Writes a string plus carriage return line feed. </summary>
        ///
        /// <param name="str">      The string. </param>
        /// <param name="token">    The token to monitor for cancellation requests. The default value is
        ///                         <see cref="P:System.Threading.CancellationToken.None" />. </param>
        ///
        /// <returns>   An asynchronous result that yields an int. </returns>
        protected async Task<int> WriteStringCrLf(string str, CancellationToken token)
        {
            var bytes = httpOut.Charset.GetBytes(str + "\r\n");
            await stream.WriteAsync(bytes, 0, bytes.Length, token).ConfigureAwait(false);
            return bytes.Length;
        }

        /// <summary>
        /// When overridden in a derived class, clears all buffers for this stream and causes any
        /// buffered data to be written to the underlying device.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        public override void Flush()
        {
            throw new InvalidOperationException("Only async methods are supported. Use FlushAsync instead.");
        }

        /// <summary>
        /// Asynchronously clears all buffers for this stream, causes any buffered data to be written to
        /// the underlying device, and monitors cancellation requests.
        /// </summary>
        ///
        /// <exception cref="ObjectDisposedException">  Thrown when a supplied object has been disposed. </exception>
        ///
        /// <param name="token">    The token to monitor for cancellation requests. The default value is
        ///                         <see cref="P:System.Threading.CancellationToken.None" />. </param>
        ///
        /// <returns>   A task that represents the asynchronous flush operation. </returns>
        public override async Task FlushAsync(CancellationToken token)
        {
            if (isClosed)
                throw new ObjectDisposedException("Response stream is disposed.");
            if (isFlushed)
                return;    
            await CheckHeader(token).ConfigureAwait(false);
            if (httpOut.IsChunked)
                await WriteStringCrLf("\r\n0\r\n", token).ConfigureAwait(false);            
            await stream.FlushAsync(token).ConfigureAwait(false);
            isFlushed = true;
        }

        /// <summary>
        /// When overridden in a derived class, reads a sequence of bytes from the current stream and
        /// advances the position within the stream by the number of bytes read.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="buffer">   An array of bytes. When this method returns, the buffer contains the
        ///                         specified byte array with the values between
        ///                         <paramref name="offset" /> and (<paramref name="offset" /> +
        ///                         <paramref name="count" /> - 1) replaced by the bytes read from the
        ///                         current source. </param>
        /// <param name="offset">   The zero-based byte offset in <paramref name="buffer" /> at which to
        ///                         begin storing the data read from the current stream. </param>
        /// <param name="count">    The maximum number of bytes to be read from the current stream. </param>
        ///
        /// <returns>
        /// The total number of bytes read into the buffer. This can be less than the number of bytes
        /// requested if that many bytes are not currently available, or zero (0) if the end of the
        /// stream has been reached.
        /// </returns>
        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new InvalidOperationException("Reading not possible.");
        }

        /// <summary>
        /// When overridden in a derived class, sets the position within the current stream.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="offset">   A byte offset relative to the <paramref name="origin" /> parameter. </param>
        /// <param name="origin">   A value of type <see cref="T:System.IO.SeekOrigin" /> indicating the
        ///                         reference point used to obtain the new position. </param>
        ///
        /// <returns>   The new position within the current stream. </returns>
        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new InvalidOperationException("Seeking not possible.");
        }
        
        /// <summary>   When overridden in a derived class, sets the length of the current stream. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="value">    The desired length of the current stream in bytes. </param>
        public override void SetLength(long value)
        {
            throw new InvalidOperationException("Setting length not possible.");
        }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether the current stream
        /// supports reading.
        /// </summary>
        ///
        /// <value>
        /// <see langword="true" /> if the stream supports reading; otherwise, <see langword="false" />.
        /// </value>
        public override bool CanRead { get; } = false;
        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether the current stream
        /// supports seeking.
        /// </summary>
        ///
        /// <value>
        /// <see langword="true" /> if the stream supports seeking; otherwise, <see langword="false" />.
        /// </value>
        public override bool CanSeek { get; } = false;
        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether the current stream
        /// supports writing.
        /// </summary>
        ///
        /// <value>
        /// <see langword="true" /> if the stream supports writing; otherwise, <see langword="false" />.
        /// </value>
        public override bool CanWrite => stream.CanWrite;
        /// <summary>
        /// When overridden in a derived class, gets the length in bytes of the stream.
        /// </summary>
        ///
        /// <value> A long value representing the length of the stream in bytes. </value>
        public override long Length => httpOut.ContentLength;
        /// <summary>
        /// When overridden in a derived class, gets or sets the position within the current stream.
        /// </summary>
        ///
        /// <value> The current position within the stream. </value>
        public override long Position { get; set; }
        
        /// <summary>
        /// Closes the current stream and releases any resources (such as sockets and file handles)
        /// associated with the current stream. Instead of calling this method, ensure that the stream is
        /// properly disposed.
        /// </summary>
        public override void Close()
        {
            if (isClosed)
                return;
            stream.Close();
            isClosed = true;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.IO.Stream" /> and optionally
        /// releases the managed resources.
        /// </summary>
        ///
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        protected override void Dispose(bool disposing)
        {
            if (isClosed)
                return;
            Close();
        }

        /// <summary>   Resets this object. </summary>
        internal void Reset()
        {
            isHeaderWritten = false;
            isFlushed = false;
            Position = 0;
        }
    }
}