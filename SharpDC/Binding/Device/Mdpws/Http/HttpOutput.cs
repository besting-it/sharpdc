﻿// file:	Binding\Device\Mdpws\Http\HttpOutput.cs
//
// summary:	Implements the HTTP output class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace SharpDC.Binding.Device.Mdpws.Http
{
    /// <summary>   A HTTP output. </summary>
    public class HttpOutput : HttpInOut
    {
        /// <summary>   Gets or sets the output stream. </summary>
        ///
        /// <value> The output stream. </value>
        public Stream? OutputStream { get; set; }

        /// <summary>   Resets this object.
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// </summary>
        internal override void Reset()
        {
            base.Reset();
            Headers.Clear();
            if (ResetStream == null)
                throw new InvalidOperationException("No reset stream action set.");
            ResetStream();
        }        
        /// <summary>   Gets or sets the reset stream. </summary>
        ///
        /// <value> The reset stream. </value>
        internal Action? ResetStream { get; set; }     
        
    }
    
    /// <summary>   A HTTP response. </summary>
    public class HttpResponse : HttpOutput
    {
        /// <summary>   The default status description. </summary>
        private const string DefaultStatusDescription = "OK";

        /// <summary>   Gets or sets information describing the status. </summary>
        ///
        /// <value> Information describing the status. </value>
        public string StatusDescription { get; set; } = DefaultStatusDescription;
        /// <summary>   Gets or sets the status code. </summary>
        ///
        /// <value> The status code. </value>
        public int StatusCode { get; set; } = 200;        
        
        /// <summary>   Resets this object. </summary>
        internal override void Reset()
        {
            base.Reset();
            StatusDescription = DefaultStatusDescription;
            ContentType = DefaultContentType;
        }        
    }
    
}