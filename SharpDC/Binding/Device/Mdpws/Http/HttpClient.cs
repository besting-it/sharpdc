﻿// file:	Binding\Device\Mdpws\Http\HttpClient.cs
//
// summary:	Implements the HTTP client class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace SharpDC.Binding.Device.Mdpws.Http
{
    
    /// <summary>   An HTTP client. </summary>
    public static class HttpClient 
    {
        /// <summary>   Connects. </summary>
        ///
        /// <param name="targetUri">    URI of the target. </param>
        /// <param name="clientConfig"> The client configuration. </param>
        ///
        /// <returns>   An asynchronous result that yields a HttpClientRequest. </returns>
        public static async Task<HttpClientRequest> Connect(string targetUri, HttpClientConfig clientConfig)
        {
            var client = new TcpClient();
            var uri = new Uri(targetUri);
            await client.ConnectAsync(uri.Host, uri.Port).ConfigureAwait(false); 
            var authOptions = clientConfig.SslAuthenticationOptions;
            Stream clientStream;
            if (clientConfig.UseHttps && authOptions != null)
            {
                X509Certificate2Collection? certCollection = null;
                if (authOptions.Certificates.Count > 0)
                {
                    certCollection = new X509Certificate2Collection();
                    foreach (var cert in authOptions.Certificates)
                    {
                        certCollection.Add(cert);
                    }
                }
                var sslStream = new SslStream(client.GetStream(), false, 
                    clientConfig.CertificateValidationCallback);
                await sslStream.AuthenticateAsClientAsync(uri.Host, certCollection, 
                    authOptions.Protocols, authOptions.IsCheckCertificateRevocation).ConfigureAwait(false);
                clientStream = sslStream;
            }
            else
            {
                clientStream = client.GetStream();
            }

            var request = new HttpClientRequest(client, clientStream, clientConfig.ReadWriteTimeoutMs)
            {
                RequestPath = uri.AbsolutePath
            };
            request.Headers.Add(HttpConstants.KeyHost, uri.Host);
            var requestStream = new HttpClientRequestStream(request, clientStream);
            request.OutputStream = requestStream;
            request.ResetStream = () =>
            {
                requestStream.Reset();
            };
            client.SendTimeout = clientConfig.ReadWriteTimeoutMs;
            client.Client.SendTimeout = clientConfig.ReadWriteTimeoutMs;
            return request;
        }        
        
    }
    
    /// <summary>   An HTTP client request. </summary>
    public class HttpClientRequest : HttpOutput, IDisposable
    {
        /// <summary>   The default request path. </summary>
        private const string DefaultRequestPath = "/";
        
        /// <summary>   Gets or sets the request method. </summary>
        ///
        /// <value> The request method. </value>
        public HttpMethod RequestMethod { get; set; } = HttpMethod.Post;

        /// <summary>   Gets or sets the full pathname of the request file. </summary>
        ///
        /// <value> The full pathname of the request file. </value>
        public string RequestPath { get; set; } = DefaultRequestPath;
        
        /// <summary>   Gets or sets the client. </summary>
        ///
        /// <value> The client. </value>
        private TcpClient Client { get; set; }
        /// <summary>   Gets or sets the client stream. </summary>
        ///
        /// <value> The client stream. </value>
        private Stream ClientStream { get; set; }
        /// <summary>   Gets or sets the timeout. </summary>
        ///
        /// <value> The timeout. </value>
        private int Timeout { get; set; }
        
        /// <summary>   The response. </summary>
        private HttpClientResponse? response;

        internal HttpClientRequest(TcpClient client, Stream clientStream, int timeout)
        {
            Client = client;
            ClientStream = clientStream;
            Timeout = timeout;
        }

        /// <summary>   Gets a value indicating whether we can commit. </summary>
        ///
        /// <value> True if we can commit, false if not. </value>
        public bool CanCommit => response == null || 
            (response != null && IsKeepAlive && response.IsKeepAlive && Client.Client.Connected);
        
        /// <summary>   Commits the given request which flushes the output stream and initialized the
        ///             input stream.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="token">    A token that allows processing to be cancelled. </param>
        ///
        /// <returns>   An asynchronous result that yields a HttpClientResponse. </returns>
        public async Task<HttpClientResponse> Commit(CancellationToken token)
        {
            try
            {
                if (OutputStream == null)
                    throw new InvalidOperationException("Output stream not initialized.");
                await OutputStream.FlushAsync(token).ConfigureAwait(false);
                if (response != null)
                {
                    if (!IsKeepAlive || !response.IsKeepAlive)
                        throw new InvalidOperationException("Connection is not keep-alive.");
                    if (!Client.Client.Connected)
                        throw new InvalidOperationException("Connection is closed.");
                    response.Reset();
                    var existingStream = response.InputStream as HttpClientResponseStream;
                    if (existingStream == null)
                        throw new InvalidOperationException("Input stream not initialized.");
                    await existingStream.Init(token, Timeout, 
                        Timeout, 
                        true).ConfigureAwait(false);                      
                    return response;
                }
                response = new HttpClientResponse();
                var responseStream = new HttpClientResponseStream(response, Client.Client, ClientStream, token);
                response.InputStream = responseStream;
                response.LocalEndpoint = (IPEndPoint) Client.Client.LocalEndPoint;
                try
                {
                    response.RemoteEndpoint = (IPEndPoint) Client.Client.RemoteEndPoint;
                }
                catch (NullReferenceException)
                {
                    response.LocalEndpoint = new IPEndPoint(IPAddress.None, 0);
                }            
                await responseStream.Init(token, Timeout, Timeout, 
                    false).ConfigureAwait(false);
                return response;
            }
            finally
            {
                if (ResetStream == null)
                    throw new InvalidOperationException("Reset action not available.");                
                ResetStream();
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        public void Dispose()
        {
            ClientStream.Dispose();
            OutputStream?.Dispose();
            response?.InputStream?.Dispose();
            Client.Dispose();
        }

        /// <summary>   Closes this object. </summary>
        public void Close() => Dispose();
    }        
    
    /// <summary>   An HTTP client response. </summary>
    public class HttpClientResponse : HttpInput
    {
        /// <summary>   Gets the HTTP version. </summary>
        ///
        /// <value> The HTTP version. </value>
        public string HttpVersion => Segments[0];
        /// <summary>   Gets the status code. </summary>
        ///
        /// <value> The status code. </value>
        public int StatusCode => string.IsNullOrEmpty(Segments[1])? int.Parse(Segments[1]) : 0;
        /// <summary>   Gets information describing the status. </summary>
        ///
        /// <value> Information describing the status. </value>
        public string StatusDescription => Segments[2];
    } 
    
    /// <summary>   An HTTP client request stream. </summary>
    public class HttpClientRequestStream : HttpOutputStream
    {
        /// <summary>   The request. </summary>
        private readonly HttpClientRequest request;

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="request">  The request. </param>
        /// <param name="stream">   The stream. </param>
        public HttpClientRequestStream(HttpClientRequest request, Stream stream) : base(request, stream)
        {
            this.request = request;
        }

        /// <summary>   Writes a top header. </summary>
        ///
        /// <param name="token">    A token that allows processing to be cancelled. </param>
        ///
        /// <returns>   An asynchronous result that yields an int. </returns>
        protected override async Task<int> WriteTopHeader(CancellationToken token)
        {
            return await WriteStringCrLf(HttpCommon.GetMethodString(request.RequestMethod) + " " + request.RequestPath + " " +
                HttpConstants.HttpProtocol, token).ConfigureAwait(false);            
        }
    }

    /// <summary>   An HTTP client response stream. </summary>
    public class HttpClientResponseStream : HttpInputStream
    {
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="response">             The response. </param>
        /// <param name="socket">               The socket. </param>
        /// <param name="stream">               The stream. </param>
        /// <param name="cancellationToken">    A token that allows processing to be cancelled. </param>
        public HttpClientResponseStream(HttpClientResponse response, Socket socket, Stream stream, CancellationToken cancellationToken) : 
            base(response, socket, stream, cancellationToken)
        {
        }
    }
    
}