﻿// file:	Binding\Device\Mdpws\Http\HttpCommon.cs
//
// summary:	Implements the HTTP common class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Text;
using System.Web;

namespace SharpDC.Binding.Device.Mdpws.Http
{
    /// <summary>   Values that represent HTTP methods. </summary>
    public enum HttpMethod
    {
        /// <summary>   An enum constant representing the get option. </summary>
        Get,
        /// <summary>   An enum constant representing the post option. </summary>
        Post,
        /// <summary>   An enum constant representing the put option. </summary>
        Put,
        /// <summary>   An enum constant representing the delete option. </summary>
        Delete
    }

    /// <summary>   A HTTP common. </summary>
    public static class HttpCommon
    {
        /// <summary>   Gets method string. </summary>
        ///
        /// <param name="method">   The method. </param>
        /// <exception cref="ArgumentException">    Thrown when an invalid argument was passed. </exception>
        /// 
        /// <returns>   The method string. </returns>
        public static string GetMethodString(HttpMethod method)
        {
            switch(method)
            {
                case HttpMethod.Get: return "GET";
                case HttpMethod.Post: return "POST";
                case HttpMethod.Put: return "PUT";
                case HttpMethod.Delete: return "DELETE";
            }
            throw new ArgumentException("Unknown method");
        }
        
        /// <summary>   Adds a query parameters to 'arguments'. </summary>
        ///
        /// <param name="urlQuery">     The URL query. </param>
        /// <param name="arguments">    The arguments. </param>
        public static void AddQueryParams(string urlQuery, Dictionary<string, string> arguments)
        {
            if (urlQuery.Length < 4)
                return;
            var decoded = HttpUtility.UrlDecode(urlQuery).Substring(1);
            var kvParam = decoded.Split('&');
            foreach (var pair in kvParam)
            {
                var kv = pair.Split('=');
                if (kv.Length == 2)
                    arguments.Add(kv[0], kv[1]);
            }
        }
        
    }
    
    /// <summary>   Extension for TCP listener to monitor if it is active. </summary>
    public class TcpListenerEx : TcpListener
    {
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="localaddr">    The local address. </param>
        /// <param name="port">         The port. </param>
        public TcpListenerEx(IPAddress localaddr, int port) : base(localaddr, port)
        {
        }

        /// <summary>   Gets a value indicating whether this object is active. </summary>
        ///
        /// <value> True if this object is active, false if not. </value>
        public bool IsActive => Active;
    }

    /// <summary>   A HTTP constants. </summary>
    public static class HttpConstants
    {
        /// <summary>   The key charset. </summary>
        public const string KeyCharset = "charset";
        
        /// <summary>   Length of the key content. </summary>
        public const string KeyContentLength = "content-length";
        /// <summary>   Type of the key content. </summary>
        public const string KeyContentType = "content-type";
        /// <summary>   The value content type form. </summary>
        public const string ValueContentTypeForm = "application/x-www-form-urlencoded";
        
        /// <summary>   The key connection. </summary>
        public const string KeyConnection = "connection";
        /// <summary>   The value connection keep alive. </summary>
        public const string ValueConnectionKeepAlive = "keep-alive";
        /// <summary>   The value connection close. </summary>
        public const string ValueConnectionClose = "close";
        
        /// <summary>   The key transfer encoding. </summary>
        public const string KeyTransferEncoding = "transfer-encoding";
        /// <summary>   The value transfer encoding chunked. </summary>
        public const string ValueTransferEncodingChunked = "chunked";
        
        /// <summary>   The key host. </summary>
        public const string KeyHost = "host";        
        
        /// <summary>   The HTTP protocol. </summary>
        public const string HttpProtocol = "HTTP/1.1";
    }
    
    /// <summary>   (Serializable) exception for signalling HTTP parse errors. </summary>
    [Serializable]
    public class HttpParseException : Exception
    {
        /// <summary>   Default constructor. </summary>
        public HttpParseException()
        {
        }

        /// <summary>   Specialized constructor for use only by derived class. </summary>
        ///
        /// <param name="message">  The message. </param>
        public HttpParseException(string message) : base(message)
        {
        }

        /// <summary>   Specialized constructor for use only by derived class. </summary>
        ///
        /// <param name="message">          The message. </param>
        /// <param name="innerException">   The inner exception. </param>
        public HttpParseException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>   Specialized constructor for use only by derived class. </summary>
        ///
        /// <param name="info">     The information. </param>
        /// <param name="context">  The context. </param>
        protected HttpParseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
    
    /// <summary>   A HTTP in out. </summary>
    public class HttpInOut
    {
        /// <summary>   The default content type. </summary>
        protected const string DefaultContentType = "text/html";
        
        /// <summary>   Gets the headers. </summary>
        ///
        /// <value> The headers. </value>
        public Dictionary<string, string> Headers { get; } = new Dictionary<string, string>();

        /// <summary>   Gets or sets the type of the content. </summary>
        ///
        /// <value> The type of the content. </value>
        public string ContentType { get; set; } = DefaultContentType;
        /// <summary>   Gets or sets the content length. </summary>
        ///
        /// <value> The length of the content. </value>
        public long ContentLength { get; set; }
        /// <summary>   Gets or sets a value indicating whether this object is chunked. </summary>
        ///
        /// <value> True if this object is chunked, false if not. </value>
        public bool IsChunked { get; set; } = true;
        /// <summary>   Gets or sets a value indicating whether this object is keep alive. </summary>
        ///
        /// <value> True if this object is keep alive, false if not. </value>
        public bool IsKeepAlive { get; set; } = true;
        /// <summary>   Gets or sets the charset. </summary>
        ///
        /// <value> The charset. </value>
        public Encoding Charset { get; set; } = Encoding.UTF8;
        
        /// <summary>   Resets this object. </summary>
        internal virtual void Reset()
        {
            ContentType = string.Empty;
            ContentLength = 0;
            IsKeepAlive = true;
            Charset = Encoding.UTF8;
            Headers.Clear();
        }

    }    

}