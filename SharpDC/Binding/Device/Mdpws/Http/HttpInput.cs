﻿// file:	Binding\Device\Mdpws\Http\HttpInput.cs
//
// summary:	Implements the HTTP input class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace SharpDC.Binding.Device.Mdpws.Http
{
    
    /// <summary>   A HTTP input. </summary>
    public class HttpInput : HttpInOut
    {
        /// <summary>   The body. </summary>
        private string body = string.Empty;
        /// <summary>   Gets the body. </summary>
        ///
        /// <returns>   An asynchronous result that yields the body. </returns>
        public async Task<string> GetBody(CancellationToken token = default) => string.IsNullOrEmpty(body) ? 
            body = await this.GetEntityBody(token).ConfigureAwait(false) : body;

        /// <summary>   Gets or sets the segments. </summary>
        ///
        /// <value> The segments. </value>
        internal string[] Segments { get; set; } = {string.Empty, string.Empty, string.Empty};
        /// <summary>   Gets or sets the input stream. </summary>
        ///
        /// <value> The input stream. </value>
        public Stream? InputStream { get; set; }
        /// <summary>   Gets or sets a value indicating whether this object has entity body. </summary>
        ///
        /// <value> True if this object has entity body, false if not. </value>
        public bool HasEntityBody { get; set; } = true;
        /// <summary>   Gets or sets the local endpoint. </summary>
        ///
        /// <value> The local endpoint. </value>
        public IPEndPoint LocalEndpoint { get; set; } = new IPEndPoint(IPAddress.None, 0);
        /// <summary>   Gets or sets the remote endpoint. </summary>
        ///
        /// <value> The remote endpoint. </value>
        public IPEndPoint RemoteEndpoint { get; set; } = new IPEndPoint(IPAddress.None, 0);

        /// <summary>   Resets this object. </summary>
        internal override void Reset()
        {
            base.Reset();
            body = string.Empty;
            Segments = new [] {string.Empty, string.Empty, string.Empty};
            Headers.Clear();
        }

    }    

    /// <summary>   A HTTP request. </summary>
    public class HttpRequest : HttpInput
    {
        /// <summary>   Gets or sets options for controlling the path. </summary>
        ///
        /// <value> Options that control the path. </value>
        internal Dictionary<string, string> PathParams { get; set; } = new Dictionary<string, string>();

        /// <summary>   Gets the HTTP method. </summary>
        ///
        /// <value> The HTTP method. </value>
        public string HttpMethod => Segments[0];
        /// <summary>   Gets URL of the document. </summary>
        ///
        /// <value> The URL. </value>
        public string Url => Segments[1];
        /// <summary>   Gets the HTTP version. </summary>
        ///
        /// <value> The HTTP version. </value>
        public string HttpVersion => Segments[2];

        /// <summary>   Resets this object. </summary>
        internal override void Reset()
        {
            base.Reset();
            PathParams.Clear();
        }

    }
    
}