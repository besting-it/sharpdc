﻿// file:	Binding\Device\Mdpws\Http\HttpInputStream.cs
//
// summary:	Implements the HTTP input stream class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace SharpDC.Binding.Device.Mdpws.Http
{
    /// <summary>   A HTTP input stream. </summary>
    public class HttpInputStream : Stream
    {
        /// <summary>   Event queue for all listeners interested in RequestReady events. </summary>
        public event EventHandler? RequestReady;

        /// <summary>   The stream. </summary>
        private readonly Stream stream;        
        /// <summary>   The input. </summary>
        private readonly HttpInput input;
        /// <summary>   True if is header read, false if not. </summary>
        private bool isHeaderRead;
        /// <summary>   True if is chunked, false if not. </summary>
        private bool isChunked;
        /// <summary>   True if is closed, false if not. </summary>
        private bool isClosed;
        /// <summary>   The length. </summary>
        private long length;
        /// <summary>   The read timeout. </summary>
        private int readTimeout;
        /// <summary>   A token that allows processing to be cancelled. </summary>
        private CancellationToken cancellationToken;
        
        /// <summary>   The queue. </summary>
        private readonly ChunkedQueue queue;

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="input">                The input. </param>
        /// <param name="socket">               The socket. </param>
        /// <param name="stream">               The stream. </param>
        /// <param name="cancellationToken">    A token that allows processing to be cancelled. </param>
        public HttpInputStream(HttpInput input, Socket socket, Stream stream, 
            CancellationToken cancellationToken)
        {
            this.input = input;
            this.stream = stream;
            this.cancellationToken = cancellationToken;
            queue = new ChunkedQueue(stream, () =>
            {
                if (!socket.Connected)
                    return -1;
                return socket.Available;
            });
        }

        /// <summary>
        /// When overridden in a derived class, clears all buffers for this stream and causes any
        /// buffered data to be written to the underlying device.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        public override void Flush()
        {
            throw new InvalidOperationException("Flushing not possible.");
        }

        /// <summary>   Initializes this object. </summary>
        ///
        /// <exception cref="HttpParseException">   Thrown when a HTTP Parse error condition occurs. </exception>
        ///
        /// <param name="token">                The token to monitor for cancellation requests. The
        ///                                     default value is
        ///                                     <see cref="P:System.Threading.CancellationToken.None" />. </param>
        /// <param name="readTimeoutMs">        The read timeout in milliseconds. </param>
        /// <param name="keepAliveTimeoutMs">   The keep alive timeout in milliseconds. </param>
        /// <param name="isContinue">           True if is continue, false if not. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        internal async Task Init(CancellationToken token, int readTimeoutMs, int keepAliveTimeoutMs, bool isContinue)
        {
            cancellationToken = token;
            isHeaderRead = false;
            isChunked = false;
            length = 0;
            Position = 0;
            
            // For continuation, we need to get header lines using keep-alive timeout
            if (isContinue)
                readTimeout = keepAliveTimeoutMs;
            else
                readTimeout = readTimeoutMs;
            try
            {
                bool headerStartParsed = false;
                while (true)
                {
                    string requestLine = await GetNextLine().ConfigureAwait(false);
                    if (!headerStartParsed)
                    {
                        var segments = requestLine.Split(new[] {" "}, StringSplitOptions.None);
                        if (segments.Length != 3 || !requestLine.Contains("HTTP/")) continue;
                        input.Segments = segments;
                        headerStartParsed = true;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(requestLine))
                        {
                            if (input.Segments == null || input.Segments.Length < 3)
                                throw new HttpParseException("Error parsing http header.");
                            break;
                        }                    
                        if (!requestLine.Contains(":")) continue;
                        var segments = requestLine.Split(new[] {":"}, StringSplitOptions.None);
                        var key = segments[0].Trim().ToLowerInvariant();
                        if (input.Headers.ContainsKey(key))
                            throw new HttpParseException($"Duplicate header key found: {key}");
                        input.Headers.Add(key, segments[1].Trim());
                    }
                };
                bool hasContentLength;
                length = 0;
                
                input.HasEntityBody = (hasContentLength = input.Headers.ContainsKey(HttpConstants.KeyContentLength)) 
                    || input.Headers.ContainsKey(HttpConstants.KeyTransferEncoding);
                if (hasContentLength && !long.TryParse(input.Headers[HttpConstants.KeyContentLength], out length)) 
                    throw new HttpParseException("Error parsing content length.");
                input.ContentLength = length;
                if (input.Headers.ContainsKey(HttpConstants.KeyContentType))
                {
                    var contentType = input.Headers[HttpConstants.KeyContentType];
                    input.ContentType = contentType;
                    if (contentType.ToLowerInvariant().Contains(HttpConstants.KeyCharset) && contentType.Contains("="))
                    {
                        string charset = contentType.Substring(contentType.LastIndexOf("=", StringComparison.Ordinal) + 1).Trim();
                        try
                        {
                            input.Charset = Encoding.GetEncoding(charset);
                        }
                        catch(ArgumentException e)
                        {
                            throw new HttpParseException(e.Message);
                        }
                    }
                }
                input.IsKeepAlive = !input.Headers.ContainsKey(HttpConstants.KeyConnection) ||
                    input.Headers[HttpConstants.KeyConnection].ToLowerInvariant() == HttpConstants.ValueConnectionKeepAlive;
                isHeaderRead = true;
                isChunked = length == 0 && input.HasEntityBody;
                RequestReady?.Invoke(this, EventArgs.Empty);
            }
            finally
            {
                // Restore normal read timeout
                readTimeout = readTimeoutMs;
            }
        }     
        
        /// <summary>   Gets the next line. </summary>
        ///
        /// <returns>   An asynchronous result that yields the next line. </returns>
        private async Task<string> GetNextLine()
        {
            var sb = new StringBuilder();
            int eolCount = 0;
            while (eolCount < 2)
            {
                var next = await queue.Dequeue(cancellationToken, readTimeout).ConfigureAwait(false);
                char chr = (char) next;
                if (chr != '\r' && chr != '\n')
                    sb.Append(chr);
                else
                    eolCount++;
            }
            return sb.ToString();
        }
        
        /// <summary>
        /// When overridden in a derived class, reads a sequence of bytes from the current stream and
        /// advances the position within the stream by the number of bytes read.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="buffer">   An array of bytes. When this method returns, the buffer contains the
        ///                         specified byte array with the values between
        ///                         <paramref name="offset" /> and (<paramref name="offset" /> +
        ///                         <paramref name="count" /> - 1) replaced by the bytes read from the
        ///                         current source. </param>
        /// <param name="offset">   The zero-based byte offset in <paramref name="buffer" /> at which to
        ///                         begin storing the data read from the current stream. </param>
        /// <param name="count">    The maximum number of bytes to be read from the current stream. </param>
        ///
        /// <returns>
        /// The total number of bytes read into the buffer. This can be less than the number of bytes
        /// requested if that many bytes are not currently available, or zero (0) if the end of the
        /// stream has been reached.
        /// </returns>
        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new InvalidOperationException("Only async methods are supported. Use ReadAsync instead.");
        }

        /// <summary>
        /// Asynchronously reads a sequence of bytes from the current stream, advances the position
        /// within the stream by the number of bytes read, and monitors cancellation requests.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="buffer">   The buffer to write the data into. </param>
        /// <param name="offset">   The byte offset in <paramref name="buffer" /> at which to begin
        ///                         writing data from the stream. </param>
        /// <param name="count">    The maximum number of bytes to read. </param>
        /// <param name="token">    The token to monitor for cancellation requests. The default value is
        ///                         <see cref="P:System.Threading.CancellationToken.None" />. </param>
        ///
        /// <returns>
        /// A task that represents the asynchronous read operation. The value of the
        /// <paramref name="TResult" /> parameter contains the total number of bytes read into the
        /// buffer. The result value can be less than the number of bytes requested if the number of
        /// bytes currently available is less than the requested number, or it can be 0 (zero) if the end
        /// of the stream has been reached.
        /// </returns>
        public override async Task<int> ReadAsync(byte[] buffer, int offset, int count, CancellationToken token)
        {
            cancellationToken = token;
            if (!isHeaderRead)
                throw new InvalidOperationException("Http stream not initialized.");
            if (isChunked)
                return await ReadChunked(buffer, offset, count).ConfigureAwait(false);
            else
                return await ReadDefault(buffer, offset, count).ConfigureAwait(false);
        }
        
        /// <summary>   Reads a chunked. </summary>
        ///
        /// <param name="buffer">   An array of bytes. This method copies <paramref name="count" /> bytes
        ///                         from <paramref name="buffer" /> to the current stream. </param>
        /// <param name="offset">   The zero-based byte offset in <paramref name="buffer" /> at which to
        ///                         begin copying bytes to the current stream. </param>
        /// <param name="count">    The number of bytes to be written to the current stream. </param>
        ///
        /// <returns>   An asynchronous result that yields the chunked. </returns>
        private async Task<int> ReadChunked(byte[] buffer, int offset, int count)
        {
            if (Position == Length)
            {
                int nextLength = await GetNextChunkLength().ConfigureAwait(false);
                if (nextLength == 0)
                    return 0;
                length += nextLength;
            }
            return await ReadDefault(buffer, offset, count).ConfigureAwait(false);
        }
        
        /// <summary>   Reads a default. </summary>
        ///
        /// <param name="buffer">   An array of bytes. This method copies <paramref name="count" /> bytes
        ///                         from <paramref name="buffer" /> to the current stream. </param>
        /// <param name="offset">   The zero-based byte offset in <paramref name="buffer" /> at which to
        ///                         begin copying bytes to the current stream. </param>
        /// <param name="count">    The number of bytes to be written to the current stream. </param>
        ///
        /// <returns>   An asynchronous result that yields the default. </returns>
        private async Task<int> ReadDefault(byte[] buffer, int offset, int count)
        {
            int len = (int)Math.Min(count, Length - Position);
            if (len == 0)
                return 0;
            int targetSize = await queue.DequeueCopy(buffer, offset, len, cancellationToken, readTimeout).
                ConfigureAwait(false);
            Position += targetSize;
            return targetSize;
        }
        
        /// <summary>   Gets the next chunk length. </summary>
        ///
        /// <exception cref="HttpParseException">   Thrown when a HTTP Parse error condition occurs. </exception>
        ///
        /// <returns>   An asynchronous result that yields the next chunk length. </returns>
        private async Task<int> GetNextChunkLength()
        {
            string hexLen = await GetNextLine().ConfigureAwait(false);
            if (string.IsNullOrEmpty(hexLen))
            {
                return 0;
            }
            if (!int.TryParse(hexLen, NumberStyles.HexNumber, CultureInfo.InvariantCulture,
                out var len))
            {
                throw new HttpParseException("Invalid chunk length.");
            }
            return len;
        }

        /// <summary>
        /// When overridden in a derived class, sets the position within the current stream.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="offset">   A byte offset relative to the <paramref name="origin" /> parameter. </param>
        /// <param name="origin">   A value of type <see cref="T:System.IO.SeekOrigin" /> indicating the
        ///                         reference point used to obtain the new position. </param>
        ///
        /// <returns>   The new position within the current stream. </returns>
        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new InvalidOperationException("Seeking not possible.");
        }

        /// <summary>   When overridden in a derived class, sets the length of the current stream. </summary>
        ///
        /// <param name="value">    The desired length of the current stream in bytes. </param>
        public override void SetLength(long value)
        {
            length = value;
        }

        /// <summary>
        /// When overridden in a derived class, writes a sequence of bytes to the current stream and
        /// advances the current position within this stream by the number of bytes written.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="buffer">   An array of bytes. This method copies <paramref name="count" /> bytes
        ///                         from <paramref name="buffer" /> to the current stream. </param>
        /// <param name="offset">   The zero-based byte offset in <paramref name="buffer" /> at which to
        ///                         begin copying bytes to the current stream. </param>
        /// <param name="count">    The number of bytes to be written to the current stream. </param>
        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new InvalidOperationException("Writing not possible.");
        }

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether the current stream
        /// supports reading.
        /// </summary>
        ///
        /// <value>
        /// <see langword="true" /> if the stream supports reading; otherwise, <see langword="false" />.
        /// </value>
        public override bool CanRead => stream.CanRead;
        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether the current stream
        /// supports seeking.
        /// </summary>
        ///
        /// <value>
        /// <see langword="true" /> if the stream supports seeking; otherwise, <see langword="false" />.
        /// </value>
        public override bool CanSeek { get; } = false;
        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether the current stream
        /// supports writing.
        /// </summary>
        ///
        /// <value>
        /// <see langword="true" /> if the stream supports writing; otherwise, <see langword="false" />.
        /// </value>
        public override bool CanWrite { get; } = false;
        /// <summary>
        /// When overridden in a derived class, gets the length in bytes of the stream.
        /// </summary>
        ///
        /// <value> A long value representing the length of the stream in bytes. </value>
        public override long Length => length;
        /// <summary>
        /// When overridden in a derived class, gets or sets the position within the current stream.
        /// </summary>
        ///
        /// <value> The current position within the stream. </value>
        public override long Position { get; set; }
        
        /// <summary>
        /// Closes the current stream and releases any resources (such as sockets and file handles)
        /// associated with the current stream. Instead of calling this method, ensure that the stream is
        /// properly disposed.
        /// </summary>
        public override void Close()
        {
            if (isClosed)
                return;
            stream.Close();
            isClosed = true;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.IO.Stream" /> and optionally
        /// releases the managed resources.
        /// </summary>
        ///
        /// <param name="disposing">    <see langword="true" /> to release both managed and unmanaged
        ///                             resources; <see langword="false" /> to release only unmanaged
        ///                             resources. </param>
        protected override void Dispose(bool disposing) => Close();

    }
}