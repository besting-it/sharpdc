﻿// file:	Binding\Device\Mdpws\Http\HttpServer.cs
//
// summary:	Implements the HTTP server class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace SharpDC.Binding.Device.Mdpws.Http
{
    /// <summary>   An HTTP server. </summary>
    public class HttpServer : IDisposable
    {
       
        /// <summary>   A token that allows processing to be cancelled. </summary>
        private CancellationToken token;
        /// <summary>   The closer. </summary>
        private CancellationTokenRegistration closer;
        /// <summary>   The listener. </summary>
        private TcpListenerEx? listener;
        /// <summary>   The routes. </summary>
        private readonly IDictionary<string, Func<HttpRequest, HttpResponse, CancellationToken, Task>> routes 
            = new ConcurrentDictionary<string, Func<HttpRequest, HttpResponse, CancellationToken, Task>>();

        /// <summary>   The server configuration. </summary>
        private HttpServerConfig serverConfig = new HttpServerConfig();
        /// <summary>   True if templates used. </summary>
        private bool templatesUsed;
        /// <summary>   True if whitecard used. </summary>
        private bool whiteCardUsed;
        
        /// <summary>   The default error log. </summary>
        private readonly string DefaultErrorLog = "HttpError.log";
        /// <summary>   The method path separator. </summary>
        private static readonly string MethodPathSeparator = "|";

        /// <summary>   Event queue for all listeners interested in Failing events. </summary>
        public event EventHandler<HttpServerFailingEventArgs>? Failing; 

        /// <summary>   Starts. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="config">         The server configuration. </param>
        /// <param name="cancellationToken">    (Optional) A token that allows processing to be
        ///                                     cancelled. </param>
        public void Start(HttpServerConfig config, CancellationToken cancellationToken = default)
        {
            if (listener != null)
                throw new InvalidOperationException("Http server already started.");
            serverConfig = config;
            token = cancellationToken;
            listener = new TcpListenerEx(config.UseIpv6 ? IPAddress.IPv6Any : IPAddress.Any, config.Port);
            listener.Start(config.ConnectionBacklog);
            closer = cancellationToken.Register(Stop);
            StartLoop(config, listener);
        }

        /// <summary>   Stops this object. </summary>
        public void Stop() => Dispose();
        
        /// <summary>   Starts a loop. </summary>
        ///
        /// <param name="config"> The server configuration. </param>
        /// <param name="tcpListener">  The TCP listener. </param>
        private async void StartLoop(HttpServerConfig config, TcpListenerEx tcpListener)
        {
            while (!token.IsCancellationRequested && tcpListener.IsActive)
            {
                try
                {
                    var client = await tcpListener.AcceptTcpClientAsync().ConfigureAwait(false);
                    async void Process() 
                    {
                        var context = new HttpContext(client);
                        await SafeProcessContext(context, async () =>
                        {
                            await context.Init(config, token).ConfigureAwait(false);

                            if (config.LocalEndpointFilter.Any() && !config.LocalEndpointFilter.Any(ep =>
                                ep.Contains(GetRelevantAddressPart(client.Client.LocalEndPoint))))
                            {
                                context.Response.StatusCode = (int) HttpStatusCode.NotFound;
                                context.Response.StatusDescription = "Not Found";
                                context.Response.IsKeepAlive = false;
                                await SafeCloseContext(context).ConfigureAwait(false);
                                return;
                            }

                            await SafeHandleRequestAsync(context).ConfigureAwait(false);
                        }).ConfigureAwait(false);
                    }
                    Process();
                }
                catch (Exception e)
                {
                    Failing?.Invoke(this, new HttpServerFailingEventArgs(e));
                }
            }
        }

        /// <summary>   Gets relevant address part. </summary>
        ///
        /// <param name="ep">           The ep. </param>
        /// <param name="ipv6Segments"> (Optional) The IPv6 segments. </param>
        ///
        /// <returns>   The relevant address part. </returns>
        private static string GetRelevantAddressPart(EndPoint ep, int ipv6Segments = 4)
        {
            var addr = ep.ToString();
            if (addr.Count(c => c == ':') == 1)
                return addr;
            var ipv6Segs = addr.Split(new[] {':'}, StringSplitOptions.None);
            return string.Join(":", ipv6Segs.Take(ipv6Segments));
        }

        /// <summary>   Safe handle request asynchronous. </summary>
        ///
        /// <param name="context">  The context. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task SafeHandleRequestAsync(HttpContext context)
        {
            bool continueLoop;
            do
            {
                continueLoop = false;
                try
                {
                    await OnHttpRequest(context.Request, context.Response).ConfigureAwait(false);
                    if (context.Response.StatusCode >= 300)
                        context.Response.IsKeepAlive = false;
                }
                catch (Exception e)
                {
                    context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                    context.Response.StatusDescription = "Internal Server Error: " + e.Message;
                    await SafeCloseContext(context).ConfigureAwait(false);
                    Failing?.Invoke(this, new HttpServerFailingEventArgs(e));
                }
                finally
                {
                    if (context.IsKeepAlive)
                    {
                        await SafeProcessContext(context, async () =>
                        {
                            try
                            {
                                await context.Continue(token, serverConfig.KeepAliveTimeoutMs).ConfigureAwait(false);
                                token.ThrowIfCancellationRequested();
                                continueLoop = true;
                            }
                            catch (TimeoutException)
                            {
                                await CloseOnKeepAliveTimeout(context).ConfigureAwait(false);
                            }
                        }).ConfigureAwait(false);
                    }
                    else
                    {
                        await SafeCloseContext(context).ConfigureAwait(false);
                    }
                }
            } while (continueLoop);
        }

        /// <summary>   Closes on keep alive timeout. </summary>
        ///
        /// <param name="context">  The context. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task CloseOnKeepAliveTimeout(HttpContext context)
        {
            await SafeCloseContext(context, false).ConfigureAwait(false);
        }
        
        /// <summary>   Sends a timeout. </summary>
        ///
        /// <param name="context">  The context. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task SendTimeout(HttpContext context)
        {
            context.Response.StatusCode = (int) HttpStatusCode.RequestTimeout;
            context.Response.StatusDescription = "Request Timeout";
            context.Response.IsKeepAlive = false;
            context.Response.ContentType = string.Empty;
            context.Response.IsChunked = false;
            await SafeCloseContext(context).ConfigureAwait(false);
        }

        /// <summary>   Sends a bad request. </summary>
        ///
        /// <param name="context">  The context. </param>
        /// <param name="e">        An Exception to process. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task SendBadRequest(HttpContext context, Exception e)
        {
            context.Response.StatusCode = (int) HttpStatusCode.BadRequest;
            context.Response.StatusDescription = $"Bad Request: {e.Message}";
            context.Response.IsKeepAlive = false;
            await SafeCloseContext(context).ConfigureAwait(false);
        }
        
        /// <summary>   Safe process context. </summary>
        ///
        /// <param name="context">  The context. </param>
        /// <param name="task">     The task. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task SafeProcessContext(HttpContext context, Func<Task> task)
        {
            try
            {
                await task().ConfigureAwait(false);
            }
            catch (Exception e) when (e is TimeoutException)
            {
                 await SendTimeout(context).ConfigureAwait(false);
            }
            catch (Exception e) when (e is OperationCanceledException || e is ObjectDisposedException)
            {
                 await SafeCloseContext(context).ConfigureAwait(false);
            }
            catch (Exception e) when (e is HttpParseException || e is NotSupportedException)
            {
                 await SendBadRequest(context, e).ConfigureAwait(false);
            }    
            catch (IOException e)
            {
                Failing?.Invoke(this, new HttpServerFailingEventArgs(e));
                await SafeCloseContext(context).ConfigureAwait(false);
            }            
            catch (Exception e)
            {
                Failing?.Invoke(this, new HttpServerFailingEventArgs(e));
                await SafeCloseContext(context).ConfigureAwait(false);
            }
        }
        
        /// <summary>   Safe close context. </summary>
        ///
        /// <param name="context">  The context. </param>
        /// <param name="flush">    (Optional) True to flush. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task SafeCloseContext(HttpContext context, bool flush = true)
        {
            await ExceptionTools.ExceptionTrap(async () => { await context.Close(flush).ConfigureAwait(false); }).ConfigureAwait(false);
        }

        /// <summary>   Adds a route. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="path">     Full pathname of the file. </param>
        /// <param name="result">   The result. </param>
        /// <param name="method">   (Optional) The method. </param>
        public void AddRoute(string path,
            Func<HttpRequest, HttpResponse, CancellationToken, Task> result,
            HttpMethod method = HttpMethod.Post)
        {
            if (listener != null)
                throw new InvalidOperationException("Http server already started, route can't be added.");
            routes.Add(GetPathKey(path, method), result);
            if (path.Contains("{"))
                templatesUsed = true;
            if (path.Contains("*"))
                whiteCardUsed = true;
        }

        /// <summary>   Clears the routes. </summary>
        public void ClearRoutes()
        {
            if (listener != null)
                throw new InvalidOperationException("Http server already started, routes can't be cleared.");
            routes.Clear();
        } 

        /// <summary>   Gets path key. </summary>
        ///
        /// <param name="path">     Full pathname of the file. </param>
        /// <param name="method">   The method. </param>
        ///
        /// <returns>   The path key. </returns>
        private static string GetPathKey(string path, HttpMethod method) =>
            GetPathKey(path, HttpCommon.GetMethodString(method));

        /// <summary>   Gets path key. </summary>
        ///
        /// <param name="path">     Full pathname of the file. </param>
        /// <param name="method">   The method. </param>
        ///
        /// <returns>   The path key. </returns>
        private static string GetPathKey(string path, string method) => method + MethodPathSeparator + path;
        
        /// <summary>   Executes the HTTP request action. </summary>
        ///
        /// <param name="request">  The request. </param>
        /// <param name="response"> The response. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task OnHttpRequest(HttpRequest request, HttpResponse response)
        {
            var route = GetRoute(request);
            
            if (route == null)
            {
                response.StatusCode = (int)HttpStatusCode.NotFound;
                response.StatusDescription = "Not Found";
                response.IsKeepAlive = false;
                return;
            }
            try
            {
                await route(request, response, token).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                await using var sw = File.AppendText(DefaultErrorLog);
                await GenerateErrorReport(sw, request, e).ConfigureAwait(false);
                throw;
            }
        }
        
        /// <summary>   Gets a route. </summary>
        ///
        /// <param name="request">  The request. </param>
        ///
        /// <returns>   A function delegate that yields a Task. </returns>
        private Func<HttpRequest, HttpResponse, CancellationToken, Task>? GetRoute(HttpRequest request)
        {
            string key = GetPathKey(request.Url, request.HttpMethod);
            // Exact match
            if (!routes.TryGetValue(key, out var match))
            {
                // Whitecard
                if (whiteCardUsed)
                {
                    var routeKey = routes.Keys.FirstOrDefault(k =>
                    {
                        if (!k.EndsWith("*")) return false;
                        if (key + "*" == k)
                            return true;
                        int index = k.IndexOf('*');
                        return index < key.Length && k.Substring(0, index) == key.Substring(0, index);
                    });
                    if (routeKey != null)
                        return routes[routeKey];
                }
                // Template
                return GetTemplateRoute(request);
            }
            return match;
        }

        /// <summary>   Gets template route. </summary>
        ///
        /// <param name="request">  The request. </param>
        ///
        /// <returns>   A function delegate that yields a Task. </returns>
        private Func<HttpRequest, HttpResponse, CancellationToken, Task>? GetTemplateRoute(HttpRequest request)
        {
            if (!templatesUsed)
                return null;
            var templatePaths = routes.Keys.Select(k => k.Substring(k.IndexOf(MethodPathSeparator, 
                StringComparison.InvariantCulture) + 1)).Where(p => p.Contains('{'));
            var routesSegments = templatePaths.Select(p => p.Split(new [] {'/'}, 
                StringSplitOptions.None));
            var reqUrlSegments = request.GetUrlPath().Split(new [] {'/'}, 
                StringSplitOptions.None);
            var variables = new Dictionary<string, string>();
            var match = routesSegments.FirstOrDefault(r =>
            {
                // Segment count must match
                if (r.Length != reqUrlSegments.Length)
                    return false;
                // Each non-variable segment must match
                var i = 0;
                foreach (var next in r)
                {
                    if (next.Contains('{'))
                    {
                        variables.Add(next.Replace("{", string.Empty).
                            Replace("}", string.Empty).Trim(), reqUrlSegments[i]);
                    }
                    else if (next != reqUrlSegments[i])
                    {
                        variables.Clear();
                        return false;
                    }
                    i++;
                }
                return true;
            });
            if (match == null)
                return null;
            string key = GetPathKey(string.Join("/", match), request.HttpMethod);
            if (!routes.TryGetValue(key, out var result))
                return null;
            request.PathParams = variables;
            return result;
        }

        /// <summary>   Generates an error report. </summary>
        ///
        /// <param name="sw">           The software. </param>
        /// <param name="request">      The request. </param>
        /// <param name="exception">    The exception. </param>
        private static async Task GenerateErrorReport(StreamWriter sw, HttpRequest request, Exception exception)
        {
            await ExceptionTools.ExceptionTrap(async () =>
            {
                await sw.WriteLineAsync(DateTime.Now.ToString(CultureInfo.InvariantCulture)).ConfigureAwait(false);
                await sw.WriteLineAsync(await request.GetBody().ConfigureAwait(false)).ConfigureAwait(false);
                await sw.WriteLineAsync(exception.Message).ConfigureAwait(false);
                await sw.WriteLineAsync(exception.StackTrace).ConfigureAwait(false);
            });
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        public void Dispose()
        {
            if (listener != null && listener.IsActive)
                listener.Stop();
            listener = null;
            closer.Dispose();
        }
    }

    /// <summary>   Additional information for HTTP server failing events. </summary>
    public class HttpServerFailingEventArgs : EventArgs
    {
        public HttpServerFailingEventArgs(Exception exception)
        {
            Exception = exception;
        }
        
        /// <summary>   Gets or sets the exception. </summary>
        ///
        /// <value> The exception. </value>
        public Exception Exception { get; set; }
    }
}