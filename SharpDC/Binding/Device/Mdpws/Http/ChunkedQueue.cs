﻿// file:	Binding\Device\Mdpws\Http\ChunkedQueue.cs
//
// summary:	Implements the chunked queue class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace SharpDC.Binding.Device.Mdpws.Http 
 {
    /// <summary>   A chunked queue to query a stream. </summary>
    public class ChunkedQueue
    {
        
        /// <summary>   The queue. </summary>
        private readonly Queue<byte[]> queue = new Queue<byte[]>();
        /// <summary>   The current chunk. </summary>
        private byte[] currentChunk = Array.Empty<byte>();
        /// <summary>   The current index. </summary>
        private int currentIndex = 0;
        /// <summary>   The stream. </summary>
        private readonly Stream stream;
        /// <summary>   The available. </summary>
        private readonly Func<int> available;
        /// <summary>   The poll delay in milliseconds. </summary>
        private const int PollDelayMs = 10;

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="stream">       The stream. </param>
        /// <param name="available">    The available. </param>
        public ChunkedQueue(Stream stream, Func<int> available)
        {
            this.stream = stream;
            this.available = available;
        }

        /// <summary>   Wait for available data. </summary>
        ///
        /// <exception cref="IOException">      Thrown when an IO failure occurred. </exception>
        /// <exception cref="TimeoutException"> Thrown when a Timeout error condition occurs. </exception>
        ///
        /// <param name="token">    (Optional) A token that allows processing to be cancelled. </param>
        /// <param name="timeout">  (Optional) The timeout. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task WaitAvailable(CancellationToken token = default, int timeout = Int32.MaxValue)
        {
            int len = available();
            if (len > 0)
                await Enqueue(len, token).ConfigureAwait(false);
            if (currentIndex < currentChunk.Length)
                return;
            if (queue.Count == 0 && len <= 0)
            {
                var lastRead = DateTime.Now;
                while (!token.IsCancellationRequested)
                {
                    len = available();
                    if (len == 0)
                    {
                        await Task.Delay(PollDelayMs, token).ConfigureAwait(false);
                        if ((DateTime.Now - lastRead).TotalMilliseconds > timeout)
                            break;
                        continue;
                    }
                    if (len == -1)
                        throw new IOException();
                    await Enqueue(len, token).ConfigureAwait(false);
                    break;
                }
            }
            if (queue.Count == 0)
                throw new TimeoutException("No data available in time.");
            currentChunk = queue.Dequeue();
            currentIndex = 0;
        }

        /// <summary>   Adds an object onto the end of this queue. </summary>
        ///
        /// <param name="count">    Number of. </param>
        /// <param name="token">    (Optional) A token that allows processing to be cancelled. </param>
        /// <param name="timeout">  (Optional) The timeout. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task Enqueue(int count, CancellationToken token = default, int timeout = Int32.MaxValue)
        {
            stream.ReadTimeout = timeout;
            var streamBuffer = new byte[count];
            int read;
            if ((read = await stream.ReadAsync(streamBuffer, 0, count, token)
                .ConfigureAwait(false)) > 0)
            {
                Enqueue(streamBuffer, 0, read);
            }
        }
        
        /// <summary>   Adds an object onto the end of this queue. </summary>
        ///
        /// <param name="data">     The data. </param>
        /// <param name="offset">   The offset. </param>
        /// <param name="length">   The length. </param>
        private void Enqueue(byte[] data, int offset, int length)
        {
            if (offset == 0 && length == data.Length)
            {
                Enqueue(data);
                return;
            }
            var temp = new byte[length];
            Array.Copy(data, offset, temp, 0, length);
            Enqueue(temp);
        }

        /// <summary>   Adds an object onto the end of this queue. </summary>
        ///
        /// <param name="chunk">    The chunk. </param>
        private void Enqueue(byte[] chunk)
        {
            queue.Enqueue(chunk);
        }        

        /// <summary>   Removes the head object from this queue. </summary>
        ///
        /// <param name="token">    (Optional) A token that allows processing to be cancelled. </param>
        /// <param name="timeout">  (Optional) The timeout. </param>
        ///
        /// <returns>   An asynchronous result that yields the head object from this queue. </returns>
        public async Task<byte> Dequeue(CancellationToken token = default, int timeout = int.MaxValue)
        {
            await WaitAvailable(token, timeout).ConfigureAwait(false);
            var ret = currentChunk[currentIndex];
            currentIndex++;
            return ret;
        }

        /// <summary>   Dequeue copy. </summary>
        ///
        /// <param name="dest">     Destination for the data. </param>
        /// <param name="offset">   The offset. </param>
        /// <param name="length">   The length. </param>
        /// <param name="token">    (Optional) A token that allows processing to be cancelled. </param>
        /// <param name="timeout">  (Optional) The timeout. </param>
        ///
        /// <returns>   An asynchronous result that yields the actual number of bytes copied. </returns>
        public async Task<int> DequeueCopy(byte[] dest, int offset, int length, CancellationToken token = default, int timeout = Int32.MaxValue)
        {
            await WaitAvailable(token, timeout).ConfigureAwait(false);
            int len = Math.Min(length, currentChunk.Length - currentIndex);
            Array.Copy(currentChunk, currentIndex, dest, offset, len);
            currentIndex += len;
            return len;
        }   
        
    }
}