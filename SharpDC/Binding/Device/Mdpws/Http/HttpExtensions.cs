﻿// file:	Binding\Device\Mdpws\Http\HttpExtensions.cs
//
// summary:	Implements the HTTP extensions class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SharpDC.Binding.Device.Mdpws.Http
{
    /// <summary>   HTTP extensions. </summary>
    public static class HttpExtensions
    {
        private const int BufLength = 1024;
        
        /// <summary>   An HttpInput extension method that gets the entity body. </summary>
        /// 
        /// <param name="request">  The request. </param>
        /// <param name="token">    (Optional) A token that allows processing to be cancelled. </param>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///  
        /// <returns>   An asynchronous result that yields the entity body. </returns>
        internal static async Task<string> GetEntityBody(this HttpInput request, CancellationToken token = default)
        {
            if (!request.HasEntityBody)
                throw new InvalidOperationException("No entity body available.");
            if (request.InputStream == null)
                throw new InvalidOperationException("Request stream not initialized.");             
            var bytes = new byte[BufLength];
            var sb = new StringBuilder();
            int len;
            while ((len = await request.InputStream.ReadAsync(bytes, 0,bytes.Length, token).ConfigureAwait(false)) > 0)
            {
                var data = len == bytes.Length ? bytes : new byte[len];
                if (len != bytes.Length)
                    Array.Copy(bytes, 0, data, 0, len);
                sb.Append(request.Charset.GetString(data));
            }
            return sb.ToString();
        }
        
        /// <summary>   Get the Url path. </summary>
        ///
        /// <param name="request">  The request. </param>
        ///
        /// <returns>   The path. </returns>
        public static string GetUrlPath(this HttpRequest request)
        {
            int queryIndex = request.Url.IndexOf("?", StringComparison.InvariantCulture);
            if (queryIndex == -1)
                return request.Url;
            return request.Url.Substring(0, queryIndex);
        }        
        
        /// <summary>   An HttpOutput extension method that provides an http output as stream. </summary>
        ///
        /// <param name="output">   The output to act on. </param>
        ///
        /// <returns>   A Stream. </returns>
        public static Stream AsStream(this HttpOutput output)
        {
            if (!output.ContentType.Contains(HttpConstants.KeyCharset))
                output.ContentType += "; " + HttpConstants.KeyCharset + "=" + output.Charset.WebName;
            if (output.OutputStream == null)
                throw new InvalidOperationException("Stream not initialized.");            
            return output.OutputStream;
        }
        
        /// <summary>   An HttpInput extension method that provides an http input as stream. </summary>
        ///
        /// <param name="input">    The input to act on. </param>
        ///
        /// <returns>   A Stream. </returns>
        public static Stream AsStream(this HttpInput input)
        {
            if (input.InputStream == null)
                throw new InvalidOperationException("Stream not initialized.");               
            return input.InputStream;
        }        

        /// <summary>   An HttpResponse extension method that provides an http response as text. </summary>
        ///
        /// <param name="response"> The response to act on. </param>
        /// <param name="text">     The text. </param>
        /// <param name="token">    (Optional) A token that allows processing to be cancelled. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public static async Task AsText(this HttpResponse response, string text, CancellationToken token = default)
            => await response.AsText(text, null, null, true, token).ConfigureAwait(false);      

        /// <summary>   An HttpResponse extension method that provides an http response as text. </summary>
        ///
        /// <param name="response">     The response to act on. </param>
        /// <param name="text">         The text. </param>
        /// <param name="contentType">  (Optional) Type of the content. </param>
        /// <param name="charset">      (Optional) The charset. </param>
        /// <param name="chunked">      (Optional) True if chunked. </param>
        /// <param name="token">        (Optional) A token that allows processing to be cancelled. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public static async Task AsText(this HttpResponse response, string text, string? contentType = null, 
            string? charset = null, bool chunked = true, CancellationToken token = default)
        {
            if (contentType != null) response.ContentType = contentType;
            if (charset != null) response.Charset = Encoding.GetEncoding(charset);
            var data = response.Charset.GetBytes(text);
            response.ContentLength = chunked ? 0 : data.Length;
            response.IsChunked = chunked;
            var stream = response.AsStream();
            await stream.WriteAsync(data, 0, data.Length, token).ConfigureAwait(false);
            await stream.FlushAsync(token).ConfigureAwait(false);
        }

    }
}