﻿// file:	Binding\Device\Mdpws\WsConstants.cs
//
// summary:	Implements the ws constants class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

namespace SharpDC.Binding.Device.Mdpws
{
    /// <summary>   The ws constants. </summary>
    class WsConstants
    {

        /// <summary>   The namespace mdpws. </summary>
        public const string NamespaceMdpws = "http://standards.ieee.org/downloads/11073/11073-20702-2016";
        /// <summary>   The namespace binding. </summary>
        public const string NamespaceBinding = "http://standards.ieee.org/downloads/11073/11073-20701-2018";
        /// <summary>   Message describing the namespace. </summary>
        public const string NamespaceMessage = "http://standards.ieee.org/downloads/11073/11073-10207-2017/message";

        /// <summary>   The dpws 11. </summary>
        public const string Dpws11 = "http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01";
        /// <summary>   The dpws discovery. </summary>
        public const string DpwsDiscovery = "http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01";
        /// <summary>   The dpws addressing. </summary>
        public const string DpwsAddressing = "http://www.w3.org/2005/08/addressing"; 
        /// <summary>   The dpws eventing. </summary>
        public const string DpwsEventing = "http://schemas.xmlsoap.org/ws/2004/08/eventing"; 
    
        /// <summary>   The service identifier desc. </summary>
        public const string ServiceIdDesc = "DescriptionEventService";        
        /// <summary>   The service identifier get. </summary>
        public const string ServiceIdGet = "GetService";
        /// <summary>   Set the service identifier belongs to. </summary>
        public const string ServiceIdSet = "SetService";
        /// <summary>   The service identifier bicecps. </summary>
        public const string ServiceIdBic = "BicepsService";
        /// <summary>   The port type desc. </summary>
        public const string PortTypeDesc = "DescriptionEventService";        
        /// <summary>   The port type get. </summary>
        public const string PortTypeGet = "GetService";
        /// <summary>   Set the port type belongs to. </summary>
        public const string PortTypeSet = "SetService";
        /// <summary>   Context for the port type. </summary>
        public const string PortTypeCtx = "ContextService";
        /// <summary>   The port type event. </summary>
        public const string PortTypeEvent = "StateEventService";
        /// <summary>   The port type wave. </summary>
        public const string PortTypeWave = "WaveformService";

        /// <summary>   The filter oir. </summary>
        public const string FilterOir = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeSet + "/OperationInvokedReport";
        /// <summary>   The filter emr. </summary>
        public const string FilterEmr = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeEvent + "/EpisodicMetricReport";
        /// <summary>   The filter ecr. </summary>
        public const string FilterEcr = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeCtx + "/EpisodicContextChangedReport";
        /// <summary>   The filter ear. </summary>
        public const string FilterEar = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeEvent + "/EpisodicAlertReport";
        /// <summary>   The filter pmr. </summary>
        public const string FilterPmr = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeEvent + "/PeriodicMetricReport";
        /// <summary>   The filter pcr. </summary>
        public const string FilterPcr = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeCtx + "/PeriodicContextChangedReport";
        /// <summary>   The filter par. </summary>
        public const string FilterPar = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeEvent + "/PeriodicAlertReport";
        /// <summary>   The filter wfs. </summary>
        public const string FilterWfs = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeWave + "/WaveformStream";
    
        /// <summary>   The SOAP fault action. </summary>
        public const string SoapFaultAction = "http://www.w3.org/2005/08/addressing/fault";
        /// <summary>   The hello action. </summary>
        public const string HelloAction = "http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/Hello";
        /// <summary>   The bye action. </summary>
        public const string ByeAction = "http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/Bye";
        /// <summary>   The probe action. </summary>
        public const string ProbeAction = "http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/Probe";
        /// <summary>   The resolve action. </summary>
        public const string ResolveAction = "http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/Resolve";
        /// <summary>   The probe matches action. </summary>
        public const string ProbeMatchesAction = "http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/ProbeMatches";
        /// <summary>   The resolve matches action. </summary>
        public const string ResolveMatchesAction = "http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/ResolveMatches";
        /// <summary>   The get action. </summary>
        public const string GetAction = "http://schemas.xmlsoap.org/ws/2004/09/transfer/Get";
        /// <summary>   The get meta action. </summary>
        public const string GetMetaAction = "http://schemas.xmlsoap.org/ws/2004/09/mex/GetMetadata/Request";
        /// <summary>   The get response action. </summary>
        public const string GetResponseAction = "http://schemas.xmlsoap.org/ws/2004/09/transfer/GetResponse";
        /// <summary>   The get meta response action. </summary>
        public const string GetMetaResponseAction = "http://schemas.xmlsoap.org/ws/2004/09/mex/GetMetadata/Response";
        /// <summary>   The eventing prefix action. </summary>
        public const string EventingPrefixAction = "http://schemas.xmlsoap.org/ws/2004/08/eventing";        
        /// <summary>   The subscribe action. </summary>
        public const string SubscribeAction = "http://schemas.xmlsoap.org/ws/2004/08/eventing/Subscribe";
        /// <summary>   The subscribe response action. </summary>
        public const string SubscribeResponseAction = "http://schemas.xmlsoap.org/ws/2004/08/eventing/SubscribeResponse";
        /// <summary>   The renew action. </summary>
        public const string RenewAction = "http://schemas.xmlsoap.org/ws/2004/08/eventing/Renew";
        /// <summary>   The renew response action. </summary>
        public const string RenewResponseAction = "http://schemas.xmlsoap.org/ws/2004/08/eventing/RenewResponse";
        /// <summary>   The unsubscribe action. </summary>
        public const string UnsubscribeAction = "http://schemas.xmlsoap.org/ws/2004/08/eventing/Unsubscribe";
        /// <summary>   The unsubscribe response action. </summary>
        public const string UnsubscribeResponseAction = "http://schemas.xmlsoap.org/ws/2004/08/eventing/UnsubscribeResponse";
        /// <summary>   The get status action. </summary>
        public const string GetStatusAction = "http://schemas.xmlsoap.org/ws/2004/08/eventing/GetStatus";
        /// <summary>   The get status response action. </summary>
        public const string GetStatusResponseAction = "http://schemas.xmlsoap.org/ws/2004/08/eventing/GetStatusResponse";
    
        /// <summary>   The get mbid action. </summary>
        public const string GetMbidAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeGet + "/GetMdib";
        /// <summary>   The get mdib response action. </summary>
        public const string GetMdibResponseAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeGet + "/GetMdibResponse";
        /// <summary>   The get mddescription action. </summary>
        public const string GetMddescriptionAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeGet + "/GetMdDescription";
        /// <summary>   The get mddescription response action. </summary>
        public const string GetMddescriptionResponseAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeGet + "/GetMdDescriptionResponse";
        /// <summary>   The get md state action. </summary>
        public const string GetMdStateAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeGet + "/GetMdState";
        /// <summary>   The get md state response action. </summary>
        public const string GetMdStateResponseAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeGet + "/GetMdStateResponse";

        /// <summary>   The set value action. </summary>
        public const string SetValueAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeSet + "/SetValue";
        /// <summary>   The set value response action. </summary>
        public const string SetValueResponseAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeSet + "/SetValueResponse";
        /// <summary>   The activate action. </summary>
        public const string ActivateAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeSet + "/Activate";
        /// <summary>   The activate response action. </summary>
        public const string ActivateResponseAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeSet + "/ActivateResponse";
        /// <summary>   The set string action. </summary>
        public const string SetStringAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeSet + "/SetString";
        /// <summary>   The set string response action. </summary>
        public const string SetStringResponseAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeSet + "/SetStringResponse";
        /// <summary>   The set alert action. </summary>
        public const string SetAlertAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeSet + "/SetAlertState";
        /// <summary>   The set alert response action. </summary>
        public const string SetAlertResponseAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeSet + "/SetAlertStateResponse";
        /// <summary>   The set metric state action. </summary>
        public const string SetMetricStateAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeSet + "/SetMetricState";
        /// <summary>   The set metric state response action. </summary>
        public const string SetMetricStateResponseAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeSet + "/SetMetricStateResponse";
        /// <summary>   The set context action. </summary>
        public const string SetContextAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeCtx + "/SetContextState";
        /// <summary>   The set context response action. </summary>
        public const string SetContextResponseAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeCtx + "/SetContextStateResponse";
        /// <summary>   The get context action. </summary>
        public const string GetContextAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeCtx + "/GetContextStates";
        /// <summary>   The get context response action. </summary>
        public const string GetContextResponseAction = "http://standards.ieee.org/downloads/11073/11073-20701-2018/" + PortTypeCtx + "/GetContextStatesResponse";

        /// <summary>   The service wsdl get. </summary>
        public const string ServiceWsdlGet = "GetService.wsdl";
        /// <summary>   Set the service wsdl belongs to. </summary>
        public const string ServiceWsdlSet = "SetService.wsdl";
        /// <summary>   The service wsdl bic. </summary>
        public const string ServiceWsdlBic = "BicepsService.wsdl";
        
        /// <summary>   The XSD file extension point. </summary>
        public const string XsdFileExtensionPoint = "ExtensionPoint.xsd";
        /// <summary>   The XSD file message model. </summary>
        public const string XsdFileMessageModel = "BICEPS_MessageModel.xsd";
        /// <summary>   The XSD file participant model. </summary>
        public const string XsdFileParticipantModel = "BICEPS_ParticipantModel.xsd";
        /// <summary>   The XSD file mdpws. </summary>
        public const string XsdFileMdpws = "MDPWS.xsd";

        /// <summary>   The waveform service. </summary>
        public const string WaveformService =
            "http://standards.ieee.org/downloads/11073/11073-20701-2018/WaveformService";
        /// <summary>   The waveform streaming base address IPv4. </summary>
        public const string WaveformStreamingBaseAddrIpv4 =
            "soap.udp://239.239.239.235";        
        /// <summary>   The waveform streaming base address IPv6. </summary>
        public const string WaveformStreamingBaseAddrIpv6 =
            "soap.udp://[FF02:FC]";                   

    }
}
