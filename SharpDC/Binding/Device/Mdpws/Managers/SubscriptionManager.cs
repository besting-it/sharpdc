﻿// file:	Binding\Device\Mdpws\Managers\SubscriptionManager.cs
//
// summary:	Implements the subscription manager class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Extensions.Logging;
using SharpDC.Binding.Device.Mdpws.Http;
using SharpDC.Binding.Device.Mdpws.Messages;
using HttpClient = SharpDC.Binding.Device.Mdpws.Http.HttpClient;
using HttpMethod = SharpDC.Binding.Device.Mdpws.Http.HttpMethod;

namespace SharpDC.Binding.Device.Mdpws.Managers
{
    /// <summary>   Manager for subscriptions. </summary>
    public class SubscriptionManager
    {
        /// <summary>   The subscriptions. </summary>
        private readonly ConcurrentDictionary<string, SubscriptionInfo> subscriptions = new ConcurrentDictionary<string, SubscriptionInfo>();
        /// <summary>   The cts. </summary>
        private readonly CancellationTokenSource cts = new CancellationTokenSource();
        /// <summary>   The delay subscription check in milliseconds. </summary>
        private readonly int DelaySubscriptionCheckMs = 1000;
        /// <summary>   The handler. </summary>
        private readonly HttpClientHandler handler = new HttpClientHandler();
        /// <summary>   The sem. </summary>
        private readonly SemaphoreSlim sem = new SemaphoreSlim(1, 1);
        /// <summary>   The client configuration. </summary>
        private readonly HttpClientConfig clientConfig;
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="clientConfig"> The client configuration. </param>
        public SubscriptionManager(HttpClientConfig clientConfig)
        {
            this.clientConfig = clientConfig;
            Task.Run(async () =>
            {
                try
                {
                    while (!cts.IsCancellationRequested)
                    {
                        var expired = subscriptions.Where(s =>
                            s.Value.Remaining <= TimeSpan.Zero).Select(s => s.Key);
                        foreach (var next in expired)
                        {
                            subscriptions.TryRemove(next, out var exp);
                            SharpDc.Instance.Logger.LogTrace($"Subscription expired: {exp.NotifyTo}.");
                        }

                        await Task.Delay(DelaySubscriptionCheckMs).ConfigureAwait(false);
                    }
                }
                catch (OperationCanceledException)
                {
                }
                catch (Exception e)
                {
                    SharpDc.Instance.Logger.LogCritical(
                        $"Exception in subscription monitor: {e.Message}.");
                }
            });
        }
        
        /// <summary>   Handles the eventing requests. </summary>
        ///
        /// <param name="rq">       The request. </param>
        /// <param name="incoming"> The incoming message. </param>
        /// <param name="serviceAddr"> The service address. </param>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>  
        ///
        /// <returns>   An asynchronous result that yields a string. </returns>
        public async Task<string> HandleEventingRequests(HttpRequest rq, Message incoming, string serviceAddr)
        {
            try
            {
                var response = incoming.Action switch
                {
                    WsConstants.SubscribeAction => await HandleSubscribe(incoming, serviceAddr).ConfigureAwait(false),
                    WsConstants.UnsubscribeAction => await HandleUnsubscribe(incoming).ConfigureAwait(false),
                    WsConstants.GetStatusAction => await HandleGetStatus(incoming).ConfigureAwait(false),
                    WsConstants.RenewAction => await HandleRenew(incoming).ConfigureAwait(false),
                    _ => null
                };
                if (response != null)
                {
                    SharpDc.Instance.Logger.LogTrace($"Outgoing message: {response.Action} to {rq.RemoteEndpoint}.");
                    return response.ToString();
                }
            }
            catch (Exception e)
            {
                var fault = new Fault() { FaultString = e.Message };
                SharpDc.Instance.Logger.LogError($"Fault message: {fault.Action}, reason: {e.Message}, to {rq.RemoteEndpoint}.");
                return fault.ToString();
            }
            throw new InvalidOperationException("Wrong EPR, resolve handling not possible.");
        }

        /// <summary>   Handles the renew described by incoming message. </summary>
        ///
        /// <param name="incoming"> The incoming. </param>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>  
        ///
        /// <returns>   An asynchronous result that yields a Message. </returns>
        private Task<Message> HandleRenew(Message incoming)
        {
            var request = new Renew(incoming.Xml);
            bool infoSuccess = GetInfo(request, out var info, out var fault);
            if (!infoSuccess && fault != null)
                return fault;
            if (info == null)
                throw new InvalidOperationException("Error retrieving subscription info.");
            info.CreatedTime = DateTime.Now;
            var response = new RenewResponse()
            {
                RelatesTo = request.MessageId,
                Expires = XmlConvert.ToString(info.Remaining)
            };
            return Task.FromResult<Message>(response);  
        }
        
        /// <summary>   Gets subscription information. </summary>
        ///
        /// <param name="request">  The request. </param>
        /// <param name="info">     [out] The information. </param>
        /// <param name="fault">    [out] The fault. </param>
        ///
        /// <returns>   True if it succeeds, false if it fails. </returns>
        private bool GetInfo(Message request, out SubscriptionInfo? info, out Task<Message>? fault)
        {
            if (!subscriptions.ContainsKey(request.To))
            {
                fault = Task.FromResult<Message>(new Fault()
                    {FaultString = $"Subscription not found with reference {request.To}"});
                info = null;
                return false;
            }
            fault = null;
            info = subscriptions[request.To];
            return true;
        }

        /// <summary>   Handles the get status described by incoming message. </summary>
        ///
        /// <param name="incoming"> The incoming. </param>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>   
        ///
        /// <returns>   An asynchronous result that yields a Message. </returns>
        private Task<Message> HandleGetStatus(Message incoming)
        {
            var request = new GetStatus(incoming.Xml);
            bool infoSuccess = GetInfo(request, out var info, out var fault);
            if (!infoSuccess && fault != null)
                return fault;
            if (info == null)
                throw new InvalidOperationException("Error retrieving subscription info.");
            var response = new GetStatusResponse()
            {
                RelatesTo = request.MessageId,
                Expires = XmlConvert.ToString(info.Remaining)
            };
            return Task.FromResult<Message>(response);            
        }

        /// <summary>   Handles the unsubscribe described by incoming message. </summary>
        ///
        /// <param name="incoming"> The incoming. </param>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>  
        ///
        /// <returns>   An asynchronous result that yields a Message. </returns>
        private Task<Message> HandleUnsubscribe(Message incoming)
        {
            var request = new Unsubscribe(incoming.Xml);
            bool infoSuccess = GetInfo(request, out var info, out var fault);
            if (!infoSuccess && fault != null)
                return fault;
            if (info == null)
                throw new InvalidOperationException("Error retrieving subscription info.");
            subscriptions.TryRemove(info.Address, out _);
            var response = new UnsubscribeResponse()
            {
                RelatesTo = request.MessageId,
            };
            return Task.FromResult<Message>(response);  
        }

        /// <summary>   Handles the subscribe described by incoming message. </summary>
        ///
        /// <param name="incoming"> The incoming message. </param>
        /// <param name="serviceAddr"> The service address. </param> 
        ///
        /// <returns>   An asynchronous result that yields a Message. </returns>
        private Task<Message> HandleSubscribe(Message incoming, string serviceAddr)
        {
            var request = new Subscribe(incoming.Xml);
            string managerAddress = serviceAddr + "/" + Guid.NewGuid();
            var info = new SubscriptionInfo(request.Filter, XmlConvert.ToTimeSpan(request.Expires), DateTime.Now, request.NotifyToAddress,
                managerAddress);
            SharpDc.Instance.Logger.LogTrace($"Subscription request '{info.Filter}'");
            if (!subscriptions.TryAdd(info.Address, info))
                return Task.FromResult<Message>(new Fault()
                    { FaultString = $"Subscription already exists: {info.Address}" });
            var response = new SubscribeResponse()
            {
                RelatesTo = request.MessageId,
                Expires = request.Expires,
                SubscriptionManagerAddress = managerAddress
            };
            return Task.FromResult<Message>(response);
        }

        /// <summary>   Stops this object. </summary>
        public void Stop()
        {
            try
            {
                sem.Wait();
                cts.Cancel();
                cts.Dispose();
                subscriptions.Clear();
            }
            finally
            {
                sem.Release();
            }
            sem.Dispose();
        }
        
        /// <summary>   Sends a report. </summary>
        ///
        /// <param name="report">   The report. </param>
        /// <param name="action">   The action. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task SendReport(AbstractReport report, String action)
        {
            try
            {
                await sem.WaitAsync().ConfigureAwait(false);
                var targets = subscriptions.Values.Where(si => si.Filter.Contains(action, StringComparison.Ordinal));
                foreach (var next in targets)
                {
                    if (report is OperationInvokedReport oir)
                    {
                        SharpDc.Instance.Logger.LogTrace($"Operation invoked report about to be sent out: {string.Join(",", oir.ReportPart.Select(rp => rp.InvocationInfo.InvocationState))}.");
                    }
                    var body = GetConcreteBody(report);
                    var msg = new Invoke()
                    {
                        Action = action,
                        To = next.NotifyTo,
                        BodyXml = body
                    };
                    SharpDc.Instance.Logger.LogTrace($"Outgoing event: {action} to {next.NotifyTo}.");
                    var response = await PostAsync(next.NotifyTo,
                        new StringContent(msg.ToString(), Encoding.Default, "application/soap+xml"), cts.Token)
                        .ConfigureAwait(false);
                    if (response.StatusCode > 204)
                    {
                        SharpDc.Instance.Logger.LogWarning(
                            $"Invocation of {next.NotifyTo} returned status {response}.");
                    }
                }
            }
            catch (Exception e) when (e is OperationCanceledException || e is ObjectDisposedException)
            {
            }
            catch (Exception e)
            {
                SharpDc.Instance.Logger.LogWarning(
                    $"Send report exception: {e.Message}");
            }            
            finally
            {
                sem.Release();
            }
        }

        /// <summary>   Posts a message to an http server. </summary>
        ///
        /// <param name="nextNotifyTo">     The next notify to. </param>
        /// <param name="stringContent">    The string content. </param>
        /// <param name="token">            A token that allows processing to be cancelled. </param>
        ///
        /// <returns>   An asynchronous result that yields the post. </returns>
        private async Task<(string Response, int StatusCode)> PostAsync(string nextNotifyTo, StringContent stringContent, 
            CancellationToken token)
        {
            using var request = await HttpClient.Connect(nextNotifyTo, clientConfig).ConfigureAwait(false);
            request.RequestMethod = HttpMethod.Post;
            request.IsKeepAlive = false;
            request.ContentType = stringContent.Headers.ContentType.MediaType;
            request.ContentLength = stringContent.Headers.ContentLength ?? 0;
            request.IsChunked = request.ContentLength == 0;
            await stringContent.CopyToAsync(request.AsStream()).ConfigureAwait(false);
            var response = await request.Commit(token).ConfigureAwait(false);
            return (response.StatusDescription, response.StatusCode);
        }

        /// <summary>   Gets concrete body. </summary>
        ///
        /// <exception cref="NotSupportedException">    Thrown when the requested operation is not
        ///                                             supported. </exception>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   The concrete body. </returns>
        private static string GetConcreteBody(AbstractReport report)
        {
            var body = report switch
            {
                OperationInvokedReport invokedReport => invokedReport.Serialize(),
                EpisodicMetricReport metricReport => metricReport.Serialize(),
                EpisodicAlertReport alertReport => alertReport.Serialize(),
                EpisodicContextReport contextReport => contextReport.Serialize(),
                PeriodicMetricReport metricReport => metricReport.Serialize(),
                PeriodicAlertReport alertReport => alertReport.Serialize(),
                PeriodicContextReport contextReport => contextReport.Serialize(),
                WaveformStream streamReport => streamReport.Serialize(),
                _ => throw new NotSupportedException("Concrete report type not supported!")
            };
            return body;
        }
    }
}