﻿// file:	Binding\Device\Mdpws\Managers\SubscriptionInfo.cs
//
// summary:	Implements the subscription information class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Xml.Linq;

namespace SharpDC.Binding.Device.Mdpws.Managers
{
    /// <summary>   Information about the subscription. </summary>
    public class SubscriptionInfo
    {
        public SubscriptionInfo(string filter, TimeSpan duration, DateTime createdTime, string notifyTo, string address)
        {
            Filter = filter;
            Duration = duration;
            CreatedTime = createdTime;
            NotifyTo = notifyTo;
            Address = address;
        }

        /// <summary>   Gets or sets the filter. </summary>
        ///
        /// <value> The filter. </value>
        public string Filter { get; set; }
        /// <summary>   Gets or sets the notify to. </summary>
        ///
        /// <value> The notify to. </value>
        public string NotifyTo { get; set; }
        /// <summary>   Gets or sets the duration. </summary>
        ///
        /// <value> The duration. </value>
        public TimeSpan Duration { get; set; }
        /// <summary>   Gets or sets the created time. </summary>
        ///
        /// <value> The created time. </value>
        public DateTime CreatedTime { get; set; }
        /// <summary>   Gets or sets the identifier. </summary>
        ///
        /// <value> The identifier. </value>
        public string Address { get; set; }
        /// <summary>   Gets the remaining. </summary>
        ///
        /// <value> The remaining. </value>
        public TimeSpan Remaining => CreatedTime + Duration - DateTime.Now;

        /// <summary>   Determines whether the specified object is equal to the current object. </summary>
        ///
        /// <param name="obj">  The object to compare with the current object. </param>
        ///
        /// <returns>
        /// <see langword="true" /> if the specified object  is equal to the current object; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj is SubscriptionInfo si)
                return si.Address.Equals(Address);
            return false;
        }

        /// <summary>   Serves as the default hash function. </summary>
        ///
        /// <returns>   A hash code for the current object. </returns>
        public override int GetHashCode()
        {
            return Address.GetHashCode();
        }
    }
}