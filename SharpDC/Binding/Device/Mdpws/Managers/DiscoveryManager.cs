﻿// file:	Binding\Device\Mdpws\Managers\DiscoveryManager.cs
//
// summary:	Implements the discovery manager class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SharpDC.Binding.Device.Mdpws.Messages;
using SharpDC.Binding.Device.Mdpws.Service;
using SharpDC.Misc;
using SharpDC.Provider;

namespace SharpDC.Binding.Device.Mdpws.Managers
{
    /// <summary>   Manager for discoveries. </summary>
    public class DiscoveryManager : IDisposable
    {
        /// <summary>   The UDP. </summary>
        private UdpMessenger udp;
        /// <summary>   The data. </summary>
        private SdcProviderData data;
        /// <summary>   List of identifiers for the messages. </summary>
        private ConcurrentQueue<string> msgIds = new ConcurrentQueue<string>();
        /// <summary>   The maximum size of the queue. </summary>
        private const int MaxQueueSize = 64;
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="data"> The data. </param>
        /// <param name="udp">  The UDP. </param>
        public DiscoveryManager(SdcProviderData data, UdpMessenger udp)
        {
            this.data = data;
            this.udp = udp;
            udp.MulticastReceived += OnMulticastMessage;
            data.PropertyChanged += OnPropertyChanged;
        }

        /// <summary>   Raises the property changed event. </summary>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private async void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(data.MetadataVersion))
                return;            
            try
            {
                await SendHello().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                SharpDc.Instance.Logger.LogError(ex, "Error sending UDP message");
            }
        }

        /// <summary>   Raises the message received event. </summary>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnMulticastMessage(object sender, MessageReceivedEventArgs e)
        {
            var incoming = new Message(e.IncomingContent);
            if (incoming.Action != WsConstants.ProbeAction && incoming.Action != WsConstants.ResolveAction && msgIds.Contains(incoming.MessageId))
            {
                SharpDc.Instance.Logger.LogTrace("Message skipped due to Id, action: {0}", incoming.Action);
                CheckIdQueueSize();
                return;
            }
            msgIds.Enqueue(incoming.MessageId);
            SharpDc.Instance.Logger.LogTrace("Incoming message: {0}", incoming.Action);
            try
            {
                Message outgoing;
                switch (incoming.Action)
                {
                    case WsConstants.ProbeAction:
                        var probe = new Probe(e.IncomingContent);
                        outgoing = HandleProbe(probe, data);
                        break;
                    case WsConstants.ResolveAction:
                        var resolve = new Resolve(e.IncomingContent);
                        outgoing = HandleResolve(resolve, data);
                        break;
                    default:
                        throw new InvalidOperationException("Incoming action not supported.");
                }
                SharpDc.Instance.Logger.LogTrace("Outgoing message: {0}", outgoing.Action);
                e.DirectResponse(StringTools.RemoveCrLf(outgoing.ToString()));
            }
            catch (InvalidOperationException)
            {
                // Invalid message requirements (e.g. Xml field missing)
            }
            CheckIdQueueSize();         
        }

        /// <summary>   Handles the resolve. </summary>
        ///
        /// <param name="resolve">  The resolve. </param>
        /// <param name="data">     The data. </param>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception> 
        /// 
        /// <returns>   The ResolveMatches. </returns>
        internal static ResolveMatches HandleResolve(Resolve resolve, SdcProviderData data)
        {
            if (resolve.Epr == data.EndpointReference)
            {
                data.AppSequence.IncrementMessageNumber();
                return new ResolveMatches()
                {
                    Epr = data.EndpointReference,
                    MetadataVersion = data.MetadataVersion,
                    AppSequence = data.AppSequence,
                    Xaddrs = data.Xaddrs,
                    RelatesTo = resolve.MessageId
                };
            }
            throw new InvalidOperationException("Wrong EPR, resolve handling not possible.");
        }

        /// <summary>   Handles the probe. </summary>
        ///
        /// <param name="probe">    The probe. </param>
        /// <param name="data">     The data. </param>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>  
        ///
        /// <returns>   The ProbeMatches. </returns>
        internal static ProbeMatches HandleProbe(Probe probe, SdcProviderData data)
        {
            string types = "MedicalDevice";
            try
            {
                types = probe.Types;
            }
            catch (InvalidOperationException)
            {
                // No types
            }
            if (types.Contains("MedicalDevice"))
            {
                data.AppSequence.IncrementMessageNumber();
                return new ProbeMatches()
                {
                    Epr = data.EndpointReference,
                    MetadataVersion = data.MetadataVersion,
                    AppSequence = data.AppSequence,
                    Xaddrs = data.Xaddrs,
                    RelatesTo = probe.MessageId
                };
            }
            throw new InvalidOperationException("Wrong EPR, probe handling not possible.");
        }

        /// <summary>   Check identifier queue size. </summary>
        private void CheckIdQueueSize()
        {
            while (msgIds.Count > MaxQueueSize)
                msgIds.TryDequeue(out _);
        }

        /// <summary>   Sends the hello. </summary>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task SendHello()
        {
            data.AppSequence.IncrementMessageNumber();
            data.MetadataVersion++;
            var msg = new Hello()
            {
                Epr = data.EndpointReference,
                MetadataVersion = data.MetadataVersion,
                AppSequence = data.AppSequence,
                Xaddrs = data.Xaddrs
            };
            SharpDc.Instance.Logger.LogTrace("Send hello from Epr: {0}", data.EndpointReference);
            msgIds.Enqueue(msg.MessageId);
            await udp.SendMulticast(StringTools.RemoveCrLf(msg.ToString())).ConfigureAwait(false);
        }

        /// <summary>   Sends the bye. </summary>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task SendBye()
        {
            data.AppSequence.IncrementMessageNumber();
            var msg = new Bye()
            {
                Epr = data.EndpointReference,
                MetadataVersion = data.MetadataVersion,
                AppSequence = data.AppSequence
            };
            msgIds.Enqueue(msg.MessageId);
            SharpDc.Instance.Logger.LogTrace("Send bye from Epr: {0}", data.EndpointReference);
            await udp.SendMulticast(StringTools.RemoveCrLf(msg.ToString())).ConfigureAwait(false);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        public void Dispose()
        {
            udp.MulticastReceived -= OnMulticastMessage; 
            data.PropertyChanged -= OnPropertyChanged;
        }
    }
}