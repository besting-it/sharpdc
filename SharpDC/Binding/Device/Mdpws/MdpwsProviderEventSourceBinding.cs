﻿// file:	Binding\Device\Mdpws\MdpwsProviderEventSourceBinding.cs
//
// summary:	Implements the mdpws provider event source binding class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Threading.Tasks;
using SharpDC.Binding.Device.Mdpws.Service.Routes;
using SharpDC.Provider;

namespace SharpDC.Binding.Device.Mdpws
{
    /// <summary>   The mdpws provider event source binding. </summary>
    public class MdpwsProviderEventSourceBinding : BicepsProviderEventSourceBinding
    {
        /// <summary>   The set service route. </summary>
        private readonly SetServiceRoute setServiceRoute;
        /// <summary>   The bic service route. </summary>
        private readonly BicServiceRoute bicServiceRoute;
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="endpoint"> The endpoint. </param>
        /// <param name="data">     The data. </param>
        /// <param name="setRoute"> The set route. </param>
        /// <param name="bicRoute"> The bic route. </param>
        public MdpwsProviderEventSourceBinding(ISdcEndpoint endpoint, SdcProviderData data, SetServiceRoute setRoute, BicServiceRoute bicRoute) : base(endpoint, data)
        {
            setServiceRoute = setRoute;
            bicServiceRoute = bicRoute;
        }

        /// <summary>   Fire episodic metric event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public override async Task FireEpisodicMetricEventReport(EpisodicMetricReport report)
        {
            await bicServiceRoute.SubscriptionManager.SendReport(report, WsConstants.FilterEmr).ConfigureAwait(false);
        }

        /// <summary>   Fire episodic context event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public override async Task FireEpisodicContextEventReport(EpisodicContextReport report)
        {
            await bicServiceRoute.SubscriptionManager.SendReport(report, WsConstants.FilterEcr).ConfigureAwait(false);
        }

        /// <summary>   Fire episodic alert event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public override async Task FireEpisodicAlertEventReport(EpisodicAlertReport report)
        {
            await bicServiceRoute.SubscriptionManager.SendReport(report, WsConstants.FilterEar).ConfigureAwait(false);
        }

        /// <summary>   Fire operation invoked event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public override async Task FireOperationInvokedEventReport(OperationInvokedReport report)
        {
            await setServiceRoute.SubscriptionManager.SendReport(report, WsConstants.FilterOir).ConfigureAwait(false);
        }

        /// <summary>   Fire periodic metric event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public override async Task FirePeriodicMetricEventReport(PeriodicMetricReport report)
        {
            await bicServiceRoute.SubscriptionManager.SendReport(report, WsConstants.FilterPmr).ConfigureAwait(false);
        }

        /// <summary>   Fire periodic context event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public override async Task FirePeriodicContextEventReport(PeriodicContextReport report)
        {
            await bicServiceRoute.SubscriptionManager.SendReport(report, WsConstants.FilterPcr).ConfigureAwait(false);
        }

        /// <summary>   Fire periodic alert event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public override async Task FirePeriodicAlertEventReport(PeriodicAlertReport report)
        {
            await bicServiceRoute.SubscriptionManager.SendReport(report, WsConstants.FilterPar).ConfigureAwait(false);
        }

        /// <summary>   Sends a stream. </summary>
        ///
        /// <param name="stream">   The stream. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public override async Task SendStream(WaveformStream stream)
        {
            await bicServiceRoute.SubscriptionManager.SendReport(stream, WsConstants.FilterWfs).ConfigureAwait(false);
        }
    }
}