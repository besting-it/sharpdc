﻿// file:	Binding\Device\Mdpws\MdpwsDeviceBinding.cs
//
// summary:	Implements the mdpws device binding class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SharpDC.Binding.Device.Mdpws.Managers;
using SharpDC.Binding.Device.Mdpws.Service;
using SharpDC.Binding.Device.Mdpws.Service.Routes;

namespace SharpDC.Binding.Device.Mdpws
{
    /// <summary>   The mdpws device binding. </summary>
    public class MdpwsDeviceBinding : BicepsDeviceBinding<MdpwsDeviceBindingSettings>
    {
        /// <summary>   The UDP. </summary>
        private UdpMessenger udp = null!;
        /// <summary>   Manager for disco. </summary>
        private DiscoveryManager discoManager = null!;
        /// <summary>   The device. </summary>
        private Service.Device device = null!;

        /// <summary>   Gets or sets the event binding. </summary>
        ///
        /// <value> The event binding. </value>
        public override IProviderEventSourceBinding EventBinding { get; set; } = null!;

        /// <summary>   Start. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <returns>   An asynchronous result. </returns>
        protected override async Task Start()
        {
            udp = new UdpMessenger(0, Settings.ServerConfig.UseIpv6)
            {
                BindInterface = Settings.BindIp,
                ReceiveTimeout = Settings.UdpTimeoutMs,
                ServerBindAll = Settings.MulticastBindAll
            };
            udp.Open();
            if (udp.LocalAddrs.Count == 0)
            {
                throw new InvalidOperationException("Binding to local address failed!");
            }
            foreach (var nextAdr in udp.LocalAddrs)
            {
                SharpDc.Instance.Logger.LogInformation($"Initializing binding for {nextAdr}.");
            }
            Data.Port = Settings.ServerConfig.Port;
            Data.PortStreaming = Settings.PortStreaming;
            string httpPrefix = Settings.ServerConfig.UseHttps ? "https://" : "http://";
            if (udp.UseIpv6)
            {
                Data.Xaddrs = string.Join(" ",
                    udp.LocalAddrs.Select(ip => httpPrefix + "[" + ip + "]:" + Data.Port + "/" + DeviceRoute.Route));
            }
            else
            {
                Data.Xaddrs = string.Join(" ", 
                    udp.LocalAddrs.Select(ip => httpPrefix + ip + ":" + Data.Port + "/" + DeviceRoute.Route));              
            }
            device = new Service.Device(Data, this);
            EventBinding = new MdpwsProviderEventSourceBinding(Endpoint, Data, device.SetServiceRoute, device.BicServiceRoute);
            await StartDeviceAsync().ConfigureAwait(false);
        }

        /// <summary>   Starts device asynchronous. </summary>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task StartDeviceAsync()
        {
            try
            {
                await device.Start(async () =>
                {
                    discoManager = new DiscoveryManager(Data, udp);
                    await discoManager.SendHello().ConfigureAwait(false);
                }).ConfigureAwait(false);
            }
            catch (ObjectDisposedException)
            {
            }
        }
        
        /// <summary>   copyright holder. </summary>
        ///
        /// <returns>   An asynchronous result. </returns>
        protected override async Task Stop()
        {
            await device.Stop().ConfigureAwait(false);
            await discoManager.SendBye().ConfigureAwait(false);
            discoManager.Dispose();
            SetBinding.Close();
            udp.Close();
        }
    }
}