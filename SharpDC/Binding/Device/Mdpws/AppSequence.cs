﻿// file:	Binding\Device\Mdpws\AppSequence.cs
//
// summary:	Implements the application sequence class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Threading;

namespace SharpDC.Binding.Device.Mdpws
{
    /// <summary>   An application sequence. </summary>
    public class AppSequence
    {
        /// <summary>   The random. </summary>
        private static readonly Random random = new Random();
        
        /// <summary>   Gets or sets the identifier of the instance. </summary>
        ///
        /// <value> The identifier of the instance. </value>
        public int InstanceId { get; internal set; } = Math.Abs(random.Next());
        
        /// <summary>   Gets or sets the message number. </summary>
        ///
        /// <value> The message number. </value>
        public long MessageNumber { get => Interlocked.Read(ref messageNumber); set => Interlocked.Exchange(ref messageNumber, value); }
        /// <summary>   The message number. </summary>
        private long messageNumber = 0;

        /// <summary>   Increment message number. </summary>
        ///
        /// <returns>   A long. </returns>
        public long IncrementMessageNumber() => Interlocked.Increment(ref messageNumber);
        
    }
}