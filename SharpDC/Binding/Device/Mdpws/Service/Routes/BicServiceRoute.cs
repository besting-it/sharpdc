﻿// file:	Binding\Device\Mdpws\Service\Routes\BicServiceRoute.cs
//
// summary:	Implements the biceps service route class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using SharpDC.Binding.Device.Mdpws.Managers;
using SharpDC.Binding.Device.Mdpws.Messages;
using SharpDC.Provider;

namespace SharpDC.Binding.Device.Mdpws.Service.Routes
{
    /// <summary>   A biceps service route. </summary>
    public class BicServiceRoute : ServiceBaseRoute
    {
        /// <summary>   The route. </summary>
        public const string Route = WsConstants.ServiceIdBic;
        /// <summary>   The service calls. </summary>
        private readonly IDictionary<string, Func<Invoke, CancellationToken, Task<InvokeResponse>>> serviceCalls 
            = new ConcurrentDictionary<string, Func<Invoke, CancellationToken, Task<InvokeResponse>>>();
        /// <summary>   The binding. </summary>
        private MdpwsDeviceBinding binding;
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="data">     The data. </param>
        /// <param name="binding">  The binding. </param>
        /// <param name="subMan">   Manager for sub. </param>
        public BicServiceRoute(SdcProviderData data, MdpwsDeviceBinding binding, SubscriptionManager subMan) : base(data,subMan)
        {
            this.binding = binding;
            InitService();
        }

        /// <summary>   Initializes the service. </summary>
        private void InitService()
        {
            serviceCalls.Add(WsConstants.GetContextAction, async (incoming, token) =>
            {
                var request = GetContextStates.Deserialize(incoming.BodyXml);
                var response = await binding.GetBinding.OnGetContextStates(request).ConfigureAwait(false);
                return new InvokeResponse() { Action = WsConstants.GetContextResponseAction, BodyXml = response.Serialize() };
            });
            serviceCalls.Add(WsConstants.SetContextAction, async (incoming, token) =>
            {
                var request = SetContextState.Deserialize(incoming.BodyXml);
                var response = await binding.SetBinding.OnSetState<SetContextState, SetContextStateResponse>(request, null).ConfigureAwait(false);
                return new InvokeResponse() { Action = WsConstants.SetContextResponseAction, BodyXml = response.Serialize() };
            });            
        }

        /// <summary>   Gets the identifier of the service. </summary>
        ///
        /// <value> The identifier of the service. </value>
        protected override string ServiceId => WsConstants.ServiceIdBic;
        
        /// <summary>   Gets a list of types of the services. </summary>
        ///
        /// <value> A list of types of the services. </value>
        protected override IEnumerable<string> ServiceTypes => new [] { WsConstants.PortTypeCtx, WsConstants.PortTypeEvent, WsConstants.PortTypeWave};
        
        /// <summary>   Gets a value indicating whether this object is streaming service. </summary>
        ///
        /// <value> True if this object is streaming service, false if not. </value>
        protected override bool IsStreamingService => true;

        /// <summary>   Gets a value indicating whether this object is eventing service. </summary>
        ///
        /// <value> True if this object is eventing service, false if not. </value>
        protected override bool IsEventingService => true;

        /// <summary>   Gets service calls. </summary>
        ///
        /// <returns>   The service calls. </returns>
        protected override IDictionary<string, Func<Invoke, CancellationToken, Task<InvokeResponse>>> GetServiceCalls() => serviceCalls;

    }
}