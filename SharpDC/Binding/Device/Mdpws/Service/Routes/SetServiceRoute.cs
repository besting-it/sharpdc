﻿// file:	Binding\Device\Mdpws\Service\Routes\SetServiceRoute.cs
//
// summary:	Implements the set service route class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using SharpDC.Binding.Device.Mdpws.Managers;
using SharpDC.Binding.Device.Mdpws.Messages;
using SharpDC.Provider;

namespace SharpDC.Binding.Device.Mdpws.Service.Routes
{
    /// <summary>   A set service route. </summary>
    public class SetServiceRoute : ServiceBaseRoute
    {
        /// <summary>   The route. </summary>
        public const string Route = WsConstants.ServiceIdSet;
        /// <summary>   The service calls. </summary>
        private readonly IDictionary<string, Func<Invoke, CancellationToken, Task<InvokeResponse>>> serviceCalls 
            = new ConcurrentDictionary<string, Func<Invoke, CancellationToken, Task<InvokeResponse>>>();
        /// <summary>   The binding. </summary>
        private readonly MdpwsDeviceBinding binding;
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="data">     The data. </param>
        /// <param name="binding">  The binding. </param>
        /// <param name="subMan">   Manager for sub. </param>
        public SetServiceRoute(SdcProviderData data, MdpwsDeviceBinding binding, SubscriptionManager subMan) : base(data,subMan)
        {
            this.binding = binding;
            InitService();
        }

        /// <summary>   Initializes the service. </summary>
        private void InitService()
        {
            serviceCalls.Add(WsConstants.ActivateAction, async (incoming, token) =>
            {
                var request = Activate.Deserialize(incoming.BodyXml);
                var response = await binding.SetBinding.OnSetState<Activate, ActivateResponse>(request, null).ConfigureAwait(false);
                return new InvokeResponse() { Action = WsConstants.ActivateResponseAction, BodyXml = response.Serialize() };
            });
            serviceCalls.Add(WsConstants.SetAlertAction, async (incoming, token) =>
            {
                var request = SetAlertState.Deserialize(incoming.BodyXml);
                var response = await binding.SetBinding.OnSetState<SetAlertState, SetAlertStateResponse>(request, null).ConfigureAwait(false);
                return new InvokeResponse() { Action = WsConstants.SetAlertResponseAction, BodyXml = response.Serialize() };
            });        
            serviceCalls.Add(WsConstants.SetStringAction, async (incoming, token) =>
            {
                var request = SetString.Deserialize(incoming.BodyXml);
                var response = await binding.SetBinding.OnSetState<SetString, SetStringResponse>(request, null).ConfigureAwait(false);
                return new InvokeResponse() { Action = WsConstants.SetStringResponseAction, BodyXml = response.Serialize() };
            }); 
            serviceCalls.Add(WsConstants.SetValueAction, async (incoming, token) =>
            {
                var request = SetValue.Deserialize(incoming.BodyXml);
                var response = await binding.SetBinding.OnSetState<SetValue, SetValueResponse>(request, null).ConfigureAwait(false);
                return new InvokeResponse() { Action = WsConstants.SetValueResponseAction, BodyXml = response.Serialize() };
            });                  
            serviceCalls.Add(WsConstants.SetMetricStateAction, async (incoming, token) =>
            {
                var request = SetMetricState.Deserialize(incoming.BodyXml);
                var response = await binding.SetBinding.OnSetState<SetMetricState, SetMetricStateResponse>(request, null).ConfigureAwait(false);
                return new InvokeResponse() { Action = WsConstants.SetMetricStateResponseAction, BodyXml = response.Serialize() };
            });
        }

        /// <summary>   Gets the identifier of the service. </summary>
        ///
        /// <value> The identifier of the service. </value>
        protected override string ServiceId => WsConstants.ServiceIdSet;
        
        /// <summary>   Gets a list of types of the services. </summary>
        ///
        /// <value> A list of types of the services. </value>
        protected override IEnumerable<string> ServiceTypes => new[] { WsConstants.PortTypeSet };
        
        /// <summary>   Gets a value indicating whether this object is streaming service. </summary>
        ///
        /// <value> True if this object is streaming service, false if not. </value>
        protected override bool IsStreamingService => false;
        
        /// <summary>   Gets a value indicating whether this object is eventing service. </summary>
        ///
        /// <value> True if this object is eventing service, false if not. </value>
        protected override bool IsEventingService => true;

        /// <summary>   Gets service calls. </summary>
        ///
        /// <returns>   The service calls. </returns>
        protected override IDictionary<string, Func<Invoke, CancellationToken, Task<InvokeResponse>>> GetServiceCalls() => serviceCalls;

    }
}