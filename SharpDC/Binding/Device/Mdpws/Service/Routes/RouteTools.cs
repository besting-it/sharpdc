﻿// file:	Binding\Device\Mdpws\Service\Routes\RouteTools.cs
//
// summary:	Implements the route tools class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Linq;

namespace SharpDC.Binding.Device.Mdpws.Service.Routes
{
    /// <summary>   Route tools. </summary>
    public static class RouteTools
    {
        /// <summary>   Gets the base address in this collection. </summary>
        ///
        /// <param name="xAddr">    The address. </param>
        ///
        /// <returns>
        /// An enumerator that allows foreach to be used to process the base address in this collection.
        /// </returns>
        public static IEnumerable<string> GetBaseAddrs(string xAddr)
        {
            var xAddrList = xAddr.Split(new[] {" "}, StringSplitOptions.None);
            return xAddrList.Select(adr =>
            {
                var uri = new Uri(adr);
                return uri.Scheme + "://" + uri.Host + ":" + uri.Port;
            });
        }

        /// <summary>   Gets the service address in this collection. </summary>
        ///
        /// <param name="xAddr">        The address. </param>
        /// <param name="serviceId">    Identifier for the service. </param>
        ///
        /// <returns>
        /// An enumerator that allows foreach to be used to process the service address in this collection.
        /// </returns>
        public static IEnumerable<string> GetServiceAddrs(string xAddr, string serviceId)
        {
            return GetBaseAddrs(xAddr).Select(a => a + "/" + serviceId);
        }
        
    }
}