﻿// file:	Binding\Device\Mdpws\Service\Routes\ServiceBaseRoute.cs
//
// summary:	Implements the service base route class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Microsoft.Extensions.Logging;
using SharpDC.Binding.Device.Mdpws.Http;
using SharpDC.Binding.Device.Mdpws.Managers;
using SharpDC.Binding.Device.Mdpws.Messages;
using SharpDC.Misc;
using SharpDC.Provider;

namespace SharpDC.Binding.Device.Mdpws.Service.Routes
{
    /// <summary>   A service base route. </summary>
    public abstract class ServiceBaseRoute
    {
        
        /// <summary>   The data. </summary>
        private readonly SdcProviderData data;
        /// <summary>   Gets the manager for subscription. </summary>
        ///
        /// <value> The subscription manager. </value>
        internal SubscriptionManager SubscriptionManager { get; }

        /// <summary>   The wsdl suffix. </summary>
        public const string WsdlSuffix = "Wsdl";

        /// <summary>   Specialized constructor for use only by derived class. </summary>
        ///
        /// <param name="data">     The data. </param>
        /// <param name="subMan">   Manager for sub. </param>
        protected ServiceBaseRoute(SdcProviderData data, SubscriptionManager subMan)
        {
            this.data = data;
            SubscriptionManager = subMan;
        }   
        
        /// <summary>   Base route. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="rq">       The request. </param>
        /// <param name="rp">       The rp. </param>
        /// <param name="token">    A token that allows processing to be cancelled. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task Base(HttpRequest rq, HttpResponse rp, CancellationToken token)
        {
            try
            {
                rp.ContentType = "application/soap+xml";
                var body = await rq.GetBody(token).ConfigureAwait(false);
                var incoming = new Message(body);          
                SharpDc.Instance.Logger.LogTrace($"Incoming message: {incoming.Action} received on {rq.LocalEndpoint}.");
                if (incoming.Action == WsConstants.GetMetaAction)
                {
                    await rp.AsText(StringTools.RemoveCrLf(await HandleGetMetadata(rq, incoming).ConfigureAwait(false)), token: token).
                        ConfigureAwait(false);
                    return;
                }
                
                if (IsEventingService && incoming.Action.StartsWith(WsConstants.EventingPrefixAction))
                {
                    var serviceAddr = data.IsSecure? "https://" : "http://" + rq.LocalEndpoint + $"/{ServiceId}";
                    var eventingResponse = await SubscriptionManager.HandleEventingRequests(rq, incoming, serviceAddr).
                        ConfigureAwait(false);
                    if (eventingResponse != null)
                    {
                        await rp.AsText(StringTools.RemoveCrLf(eventingResponse), token: token).ConfigureAwait(false);
                        return;
                    }
                }
                
                var serviceCalls = GetServiceCalls();
                if (!serviceCalls.ContainsKey(incoming.Action))
                    throw new InvalidOperationException($"Action not supported: {incoming.Action}");
                
                // SDC input message, try validation if activated
                if (SharpDc.Instance.ProviderSchemaValidator != null)
                {
                    var dmpIn = incoming.DataModelPart;
                    SharpDc.Instance.ProviderSchemaValidator.Validate(dmpIn.ToString());
                }
                var call = serviceCalls[incoming.Action];
                var invokeResponse = await call(new Invoke(incoming.Xml), token).ConfigureAwait(false);
                invokeResponse.RelatesTo = incoming.MessageId;
                SharpDc.Instance.Logger.LogTrace($"Outgoing message: {invokeResponse.Action} to {rq.RemoteEndpoint}.");
                // SDC output message, try validation if activated
                if (SharpDc.Instance.ProviderSchemaValidator != null)
                {
                    var dmpOut = invokeResponse.DataModelPart;
                    SharpDc.Instance.ProviderSchemaValidator.Validate(dmpOut.ToString());
                }
                await rp.AsText(StringTools.RemoveCrLf(invokeResponse.ToString()), token: token).ConfigureAwait(false);
            }
            catch (IOException e)
            {
                SharpDc.Instance.Logger.LogError(
                    $"Connection error when sending to {rq.RemoteEndpoint}: {e.Message}");
            }
            catch (XmlException e)
            {
                var fault = new Fault() { FaultString = e.Message };
                SharpDc.Instance.Logger.LogError($"Xml validation error: {fault.Action}, reason: {e.Message}, to {rq.RemoteEndpoint}.");
                await rp.AsText(StringTools.RemoveCrLf(fault.ToString()), token: token).ConfigureAwait(false);
            }            
            catch (Exception e)
            {
                var fault = new Fault() { FaultString = e.Message };
                SharpDc.Instance.Logger.LogError($"Fault message: {fault.Action}, reason: {e.Message}, to {rq.RemoteEndpoint}.");
                await rp.AsText(StringTools.RemoveCrLf(fault.ToString()), token: token).ConfigureAwait(false);
            }
        }

        /// <summary>   Handles the get metadata. </summary>
        ///
        /// <param name="rq">       The request. </param>
        /// <param name="incoming"> The incoming. </param>
        ///
        /// <returns>   An asynchronous result that yields a string. </returns>
        private Task<string> HandleGetMetadata(HttpRequest rq, Message incoming)
        {
            var getMetaResponse = CreateGetMetadataResponse(rq.LocalEndpoint.Address.ToString());
            getMetaResponse.RelatesTo = incoming.MessageId;
            SharpDc.Instance.Logger.LogTrace($"Outgoing message: {getMetaResponse.Action} to {rq.RemoteEndpoint}.");
            return Task.FromResult(getMetaResponse.ToString());
        }

        /// <summary>   Creates get metadata response. </summary>
        ///
        /// <param name="ip">   The IP. </param>
        ///
        /// <returns>   The new get metadata response. </returns>
        private GetMetaResponse CreateGetMetadataResponse(string ip)
        {
            var response = new GetMetaResponse();
            response.HostEpr = data.EndpointReference;
            var serviceAddrs = RouteTools.GetServiceAddrs(data.Xaddrs, ServiceId);
            var serviceAdr = serviceAddrs.First(x => x.Contains(ip));
            response.AddHostedSection(serviceAdr, ServiceTypes, ServiceId);
            response.WsdlLocation = serviceAdr + "/" + WsdlSuffix;
            if (IsStreamingService)
            {
                string baseAdr = data.IsIpV6
                    ? WsConstants.WaveformStreamingBaseAddrIpv6
                    : WsConstants.WaveformStreamingBaseAddrIpv4;
                response.SetStreamAddress(baseAdr + ":" + data.PortStreaming);
            }
            return response;
        }

        /// <summary>   Gets the identifier of the service. </summary>
        ///
        /// <value> The identifier of the service. </value>
        protected abstract string ServiceId { get; }
        
        /// <summary>   Gets a list of types of the services. </summary>
        ///
        /// <value> A list of types of the services. </value>
        protected abstract IEnumerable<string> ServiceTypes { get; }
        
        /// <summary>   Gets a value indicating whether this object is streaming service. </summary>
        ///
        /// <value> True if this object is streaming service, false if not. </value>
        protected abstract bool IsStreamingService { get; }
        
        /// <summary>   Gets a value indicating whether this object is eventing service. </summary>
        ///
        /// <value> True if this object is eventing service, false if not. </value>
        protected abstract bool IsEventingService { get; }

        /// <summary>   Gets service calls. </summary>
        ///
        /// <returns>   The service calls. </returns>
        protected abstract IDictionary<string, Func<Invoke, CancellationToken, Task<InvokeResponse>>> GetServiceCalls();

        /// <summary>   Stops this object. </summary>
        public void Stop()
        {
            SubscriptionManager.Stop();
        }

    }
}