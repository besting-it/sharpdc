﻿// file:	Binding\Device\Mdpws\Service\Routes\DescServiceRoute.cs
//
// summary:	Implements the description service route class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using SharpDC.Binding.Device.Mdpws.Managers;
using SharpDC.Binding.Device.Mdpws.Messages;
using SharpDC.Provider;

namespace SharpDC.Binding.Device.Mdpws.Service.Routes
{
    /// <summary>   A get service route. </summary>
    public class DescServiceRoute : ServiceBaseRoute
    {
        /// <summary>   The route. </summary>
        public const string Route = WsConstants.ServiceIdDesc;
        /// <summary>   The service calls. </summary>
        private readonly IDictionary<string, Func<Invoke, CancellationToken, Task<InvokeResponse>>> serviceCalls 
            = new ConcurrentDictionary<string, Func<Invoke, CancellationToken, Task<InvokeResponse>>>();
        /// <summary>   The binding. </summary>
        private readonly MdpwsDeviceBinding binding;
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="data">     The data. </param>
        /// <param name="binding">  The binding. </param>
        /// <param name="subMan">   Manager for sub. </param>
        public DescServiceRoute(SdcProviderData data, MdpwsDeviceBinding binding, SubscriptionManager subMan) : base(data,subMan)
        {
            this.binding = binding;
        }

        /// <summary>   Gets the identifier of the service. </summary>
        ///
        /// <value> The identifier of the service. </value>
        protected override string ServiceId => WsConstants.ServiceIdDesc;
        
        /// <summary>   Gets a list of types of the services. </summary>
        ///
        /// <value> A list of types of the services. </value>
        protected override IEnumerable<string> ServiceTypes => new[] { WsConstants.PortTypeDesc };

        /// <summary>   Gets a value indicating whether this object is streaming service. </summary>
        ///
        /// <value> True if this object is streaming service, false if not. </value>
        protected override bool IsStreamingService => false;
        
        /// <summary>   Gets a value indicating whether this object is eventing service. </summary>
        ///
        /// <value> True if this object is eventing service, false if not. </value>
        protected override bool IsEventingService => true;

        /// <summary>   Gets service calls. </summary>
        ///
        /// <returns>   The service calls. </returns>
        protected override IDictionary<string, Func<Invoke, CancellationToken, Task<InvokeResponse>>> GetServiceCalls() => serviceCalls;

    }
}