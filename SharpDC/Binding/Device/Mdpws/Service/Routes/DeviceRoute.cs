﻿// file:	Binding\Device\Mdpws\Service\Routes\DeviceRoute.cs
//
// summary:	Implements the device route class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.Extensions.Logging;
using SharpDC.Binding.Device.Mdpws.Http;
using SharpDC.Binding.Device.Mdpws.Managers;
using SharpDC.Binding.Device.Mdpws.Messages;
using SharpDC.Misc;
using SharpDC.Provider;

namespace SharpDC.Binding.Device.Mdpws.Service.Routes
{
    /// <summary>   A device route. </summary>
    public class DeviceRoute
    {
        
        /// <summary>   The route. </summary>
        public const string Route = "Device";
        /// <summary>   The data. </summary>
        private SdcProviderData data;
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="data"> The data. </param>
        public DeviceRoute(SdcProviderData data)
        {
            this.data = data;
        }        
        
        /// <summary>   Base route. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="rq">       The request. </param>
        /// <param name="rp">       The rp. </param>
        /// <param name="token">    A token that allows processing to be cancelled. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task Base(HttpRequest rq, HttpResponse rp, CancellationToken token)
        {
            try
            {
                rp.ContentType = "application/soap+xml";
                var body = await rq.GetBody(token).ConfigureAwait(false);
                var incoming = new Message(body);
                SharpDc.Instance.Logger.LogTrace($"Incoming message: {incoming.Action} received on {rq.LocalEndpoint}.");
                
                Message response;
                switch (incoming.Action)
                {
                    case WsConstants.GetAction:
                        response = CreateGetResponse();
                        response.RelatesTo = incoming.MessageId;
                        break;
                    case WsConstants.ProbeAction:
                        response = DiscoveryManager.HandleProbe(new Probe(incoming.Xml), data);
                        break;
                    case WsConstants.ResolveAction:
                        response = DiscoveryManager.HandleResolve(new Resolve(incoming.Xml), data);
                        break;
                    default: throw new InvalidOperationException($"Action not supported: {incoming.Action}");
                }
                
                SharpDc.Instance.Logger.LogTrace($"Outgoing message: {response.Action} to {rq.RemoteEndpoint}.");
                await rp.AsText(StringTools.RemoveCrLf(response.ToString()), token: token).ConfigureAwait(false);
            }
            catch (IOException e)
            {
                SharpDc.Instance.Logger.LogError(
                    $"Connection error when sending to {rq.RemoteEndpoint}: {e.Message}");
            }
            catch (Exception e)
            {
                var fault = new Fault() {FaultString = e.Message};
                SharpDc.Instance.Logger.LogError(
                    $"Fault message: {fault.Action}, reason: {e.Message}, to {rq.RemoteEndpoint}.");
                await rp.AsText(StringTools.RemoveCrLf(fault.ToString()), token: token).ConfigureAwait(false);
            }
        }

        /// <summary>   Creates get response. </summary>
        ///
        /// <returns>   The new get response. </returns>
        private GetResponse CreateGetResponse()
        {
            var endpointBaseAddrs = RouteTools.GetBaseAddrs(data.Xaddrs);
            var response = new GetResponse();
            response.AddHostedSection(endpointBaseAddrs.Select(adr => adr + "/" + WsConstants.ServiceIdGet), 
                new [] { WsConstants.PortTypeGet}, WsConstants.ServiceIdGet);
            response.AddHostedSection(endpointBaseAddrs.Select(adr => adr + "/" + WsConstants.ServiceIdSet), 
                new [] { WsConstants.PortTypeSet}, WsConstants.ServiceIdSet);
            response.AddHostedSection(endpointBaseAddrs.Select(adr => adr + "/" + WsConstants.ServiceIdBic), 
                new [] { WsConstants.PortTypeCtx, WsConstants.PortTypeEvent, WsConstants.PortTypeWave }, 
                WsConstants.ServiceIdBic);            
            response.AddHostedSection(endpointBaseAddrs.Select(adr => adr + "/" + WsConstants.ServiceIdDesc), 
                new [] { WsConstants.PortTypeDesc}, WsConstants.ServiceIdDesc);
            response.HostEpr = data.EndpointReference;
            var providerMetadata = data.Meta;
            response.Manufacturer = providerMetadata.Manufacturer;
            response.ModelName = providerMetadata.ModelName;
            response.ModelNumber = providerMetadata.ModelNumber;
            response.FriendlyName = providerMetadata.FriendlyName;
            response.SerialNumber = providerMetadata.SerialNumber;
            return response;
        }
    }
}