﻿// file:	Binding\Device\Mdpws\Service\UdpMessenger.cs
//
// summary:	Implements the UDP messenger class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Threading;
using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;

namespace SharpDC.Binding.Device.Mdpws.Service
{

    /// <summary>   An UDP state. </summary>
    public class UdpState
    {
        public UdpState(UdpClient client)
        {
            Client = client;
        }
        
        /// <summary>   Gets or sets the remote endpoint. </summary>
        ///
        /// <value> The remote endpoint. </value>
        public IPEndPoint RemoteEndpoint { get; set; } = new IPEndPoint(IPAddress.Any, 0);
        
        /// <summary>   Gets or sets the client. </summary>
        ///
        /// <value> The client. </value>
        public UdpClient Client { get; set; }

        /// <summary>   Gets or sets information describing the received. </summary>
        ///
        /// <value> Information describing the received. </value>
        public byte[] ReceivedData { get; set; } = new byte[0];

        /// <summary>   Gets or sets a value indicating whether this object is unicast. </summary>
        ///
        /// <value> True if this object is unicast, false if not. </value>
        public bool IsUnicast { get; set; }
    }

    /// <summary>   An outgoing message. </summary>
    public class OutgoingMessage
    {
        /// <summary>   Gets or sets the content. </summary>
        ///
        /// <value> The content. </value>
        public string Content { get; set; } = string.Empty;

        /// <summary>   Gets or sets the addrs. </summary>
        ///
        /// <value> The addrs. </value>
        public string Addrs { get; set; } = string.Empty;
        
        /// <summary>   Gets or sets the port. </summary>
        ///
        /// <value> The port. </value>
        public int Port { get; set; }
    }

    /// <summary>   Additional information for message received events. </summary>
    public class MessageReceivedEventArgs : EventArgs
    {
        /// <summary>   The XML header. </summary>
        private const string XmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

        /// <summary>   Gets or sets the remote endpoint address. </summary>
        ///
        /// <value> The remote endpoint address. </value>
        public string RemoteEndpointAddr { get; set; } = string.Empty;
        
        /// <summary>   Gets or sets the remote endpoint port. </summary>
        ///
        /// <value> The remote endpoint port. </value>
        public int RemoteEndpointPort { get; set; }

        /// <summary>   Gets or sets the incoming content. </summary>
        ///
        /// <value> The incoming content. </value>
        /// 
        public string IncomingContent { get; set; } = string.Empty;
        
        /// <summary>   Gets or sets the outgoing. </summary>
        ///
        /// <value> The outgoing. </value>
        public OutgoingMessage? Outgoing { get; set; }

        /// <summary>   Direct response. </summary>
        ///
        /// <param name="content">      The content. </param>
        /// <param name="xmlHeader">    (Optional) True to XML header. </param>
        public void DirectResponse(string content, bool xmlHeader = true)
        {
            Outgoing = new OutgoingMessage()
            {
                Addrs = RemoteEndpointAddr,
                Port = RemoteEndpointPort,
                Content = xmlHeader? XmlHeader + content : content
            };
        }
    }

    /// <summary>   A request response client. </summary>
    public class RequestResponseClient
    {
        public RequestResponseClient(UdpClient client)
        {
            Client = client;
        }

        /// <summary>   Gets or sets the client. </summary>
        ///
        /// <value> The client. </value>
        public UdpClient Client { get; set; }

        /// <summary>   Gets or sets the receive task. </summary>
        ///
        /// <value> An asynchronous result. </value>
        public Task ReceiveTask { get; set; } = Task.CompletedTask;
        /// <summary>   Gets or sets the cts. </summary>
        ///
        /// <value> The cts. </value>
        public CancellationTokenSource? Cts { get; set; }

        /// <summary>   Cancel receiving. </summary>
        public void CancelReceiving()
        {
            if (Cts != null)
            {
                Cts.Cancel();
                Cts.Dispose();
                Cts = null;
            }
        }
    }

    /// <summary>   A UDP messenger. </summary>
    public class UdpMessenger : IDisposable
    {
        /// <summary>   The default m cast port. </summary>
        private const int DefaultMCastPort = 3702;
        /// <summary>   The default m cast IPv4. </summary>
        private const string DefaultMCastIpV4 = "239.255.255.250";
        /// <summary>   The default m cast IPv6. </summary>
        private const string DefaultMCastIpV6 = "FF02::C";
        /// <summary>   The default receive timeout. </summary>
        private const int DefaultReceiveTimeout = 3000;
        /// <summary>   The default send repeat delay milliseconds. </summary>
        private const int DefaultSendRepeatDelayMs = 100;

        /// <summary>   Event queue for all listeners interested in UnicastReceived events. </summary>
        public event EventHandler<MessageReceivedEventArgs>? UnicastReceived;
        /// <summary>   Event queue for all listeners interested in MulticastReceived events. </summary>
        public event EventHandler<MessageReceivedEventArgs>? MulticastReceived;

        /// <summary>   Gets the local port. </summary>
        ///
        /// <value> The local port. </value>
        public int LocalPort { get; }
        /// <summary>   Gets the local addrs. </summary>
        ///
        /// <value> The local addrs. </value>
        public HashSet<string> LocalAddrs { get; } = new HashSet<string>();
        /// <summary>   Gets the local address string. </summary>
        ///
        /// <value> The local address string. </value>
        public string LocalAddrString { get => string.Join(",", LocalAddrs); }
        /// <summary>   Gets or sets the bind interface. </summary>
        ///
        /// <value> The bind interface. </value>
        public string BindInterface { get; set; } = string.Empty;
        /// <summary>   Gets or sets the cast IP. </summary>
        ///
        /// <value> The m cast IP. </value>
        public string MCastIp { get => mcastIp; set => mcastIp = value; }
        /// <summary>   Gets or sets the cast port. </summary>
        ///
        /// <value> The m cast port. </value>
        public int MCastPort { get => mcastPort; set => mcastPort = value; }
        /// <summary>   Gets or sets the receive timeout. </summary>
        ///
        /// <value> The receive timeout. </value>
        public int ReceiveTimeout { get; set; } = DefaultReceiveTimeout;
        /// <summary>   Gets or sets the send repeat delay milliseconds. </summary>
        ///
        /// <value> The send repeat delay milliseconds. </value>
        public int SendRepeatDelayMs { get; set; } = DefaultSendRepeatDelayMs;

        /// <summary>   The UDP clients. </summary>
        readonly ConcurrentDictionary<string, RequestResponseClient> udpClients = new ConcurrentDictionary<string, RequestResponseClient>();

        /// <summary>   The local interfaces. </summary>
        private List<InterfaceAddress> localInterfaces = new List<InterfaceAddress>();

        /// <summary>   The server token source. </summary>
        CancellationTokenSource? serverTokenSource;
        /// <summary>   The joiner. </summary>
        IInterfaceJoiner? joiner;
        /// <summary>   The mcast IP. </summary>
        string mcastIp = string.Empty;
        /// <summary>   The mcast port. </summary>
        int mcastPort;
        /// <summary>   True if is disposed, false if not. </summary>
        bool isDisposed;
        
        /// <summary>   any address. </summary>
        readonly IPAddress anyAddr;
        /// <summary>   The address family. </summary>
        readonly AddressFamily addrFamily;
        /// <summary>   Gets or sets a value indicating whether the server bind all. </summary>
        ///
        /// <value> True if server bind all, false if not. </value>
        public bool ServerBindAll { get; set; } = true;
        /// <summary>   Gets a value indicating whether this object use IPv6. </summary>
        ///
        /// <value> True if use ipv6, false if not. </value>
        public bool UseIpv6 { get; } = false;

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="localPort">    (Optional) The local port. </param>
        /// <param name="useIpv6">      (Optional) True to use IPv6. </param>
        public UdpMessenger(int localPort = 0, bool useIpv6 = false)
        {
            UseIpv6 = useIpv6;
            LocalPort = localPort;
            anyAddr = useIpv6 ? IPAddress.IPv6Any : IPAddress.Any;
            addrFamily = useIpv6 ? AddressFamily.InterNetworkV6 : AddressFamily.InterNetwork;
            mcastIp = useIpv6 ? DefaultMCastIpV6 : DefaultMCastIpV4;
            mcastPort = DefaultMCastPort;
            if (useIpv6)
            {
                joiner = new Ipv6Joiner();
            }
            else
            {
                joiner = new Ipv4Joiner();
            }
        }

        /// <summary>   Send UDP message to multicast target (the same target this messenger uses) </summary>
        ///
        /// <param name="data">             The data to send. </param>
        /// <param name="expectResponse">   (Optional) Whether to expect a response. </param>
        /// <param name="sendRepeat">       (Optional) Send repeat count. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task SendMulticast(string data, bool expectResponse = false, int sendRepeat = 0)
        {
            await Send(MCastIp, MCastPort, data, expectResponse);
            for (int i = 0; i < sendRepeat; i++)
            {
                await Task.Delay(SendRepeatDelayMs).ConfigureAwait(false);
                await Send(MCastIp, MCastPort, data, false).ConfigureAwait(false);
            }
        }

        /// <summary>   Send UDP message to unicast or multicast target. </summary>
        ///
        /// <param name="ip">               Single target IP address, or multiple addresses separated by
        ///                                 comma. </param>
        /// <param name="port">             Target port. </param>
        /// <param name="data">             The data to send. </param>
        /// <param name="expectResponse">   (Optional) Whether to expect a response. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task Send(string ip, int port, string data, bool expectResponse = false)
        {
            CheckNull(ip, port, data);
            var dataBytes = Encoding.ASCII.GetBytes(data);
            var targets = ip.Split(',');
            foreach (var nextIp in targets)
            {
                var ep = new IPEndPoint(IPAddress.Parse(nextIp), port);
                if (UdpUtils.IsMulticast(nextIp))
                {
                    // Multicast, use all interfaces
                    foreach (var nextClient in udpClients)
                    {
                        if (expectResponse)
                            ExpectResponse(nextClient.Value);
                        await nextClient.Value.Client.SendAsync(dataBytes, dataBytes.Length, ep).ConfigureAwait(false);
                    }
                }
                else
                {
                    // Unicast, use interface matching best with target address
                    string? matchingIpKey = UdpUtils.GetIpRoute(nextIp, udpClients.Keys);
                    if (matchingIpKey == null)
                        continue;
                    if (!udpClients.ContainsKey(matchingIpKey))
                    {
                        SharpDc.Instance.Logger.LogCritical("Target address not reachable by used network interfaces!");
                        continue;
                    }
                    // Start receiving response if needed
                    var matchingClient = udpClients[matchingIpKey];
                    if (expectResponse)
                        ExpectResponse(matchingClient);
                    // Send
                    await matchingClient.Client.SendAsync(dataBytes, dataBytes.Length, ep).ConfigureAwait(false);
                }
            }
        }

        /// <summary>   Expect response. </summary>
        ///
        /// <param name="matchingClient">   The matching client. </param>
        private void ExpectResponse(RequestResponseClient matchingClient)
        {
            var unicastState = new UdpState(matchingClient.Client)
            {
                IsUnicast = true
            };
            matchingClient.CancelReceiving();
            matchingClient.Cts = new CancellationTokenSource();
            matchingClient.ReceiveTask =
                StartReceiving(matchingClient.Client, unicastState, matchingClient.Cts.Token, ReceiveTimeout);
        }

        /// <summary>   Check if null. </summary>
        ///
        /// <exception cref="NullReferenceException">   Thrown when a value was unexpectedly null. </exception>
        ///
        /// <param name="ip">   Single target IP address, or multiple addresses separated by comma. </param>
        /// <param name="port"> Target port. </param>
        /// <param name="data"> The data to send. </param>
        private static void CheckNull(string ip, int port, string data)
        {
            if (ip == null)
                throw new NullReferenceException("IP cannot be null!");
            if (data == null)
                throw new NullReferenceException("Data cannot be null!");
            if (port == 0)
                throw new NullReferenceException("Port cannot be 0!");
        }

        /// <summary>   Starts the messenger. </summary>
        ///
        /// <param name="startMulticastServer"> (Optional) Whether to start a multicast server. </param>
        public void Open(bool startMulticastServer = true)
        {
            serverTokenSource = new CancellationTokenSource();
            InitInterfaces(startMulticastServer);
            foreach (string nextLocal in LocalAddrs)
            {
                var unicastEndpoint = new IPEndPoint(IPAddress.Parse(nextLocal), 0);
                var client = new UdpClient(unicastEndpoint);
                udpClients.TryAdd(nextLocal, new RequestResponseClient(client));
            }
            isDisposed = false;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        public void Dispose()
        {
            Close();
        }

        /// <summary>   Closes this object. </summary>
        public void Close()
        {
            if (isDisposed)
                return;
            serverTokenSource?.Cancel();
            serverTokenSource?.Dispose();
            foreach (var next in localInterfaces)
                next.Server?.Close();
            localInterfaces.Clear();
            foreach (var next in udpClients)
            {
                next.Value.CancelReceiving();
                next.Value.Client.Close();
            }
            udpClients.Clear();
            LocalAddrs.Clear();
            isDisposed = true;
        }

        /// <summary>   Initializes the interfaces. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="startMulticastServer"> Whether to start a multicast server. </param>
        void InitInterfaces(bool startMulticastServer)
        {
            if (startMulticastServer && !UdpUtils.IsMulticast(mcastIp))
            {
                throw new InvalidOperationException("Multicast binding not possible, IP is unicast address: " + mcastIp);
            }
            if (joiner == null)
                throw new InvalidOperationException("Joiner not initialized.");
            if (serverTokenSource == null)
                throw new InvalidOperationException("Server token source not initialized.");
            var mcastAdr = IPAddress.Parse(mcastIp);
            localInterfaces = joiner.InitInterfaces(addrFamily, mcastAdr, mcastPort, BindInterface, startMulticastServer, ServerBindAll);
            foreach (var nextServer in localInterfaces)
            {
                LocalAddrs.Add(nextServer.LocalEndpointAdr);
                if (nextServer.Server != null)
                {
                    var multicastState = new UdpState(nextServer.Server)
                    {
                        IsUnicast = false
                    };
                    StartReceivingAsync(nextServer.Server, multicastState, serverTokenSource.Token);

                }
            }
        }

        /// <summary>   Starts receiving asynchronous. </summary>
        ///
        /// <param name="client">       The client. </param>
        /// <param name="state">        The state. </param>
        /// <param name="token">        A token that allows processing to be cancelled. </param>
        /// <param name="timeout">      (Optional) The timeout. </param>
        /// <param name="loopInfinite"> (Optional) True to loop infinite. </param>
        private async void StartReceivingAsync(UdpClient client, UdpState state, CancellationToken token, int timeout = Timeout.Infinite, bool loopInfinite = true)
        {
            try
            {
                await StartReceiving(client, state, token, timeout, loopInfinite).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                SharpDc.Instance.Logger.LogCritical(e, "Error in UDP reception, received exception: " + e.Message); 
            }
        }
        
        /// <summary>   Starts a receiving. </summary>
        ///
        /// <param name="client">       The client. </param>
        /// <param name="state">        The state. </param>
        /// <param name="token">        A token that allows processing to be cancelled. </param>
        /// <param name="timeout">      (Optional) The timeout. </param>
        /// <param name="loopInfinite"> (Optional) True to loop infinite. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task StartReceiving(UdpClient client, UdpState state, CancellationToken token, int timeout = Timeout.Infinite, bool loopInfinite = true)
        {
            do
            {
                try
                {
                    var receiveTask = client.ReceiveAsync();
                    await Task.WhenAny(receiveTask, Task.Delay(timeout, token)).ConfigureAwait(false);
                    token.ThrowIfCancellationRequested();
                    if (receiveTask.IsCompleted)
                    {
                        state.RemoteEndpoint = receiveTask.Result.RemoteEndPoint;
                        state.ReceivedData = receiveTask.Result.Buffer;
                        await ReceiveCallback(state).ConfigureAwait(false);                        
                    }
                    else
                    {
                        SharpDc.Instance.Logger.LogDebug("UDP received timeout.");
                        break;
                    }
                }
                catch (Exception ex) when (ex is OperationCanceledException || ex is ObjectDisposedException)
                {
                    break;
                }
                catch (Exception e)
                {
                    SharpDc.Instance.Logger.LogCritical(e, "Error processing UDP message");
                }
            } while (loopInfinite);
        }

        /// <summary>   Callback, called when the receive. </summary>
        ///
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task ReceiveCallback(UdpState state)
        {
            var args = new MessageReceivedEventArgs()
            {
                IncomingContent = Encoding.ASCII.GetString(state.ReceivedData),
                RemoteEndpointAddr = state.RemoteEndpoint.Address.ToString(),
                RemoteEndpointPort = state.RemoteEndpoint.Port
            };
            if (state.IsUnicast)
                UnicastReceived?.Invoke(this, args);
            else
                MulticastReceived?.Invoke(this, args);
            // Check for direct response
            if (args.Outgoing != null)
            {
                await Send(args.Outgoing.Addrs, args.Outgoing.Port, args.Outgoing.Content).ConfigureAwait(false);
            }
        }

    }
}
