﻿// file:	Binding\Device\Mdpws\Service\UdpUtils.cs
//
// summary:	Implements the UDP utilities class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace SharpDC.Binding.Device.Mdpws.Service
{
    /// <summary>   Nif action. </summary>
    ///
    /// <param name="ifaceLocalIp"> The interface local IP. </param>
    /// <param name="iface">        The interface. </param>
    ///
    /// <returns>   A bool. </returns>
    internal delegate bool NifAction(string ifaceLocalIp, NetworkInterface iface);

    /// <summary>   A UDP utilities class. </summary>
    class UdpUtils
    {
        /// <summary>   The minimum length of the match. </summary>
        const int MinMatchLength = 4;

        /// <summary>   Get the best route that matches longest prefix of given local ips. </summary>
        ///
        /// <param name="targetIp"> The target route/param> </param>
        /// <param name="localIps"> The local ips. </param>
        ///
        /// <returns>   The best matching route, or null. </returns>
        internal static string? GetIpRoute(string targetIp, ICollection<string> localIps)
        {
            var matches = new Dictionary<string, int>();
            foreach (var next in localIps)
                matches.Add(next, 0);
            // Start comparing prefix lengths
            int longestMax = 0;
            foreach (var next in localIps)
            {
                for (int i = 0; i < next.Length; i++)
                {
                    if (i < targetIp.Length)
                    {
                        int current = matches[next];
                        if (next[i] == targetIp[i])
                        {
                            current++;
                            longestMax = Math.Max(current, longestMax);
                            matches[next] = current;
                        }
                        else
                        {
                            if (current < longestMax)
                            {
                                // Better ones available
                                matches.Remove(next);
                                break;
                            }
                        }
                    }
                }
                // Already found best
                if (matches.Count == 1)
                {
                    if (longestMax >= MinMatchLength)
                        return matches.First().Key;
                    return null;
                }
            }
            if (longestMax < MinMatchLength)
                return null;
            // Sort by match length and get last value (longest match)
            var sorted = matches.OrderBy(e => e.Value);
            return sorted.Last().Key;
        }

        /// <summary>   Query if 'ip' is multicast. </summary>
        ///
        /// <param name="ip">   The IP. </param>
        ///
        /// <returns>   True if multicast, false if not. </returns>
        internal static bool IsMulticast(string ip)
        {
            if (ip.Contains('.'))
            {
                // ipv4
                return int.Parse(ip.Substring(0, ip.IndexOf('.'))) >= 224;
            }
            else if (ip.Contains(':'))
            {
                // ipv6
                return ip.ToUpperInvariant().StartsWith("FF0");
            }
            return false;
        }

    }
}
