﻿// file:	Binding\Device\Mdpws\Service\Device.cs
//
// summary:	Implements the device class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SharpDC.Binding.Device.Mdpws.Http;
using SharpDC.Binding.Device.Mdpws.Managers;
using SharpDC.Binding.Device.Mdpws.Service.Routes;
using SharpDC.Misc;
using SharpDC.Properties;
using SharpDC.Provider;

namespace SharpDC.Binding.Device.Mdpws.Service
{
    /// <summary>   A device. </summary>
    public class Device
    {
        /// <summary>   The cts. </summary>
        private CancellationTokenSource? cts;
        /// <summary>   The data. </summary>
        private readonly SdcProviderData data;
        /// <summary>   The binding. </summary>
        private readonly MdpwsDeviceBinding binding;
        /// <summary>   The server. </summary>
        private HttpServer? server;
        /// <summary>   The device route. </summary>
        private readonly DeviceRoute deviceRoute;
        /// <summary>   The get service route. </summary>
        private readonly GetServiceRoute getServiceRoute;
        /// <summary>   Gets the set service route. </summary>
        ///
        /// <value> The set service route. </value>
        internal SetServiceRoute SetServiceRoute { get; }
        /// <summary>   Gets the bic service route. </summary>
        ///
        /// <value> The bic service route. </value>
        internal BicServiceRoute BicServiceRoute { get; }
        /// <summary>   Gets the desc service route. </summary>
        ///
        /// <value> The desc service route. </value>
        internal DescServiceRoute DescServiceRoute { get; }        

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="data">     The data. </param>
        /// <param name="binding">  The binding. </param>
        public Device(SdcProviderData data, MdpwsDeviceBinding binding)
        {
            this.data = data;
            this.binding = binding;
            deviceRoute = new DeviceRoute(data);
            var subMan = new SubscriptionManager(binding.Settings.ClientConfig);
            getServiceRoute = new GetServiceRoute(data, binding, subMan);
            SetServiceRoute = new SetServiceRoute(data, binding, subMan);
            BicServiceRoute = new BicServiceRoute(data, binding, subMan);
            DescServiceRoute = new DescServiceRoute(data, binding, subMan);
            data.PropertyChanged += OnPropertyChanged;
        }
        
        /// <summary>   Raises the property changed event. </summary>
        ///
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != nameof(data.Port) && e.PropertyName != nameof(data.Xaddrs)
                && e.PropertyName != nameof(data.PortStreaming))
                return;
            SharpDc.Instance.Logger.LogWarning($"Settings which are related to transport have been changed. Restart of device is needed!");
        }

        /// <summary>   Starts the given post start. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="postStart">    The post start. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task Start(Func<Task> postStart)
        {
            if (binding.Settings.ServerConfig.SslAuthenticationOptions?.Certificates.Count > 0 && !binding.Settings.ServerConfig.UseHttps)
                SharpDc.Instance.Logger.LogWarning("Found certificate but IsSecure is 'false'. Security will be turned off.");
            if (binding.Settings.ServerConfig.UseIpv6 && binding.Settings.BindIp == MdpwsDeviceBindingSettings.Ipv4All)
                throw new InvalidOperationException("Incompatible settings detected: IsIpv6 and bind to Ipv4All.");
            
            server = new HttpServer();
            server.Failing += (sender, args) => SharpDc.Instance.Logger.LogWarning(args.Exception.Message);
            
            cts = new CancellationTokenSource();
            SharpDc.Instance.Logger.LogInformation($"Creating listening endpoint for port {data.Port}.");
            
            server.AddRoute("/" + DeviceRoute.Route, deviceRoute.Base);
            
            AddXsdResourceRoutes(GetServiceRoute.Route, () => Encoding.Default.GetString(Resources.GetService));
            server.AddRoute("/" + GetServiceRoute.Route, getServiceRoute.Base);

            AddXsdResourceRoutes(SetServiceRoute.Route, () => Encoding.Default.GetString(Resources.SetService));
            server.AddRoute("/" + SetServiceRoute.Route + "*", SetServiceRoute.Base);
            
            AddXsdResourceRoutes(BicServiceRoute.Route, () => Encoding.Default.GetString(Resources.BicepsService));
            server.AddRoute("/" + BicServiceRoute.Route + "*", BicServiceRoute.Base);
            
            AddXsdResourceRoutes(DescServiceRoute.Route, () => Encoding.Default.GetString(Resources.DescService));
            server.AddRoute("/" + DescServiceRoute.Route + "*", DescServiceRoute.Base);            

            var endpoints = RouteTools.GetBaseAddrs(data.Xaddrs);
            binding.Settings.ServerConfig.LocalEndpointFilter.Clear(); 
            foreach (var ep in endpoints.ToList())
                binding.Settings.ServerConfig.LocalEndpointFilter.Add(ep);
            
            try
            {
                server.Start(binding.Settings.ServerConfig, cts.Token);
            }
            catch (Exception e)
            {
                SharpDc.Instance.Logger.LogCritical("Error starting Http server. " +
                    $"This could be due to a missing namespace reservation (Windows only): {e.Message}");
                return;
            }

            await postStart().ConfigureAwait(false);
        }

        /// <summary>   Adds an XSD resource routes to 'wsdlProvider'. </summary>
        ///
        /// <param name="servicePath">  Full pathname of the service file. </param>
        /// <param name="wsdlProvider"> The wsdl provider. </param>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception> 
        private void AddXsdResourceRoutes(string servicePath, Func<string> wsdlProvider)
        {
            if (server == null)
                throw new InvalidOperationException("Server not initialized.");
            server.AddRoute("/" + servicePath + "/" + ServiceBaseRoute.WsdlSuffix, 
                async (rq, rp, token) =>
                {
                    rp.ContentType = "application/xml";
                    SharpDc.Instance.Logger.LogTrace($"Serving WSDL for {servicePath} to {rq.RemoteEndpoint}.");
                    await rp.AsText(StringTools.RemoveCrLf(wsdlProvider()), token).ConfigureAwait(false);
                }, HttpMethod.Get);            
            server.AddRoute("/" + servicePath + "/" + WsConstants.XsdFileMessageModel,
                XsdMessageModelProvider, HttpMethod.Get);
            server.AddRoute("/" + servicePath + "/" + WsConstants.XsdFileParticipantModel,
                XsdParticipantModelProvider, HttpMethod.Get);
            server.AddRoute("/" + servicePath + "/" + WsConstants.XsdFileExtensionPoint,
                XsdExtensionPointProvider, HttpMethod.Get);
            server.AddRoute("/" + servicePath + "/" + WsConstants.XsdFileMdpws,
                XsdMdpwsProvider, HttpMethod.Get);
        }

        /// <summary>   XSD mdpws provider. </summary>
        ///
        /// <param name="rq">       The request. </param>
        /// <param name="rp">       The rp. </param>
        /// <param name="token">    A token that allows processing to be cancelled. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task XsdMdpwsProvider(HttpRequest rq, HttpResponse rp, CancellationToken token)
        {
            rp.ContentType = "application/xml";
            SharpDc.Instance.Logger.LogTrace($"Serving resource {WsConstants.XsdFileMdpws} to {rq.RemoteEndpoint}.");
            await rp.AsText(StringTools.RemoveCrLf(Resources.MDPWS), token).ConfigureAwait(false);
        }

        /// <summary>   XSD extension point provider. </summary>
        ///
        /// <param name="rq">       The request. </param>
        /// <param name="rp">       The rp. </param>
        /// <param name="token">    A token that allows processing to be cancelled. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task XsdExtensionPointProvider(HttpRequest rq, HttpResponse rp, CancellationToken token)
        {
            rp.ContentType = "application/xml";
            SharpDc.Instance.Logger.LogTrace($"Serving resource {WsConstants.XsdFileExtensionPoint} to {rq.RemoteEndpoint}.");
            await rp.AsText(StringTools.RemoveCrLf(Resources.ExtensionPoint), token).ConfigureAwait(false);
        }

        /// <summary>   XSD participant model provider. </summary>
        ///
        /// <param name="rq">       The request. </param>
        /// <param name="rp">       The rp. </param>
        /// <param name="token">    A token that allows processing to be cancelled. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task XsdParticipantModelProvider(HttpRequest rq, HttpResponse rp, CancellationToken token)
        {
            rp.ContentType = "application/xml";
            SharpDc.Instance.Logger.LogTrace($"Serving resource {WsConstants.XsdFileParticipantModel} to {rq.RemoteEndpoint}.");
            await rp.AsText(StringTools.RemoveCrLf(Resources.BICEPS_ParticipantModel), token).ConfigureAwait(false);
        }

        /// <summary>   XSD message model provider. </summary>
        ///
        /// <param name="rq">       The request. </param>
        /// <param name="rp">       The rp. </param>
        /// <param name="token">    A token that allows processing to be cancelled. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private async Task XsdMessageModelProvider(HttpRequest rq, HttpResponse rp, CancellationToken token)
        {
            rp.ContentType = "application/xml";
            SharpDc.Instance.Logger.LogTrace($"Serving resource {WsConstants.XsdFileMessageModel} to {rq.RemoteEndpoint}.");
            await rp.AsText(StringTools.RemoveCrLf(Resources.BICEPS_MessageModel), token).ConfigureAwait(false);
        }

        /// <summary>   Stop. </summary>
        ///
        /// <returns>   An asynchronous result. </returns>
        public Task Stop()
        {
            SharpDc.Instance.Logger.LogInformation($"Stopping listening endpoint for port {data.Port}.");
            server?.Dispose();
            data.PropertyChanged -= OnPropertyChanged;
            cts?.Cancel();
            cts?.Dispose();
            SetServiceRoute.Stop();
            getServiceRoute.Stop();
            BicServiceRoute.Stop();
            return Task.CompletedTask;
        }

    }
}