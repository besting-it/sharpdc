﻿// file:	Binding\Device\Mdpws\Service\Ipv4Joiner.cs
//
// summary:	Implements the IPv4 joiner class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace SharpDC.Binding.Device.Mdpws.Service
{
    /// <summary>   An IPv4 joiner. </summary>
    class Ipv4Joiner : IInterfaceJoiner
    {

        /// <summary>   Initializes the interfaces. </summary>
        ///
        /// <param name="addrFamily">           The address family. </param>
        /// <param name="mcastAdr">             The mcast address. </param>
        /// <param name="mcastPort">            The mcast port. </param>
        /// <param name="ipFilter">             (Optional) A filter specifying the IP. </param>
        /// <param name="startMulticastServer"> (Optional) True to start multicast server. </param>
        /// <param name="bindAll">              (Optional) True to bind all. </param>
        ///
        /// <returns>   A List&lt;InterfaceAddress&gt; </returns>
        public List<InterfaceAddress> InitInterfaces(AddressFamily addrFamily, IPAddress mcastAdr, int mcastPort,
            string? ipFilter = null, bool startMulticastServer = true, bool bindAll = true)
        {
            var serverList = new List<InterfaceAddress>();
            var epGroup = new IPEndPoint(mcastAdr, mcastPort);
            NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface networkInterface in networkInterfaces)
            {
                if (NifVisitor.IsInvalidNic(networkInterface, NetworkInterfaceComponent.IPv4))
                    continue;
                foreach (var uac in networkInterface.GetIPProperties().UnicastAddresses)
                {
                    if (uac.Address == null)
                        continue;
                    if (uac.Address.AddressFamily != AddressFamily.InterNetwork)
                        continue;
                    try
                    {
                        var ip = uac.Address.ToString();
                        if (ipFilter == null || ipFilter == "0.0.0.0" || ip.ToUpperInvariant().StartsWith(ipFilter.ToUpperInvariant()))
                        {
                            if (!startMulticastServer || !UdpUtils.IsMulticast(mcastAdr.ToString()))
                            {
                                serverList.Add(new InterfaceAddress() { Server = null, LocalEndpointAdr = ip });
                                SharpDc.Instance.Logger.LogDebug("UDP interface joining: {0} on {1}", epGroup.Address.ToString(), ip);
                                break;
                            }
                            
                            var udpMulticastServer = new UdpClient(addrFamily);
                            udpMulticastServer.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                            udpMulticastServer.Client.Bind(bindAll
                                ? new IPEndPoint(IPAddress.Any, mcastPort)
                                : new IPEndPoint(IPAddress.Parse(ip), mcastPort));
                            udpMulticastServer.JoinMulticastGroup(epGroup.Address, uac.Address);
                            serverList.Add(new InterfaceAddress() { Server = udpMulticastServer, LocalEndpointAdr = ip});
                            SharpDc.Instance.Logger.LogDebug("UDP multicast joining: {0} on {1}", epGroup.Address.ToString(), ip);           
                            break;
                        }
                    }
                    catch (SocketException)
                    {
                        SharpDc.Instance.Logger.LogCritical("UDP multicast joining error: {0} on {1}", epGroup.Address.ToString(), uac.Address.ToString());
                        continue;
                    }
                }
            }

            return serverList;
        }
        
    }
}
