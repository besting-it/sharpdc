﻿// file:	Binding\Device\Mdpws\Service\Ipv6Joiner.cs
//
// summary:	Implements the IPv6 joiner class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace SharpDC.Binding.Device.Mdpws.Service
{
    /// <summary>   An IPv6 joiner. </summary>
    class Ipv6Joiner : IInterfaceJoiner
    {

        /// <summary>   Initializes the interfaces. </summary>
        ///
        /// <param name="addrFamily">           The address family. </param>
        /// <param name="mcastAdr">             The mcast address. </param>
        /// <param name="mcastPort">            The mcast port. </param>
        /// <param name="ipFilter">             (Optional) A filter specifying the IP. </param>
        /// <param name="startMulticastServer"> (Optional) True to start multicast server. </param>
        /// <param name="bindAll">              (Optional) True to bind all. </param>
        ///
        /// <returns>   A List&lt;InterfaceAddress&gt; </returns>
        public List<InterfaceAddress> InitInterfaces(AddressFamily addrFamily, IPAddress mcastAdr, int mcastPort,
            string? ipFilter = null, bool startMulticastServer = true, bool bindAll = true)
        {
            var serverList = new List<InterfaceAddress>();
            NifVisitor.Visit((ip, networkInterface) =>
            {
                try
                {
                    if (ipFilter == null || ipFilter == "::" || ip.ToUpperInvariant().StartsWith(ipFilter.ToUpperInvariant()))
                    {
                        if (!startMulticastServer || !UdpUtils.IsMulticast(mcastAdr.ToString()))
                        {
                            serverList.Add(new InterfaceAddress() { Server = null, LocalEndpointAdr = ip });
                            SharpDc.Instance.Logger.LogDebug("UDP multicast joining: {0} on {1}", mcastAdr.ToString(), ip);
                            return true;
                        }

                        var ipv6MulticastOption = new IPv6MulticastOption(mcastAdr)
                        {
                            InterfaceIndex = networkInterface.GetIPProperties().GetIPv6Properties().Index
                        };
                        var udpMulticastServer = new UdpClient(addrFamily);
                        udpMulticastServer.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                        udpMulticastServer.Client.Bind(bindAll
                            ? new IPEndPoint(IPAddress.IPv6Any, mcastPort)
                            : new IPEndPoint(IPAddress.Parse(ip), mcastPort));
                        udpMulticastServer.JoinMulticastGroup((int)ipv6MulticastOption.InterfaceIndex, ipv6MulticastOption.Group);
                        serverList.Add(new InterfaceAddress() { Server = udpMulticastServer, LocalEndpointAdr = ip });
                        SharpDc.Instance.Logger.LogDebug("UDP multicast joining: {0} on {1}", mcastAdr.ToString(), ip);
                        return true;
                    }
                }
                catch (SocketException)
                {
                    SharpDc.Instance.Logger.LogCritical("UDP multicast joining error: {0}", mcastAdr.ToString());
                }
                return false;
            }, AddressFamily.InterNetworkV6, NetworkInterfaceComponent.IPv6);
            return serverList;
        }

    }
}
