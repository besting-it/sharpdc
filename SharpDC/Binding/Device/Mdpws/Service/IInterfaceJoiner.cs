﻿// file:	Binding\Device\Mdpws\Service\IInterfaceJoiner.cs
//
// summary:	Declares the IInterfaceJoiner interface
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;

namespace SharpDC.Binding.Device.Mdpws.Service
{
    /// <summary>   An interface address. </summary>
    class InterfaceAddress
    {
        /// <summary>   Gets or sets the server. </summary>
        ///
        /// <value> The server. </value>
        public UdpClient? Server { get; set; }

        /// <summary>   Gets or sets the local endpoint address. </summary>
        ///
        /// <value> The local endpoint address. </value>
        public string LocalEndpointAdr { get; set; } = string.Empty;
    }
    
    /// <summary>   A nif visitor. </summary>
    internal class NifVisitor
    {
        /// <summary>   Visits. </summary>
        ///
        /// <param name="action">       The action. </param>
        /// <param name="family">       The family. </param>
        /// <param name="component">    The component. </param>
        static internal void Visit(NifAction action, AddressFamily family, NetworkInterfaceComponent component)
        {
            NetworkInterface[] networkInterfaces = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface networkInterface in networkInterfaces)
            {
                if (IsInvalidNic(networkInterface, component))
                    continue;
                foreach (var uca in networkInterface.GetIPProperties().UnicastAddresses)
                {
                    if (uca.Address == null)
                        continue;
                    if (uca.Address.AddressFamily != family)
                        continue;
                    var ip = uca.Address.ToString();
                    if (action(ip, networkInterface))
                    {
                        break;
                    }
                }
            }
        }

        /// <summary>   Query if 'networkInterface' is invalid NIC. </summary>
        ///
        /// <param name="networkInterface"> The network interface. </param>
        /// <param name="component">        The component. </param>
        ///
        /// <returns>   True if invalid nic, false if not. </returns>
        internal static bool IsInvalidNic(NetworkInterface networkInterface, NetworkInterfaceComponent component)
        {
            return (!networkInterface.Supports(component)
                    || (networkInterface.OperationalStatus != OperationalStatus.Up)
                    || (networkInterface.GetIPProperties().GetIPv6Properties().Index == NetworkInterface.LoopbackInterfaceIndex));
        }
    }

    /// <summary>   Interface for interface joiner. </summary>
    interface IInterfaceJoiner
    {
        /// <summary>   Initializes the interfaces. </summary>
        ///
        /// <param name="addrFamily">           The address family. </param>
        /// <param name="mcastAdr">             The mcast address. </param>
        /// <param name="mcastPort">            The mcast port. </param>
        /// <param name="ipFilter">             A filter specifying the IP. </param>
        /// <param name="startMulticastServer"> True to start multicast server. </param>
        /// <param name="bindAll">              True to bind all. </param>
        ///
        /// <returns>   A List&lt;InterfaceAddress&gt; </returns>
        List<InterfaceAddress> InitInterfaces(AddressFamily addrFamily, IPAddress mcastAdr, int mcastPort,
            string ipFilter, bool startMulticastServer, bool bindAll);
    }
}
