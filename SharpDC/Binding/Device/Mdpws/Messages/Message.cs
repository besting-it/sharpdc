﻿// file:	Binding\Device\Mdpws\Messages\Message.cs
//
// summary:	Implements the message class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Linq;
using System.Xml.Linq;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A message. </summary>
    public class Message
    {
        
        /// <summary>   The header. </summary>
        protected XElement Header { get; private set; }
        
        /// <summary>   The body. </summary>
        protected XElement Body { get; private set; }
        
        /// <summary>   The XML. </summary>
        internal XDocument Xml { get; private set; }

        /// <summary>   Constructor to be used with string content. </summary>
        ///
        /// <param name="xmlContent">           The XML content. </param>
        /// <param name="setRandomMessageId">   (Optional) True to set random message identifier. </param>
        public Message(string xmlContent, bool setRandomMessageId = false)
        {
            Xml = XDocument.Parse(xmlContent);
            Header = GetDescendant(Xml, "Header");
            Body = GetDescendant(Xml, "Body");
            if (setRandomMessageId)
            {
                MessageId = Guid.NewGuid().ToString();
            }
        }
        
        /// <summary>   Constructor to be used with XDocument. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public Message(XDocument doc)
        {
            Xml = doc;
            Header = GetDescendant(Xml, "Header");
            Body = GetDescendant(Xml, "Body");
        }        

        /// <summary>   Gets the data model part. </summary>
        ///
        /// <value> The data model part. </value>
        internal XElement DataModelPart => Body.Descendants().First();
        
        /// <summary>   Gets a descendant. </summary>
        ///
        /// <param name="container">    The container. </param>
        /// <param name="localName">    Name of the local. </param>
        ///
        /// <returns>   The descendant. </returns>
        protected XElement GetDescendant(XContainer container, string localName) 
            => container.Descendants().First(p => p.Name.LocalName == localName);

        /// <summary>   Gets header element. </summary>
        ///
        /// <param name="localName">    Name of the local. </param>
        ///
        /// <returns>   The header element. </returns>
        protected XElement GetHeaderElement(string localName) => GetDescendant(Header, localName);
        /// <summary>   Gets body element. </summary>
        ///
        /// <param name="localName">    Name of the local. </param>
        ///
        /// <returns>   The body element. </returns>
        protected XElement GetBodyElement(string localName) => GetDescendant(Body, localName);
        
        /// <summary>   Gets or sets the identifier of the message. </summary>
        ///
        /// <value> The identifier of the message. </value>
        public string MessageId
        {
            get => GetHeaderElement("MessageID").Value;
            set => GetHeaderElement("MessageID").Value = value;
        }   
        
        /// <summary>   Gets or sets the relates to. </summary>
        ///
        /// <value> The relates to. </value>
        public string RelatesTo
        {
            get => GetHeaderElement("RelatesTo").Value;
            set => GetHeaderElement("RelatesTo").Value = value;
        }         
        
        /// <summary>   Gets or sets to. </summary>
        ///
        /// <value> to. </value>
        public string To
        {
            get => GetHeaderElement("To").Value;
            set => GetHeaderElement("To").Value = value;
        }             
        
        /// <summary>   Gets or sets the action. </summary>
        ///
        /// <value> The action. </value>
        public string Action
        {
            get => GetHeaderElement("Action").Value;
            set => GetHeaderElement("Action").Value = value;
        }

        /// <summary>   Returns a string that represents the current object. </summary>
        ///
        /// <returns>   A string that represents the current object. </returns>
        public override string ToString() => Xml.ToString();
    }
}
