﻿// file:	Binding\Device\Mdpws\Messages\DiscoveryMessage.cs
//
// summary:	Implements the discovery message class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A discovery message. </summary>
    internal class DiscoveryMessage : Message
    {
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="xmlContent">           The XML content. </param>
        /// <param name="setRandomMessageId">   (Optional) True to set random message identifier. </param>
        public DiscoveryMessage(string xmlContent, bool setRandomMessageId = false) : base(xmlContent, setRandomMessageId)
        {
        }
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public DiscoveryMessage(XDocument doc) : base(doc)
        {
        }

        /// <summary>   Gets or sets the epr. </summary>
        ///
        /// <value> The epr. </value>
        public string Epr 
        { 
            get => GetBodyElement("Address").Value;
            set => GetBodyElement("Address").Value = value;
        }

        /// <summary>   Gets or sets the metadata version. </summary>
        ///
        /// <value> The metadata version. </value>
        public int MetadataVersion
        {
            get => int.Parse(GetBodyElement("MetadataVersion").Value);
            set => GetBodyElement("MetadataVersion").Value = value.ToString();
        }
        
        /// <summary>   Gets or sets the application sequence. </summary>
        ///
        /// <value> The application sequence. </value>
        public AppSequence AppSequence
        {
            get
            {
                var el = GetHeaderElement("AppSequence");
                return new AppSequence()
                {
                    InstanceId = int.Parse(el.Attribute("InstanceId").Value),
                    MessageNumber = long.Parse(el.Attribute("MessageNumber").Value),
                };
            }
            set
            {
                var el = GetHeaderElement("AppSequence");
                el.Attribute("InstanceId").Value = value.InstanceId.ToString();
                el.Attribute("MessageNumber").Value = value.MessageNumber.ToString();
            }
        }
    }
}