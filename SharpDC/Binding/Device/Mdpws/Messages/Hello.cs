﻿// file:	Binding\Device\Mdpws\Messages\Hello.cs
//
// summary:	Implements the hello class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A hello. </summary>
    internal class Hello : DiscoveryMessage
    {
        /// <summary>   Default constructor. </summary>
        public Hello() : base(Resources.Hello, true) { }
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="xmlContent">   The XML content. </param>
        public Hello(string xmlContent) : base(xmlContent) { }
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public Hello(XDocument doc) : base(doc)
        {
        }

        /// <summary>   Gets or sets the xaddrs. </summary>
        ///
        /// <value> The xaddrs. </value>
        public string Xaddrs
        {
            get => GetBodyElement("XAddrs").Value;
            set => GetBodyElement("XAddrs").Value = value;
        }
        
        /// <summary>   Gets or sets the types. </summary>
        ///
        /// <value> The types. </value>
        public string Types
        {
            get => GetBodyElement("Types").Value;
            set => GetBodyElement("Types").Value = value;
        }        
        
    }
}
