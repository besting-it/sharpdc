﻿// file:	Binding\Device\Mdpws\Messages\SubscribeResponse.cs
//
// summary:	Implements the subscribe response class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A subscribe response. </summary>
    internal class SubscribeResponse : Message
    {
        /// <summary>   Default constructor. </summary>
        public SubscribeResponse() : base(Resources.SubscribeResponse) { }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public SubscribeResponse(XDocument doc) : base(doc)
        {
        }

        /// <summary>   Gets or sets the subscription manager address. </summary>
        ///
        /// <value> The subscription manager address. </value>
        public string SubscriptionManagerAddress
        {
            get => GetBodyElement("Address").Value;
            set => GetBodyElement("Address").Value = value;
        }  
        
        /// <summary>   Gets or sets the expires. </summary>
        ///
        /// <value> The expires. </value>
        public string Expires
        {
            get => GetBodyElement("Expires").Value;
            set => GetBodyElement("Expires").Value = value;
        } 
        
    }
}
