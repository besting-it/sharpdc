﻿// file:	Binding\Device\Mdpws\Messages\RenewResponse.cs
//
// summary:	Implements the renew response class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A renew response. </summary>
    internal class RenewResponse : Message
    {
        /// <summary>   Default constructor. </summary>
        public RenewResponse() : base(Resources.RenewResponse) { }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public RenewResponse(XDocument doc) : base(doc)
        {
        }
        
        /// <summary>   Gets or sets the expires. </summary>
        ///
        /// <value> The expires. </value>
        public string Expires
        {
            get => GetBodyElement("Expires").Value;
            set => GetBodyElement("Expires").Value = value;
        }

    }
}
