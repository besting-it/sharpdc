﻿// file:	Binding\Device\Mdpws\Messages\GetResponse.cs
//
// summary:	Implements the get response class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A get response. </summary>
    internal class GetResponse : Message
    {
        /// <summary>   The hosted template. </summary>
        private XElement? hostedTemplate;
        /// <summary>   The hosted epr template. </summary>
        private XElement? hostedEprTemplate;

        /// <summary>   Default constructor. </summary>
        public GetResponse() : base(Resources.GetResponse)
        {
            PrepareTemplates();
        }
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public GetResponse(XDocument doc) : base(doc)
        {
        } 
        
        /// <summary>   Prepare templates. </summary>
        protected void PrepareTemplates()
        {
            var parent = GetBodyElement("Relationship");
            hostedTemplate = GetDescendant(parent, "Hosted");
            hostedTemplate.Remove();
            hostedEprTemplate = GetDescendant(hostedTemplate, "EndpointReference");
            hostedEprTemplate.Remove();
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="xmlContent">   The XML content. </param>
        public GetResponse(string xmlContent) : base(xmlContent)
        {
        }

        /// <summary>   Gets or sets the manufacturer. </summary>
        ///
        /// <value> The manufacturer. </value>
        public string Manufacturer
        {
            get => GetBodyElement("Manufacturer").Value;
            set => GetBodyElement("Manufacturer").Value = value;
        }
        
        /// <summary>   Gets or sets the name of the model. </summary>
        ///
        /// <value> The name of the model. </value>
        public string ModelName
        {
            get => GetBodyElement("ModelName").Value;
            set => GetBodyElement("ModelName").Value = value;
        }
        
        /// <summary>   Gets or sets the model number. </summary>
        ///
        /// <value> The model number. </value>
        public string ModelNumber
        {
            get => GetBodyElement("ModelNumber").Value;
            set => GetBodyElement("ModelNumber").Value = value;
        }
        
        /// <summary>   Gets or sets the name of the friendly. </summary>
        ///
        /// <value> The name of the friendly. </value>
        public string FriendlyName
        {
            get => GetBodyElement("FriendlyName").Value;
            set => GetBodyElement("FriendlyName").Value = value;
        }
        
        /// <summary>   Gets or sets the serial number. </summary>
        ///
        /// <value> The serial number. </value>
        public string SerialNumber
        {
            get => GetBodyElement("SerialNumber").Value;
            set => GetBodyElement("SerialNumber").Value = value;
        }   
        
        /// <summary>   Gets or sets the host epr. </summary>
        ///
        /// <value> The host epr. </value>
        public string HostEpr 
        { 
            get => GetDescendant(GetBodyElement("Host"), "Address").Value;
            set => GetDescendant(GetBodyElement("Host"), "Address").Value = value;
        }

        /// <summary>   Adds a hosted section. </summary>
        ///
        /// <param name="addrs">        The addrs. </param>
        /// <param name="serviceTypes"> List of types of the services. </param>
        /// <param name="serviceId">    Identifier for the service. </param>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception> 
        /// 
        public void AddHostedSection(IEnumerable<string> addrs, IEnumerable<string> serviceTypes, string serviceId)
        {
            if (hostedTemplate == null)
                throw new InvalidOperationException("Hosted template not initialized.");            
            if (hostedEprTemplate == null)
                throw new InvalidOperationException("Hosted EPR template not initialized.");            
            var newHostedNode = new XElement(hostedTemplate);
            foreach (var nextAddr in addrs)
            {
                var eprNode = new XElement(hostedEprTemplate);
                GetDescendant(eprNode, "Address").Value = nextAddr;
                newHostedNode.AddFirst(eprNode);
            }
            GetDescendant(newHostedNode, "Types").Value = string.Join(" ", serviceTypes.Select(t => "pm:" + t));
            GetDescendant(newHostedNode, "ServiceId").Value = serviceId;
            GetBodyElement("Relationship").Add(newHostedNode);
        }
    }
}
