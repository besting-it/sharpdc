﻿// file:	Binding\Device\Mdpws\Messages\Probe.cs
//
// summary:	Implements the probe class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A probe. </summary>
    internal class Probe : Message
    {
        /// <summary>   Default constructor. </summary>
        public Probe() : base(Resources.Probe, true) { }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="xmlContent">   The XML content. </param>
        public Probe(string xmlContent) : base(xmlContent) { }
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public Probe(XDocument doc) : base(doc)
        {
        } 

        /// <summary>   Gets or sets the types. </summary>
        ///
        /// <value> The types. </value>
        public string Types
        {
            get => GetBodyElement("Types").Value;
            set => GetBodyElement("Types").Value = value;
        }
        
    }
}
