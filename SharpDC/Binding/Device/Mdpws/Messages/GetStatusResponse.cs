﻿// file:	Binding\Device\Mdpws\Messages\GetStatusResponse.cs
//
// summary:	Implements the get status response class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A get status response. </summary>
    internal class GetStatusResponse : Message
    {
        /// <summary>   Default constructor. </summary>
        public GetStatusResponse() : base(Resources.GetStatusResponse) { }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public GetStatusResponse(XDocument doc) : base(doc)
        {
        }
        
        /// <summary>   Gets or sets the expires. </summary>
        ///
        /// <value> The expires. </value>
        public string Expires
        {
            get => GetBodyElement("Expires").Value;
            set => GetBodyElement("Expires").Value = value;
        }

    }
}
