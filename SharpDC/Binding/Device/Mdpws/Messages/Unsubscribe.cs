﻿// file:	Binding\Device\Mdpws\Messages\Unsubscribe.cs
//
// summary:	Implements the unsubscribe class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   An unsubscribe. </summary>
    internal class Unsubscribe : Message
    {
        /// <summary>   Default constructor. </summary>
        public Unsubscribe() : base(Resources.Unsubscribe) { }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public Unsubscribe(XDocument doc) : base(doc)
        {
        }

    }
}
