﻿// file:	Binding\Device\Mdpws\Messages\InvokeResponse.cs
//
// summary:	Implements the invoke response class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   An invoke response. </summary>
    public class InvokeResponse : Invoke
    {
        /// <summary>   Default constructor. </summary>
        public InvokeResponse() : base(Resources.InvokeResponse) { }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="xmlContent">   The XML content. </param>
        public InvokeResponse(string xmlContent) : base(xmlContent)
        {
        }
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public InvokeResponse(XDocument doc) : base(doc)
        {
        }
    }
}
