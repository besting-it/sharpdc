﻿// file:	Binding\Device\Mdpws\Messages\Renew.cs
//
// summary:	Implements the renew class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A renew. </summary>
    internal class Renew : Message
    {
        /// <summary>   Gets the renew. </summary>
        ///
        /// <value> . </value>
        public Renew() : base(Resources.Renew) { }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public Renew(XDocument doc) : base(doc)
        {
        }
        
        /// <summary>   Gets or sets the expires. </summary>
        ///
        /// <value> The expires. </value>
        public string Expires
        {
            get => GetBodyElement("Expires").Value;
            set => GetBodyElement("Expires").Value = value;
        }

    }
}
