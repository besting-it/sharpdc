﻿// file:	Binding\Device\Mdpws\Messages\UnsubscribeResponse.cs
//
// summary:	Implements the unsubscribe response class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   An unsubscribe response. </summary>
    internal class UnsubscribeResponse : Message
    {
        /// <summary>   Default constructor. </summary>
        public UnsubscribeResponse() : base(Resources.UnsubscribeResponse) { }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public UnsubscribeResponse(XDocument doc) : base(doc)
        {
        }
    }
}
