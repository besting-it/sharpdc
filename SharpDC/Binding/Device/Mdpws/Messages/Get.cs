﻿// file:	Binding\Device\Mdpws\Messages\Get.cs
//
// summary:	Implements the get class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A get. </summary>
    internal class Get : Message
    {
        /// <summary>   Default constructor. </summary>
        public Get() : base(Resources.Get, true) { }
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The Document to get. </param>
        public Get(XDocument doc) : base(doc)
        {
        } 
    }
}
