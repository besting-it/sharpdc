﻿// file:	Binding\Device\Mdpws\Messages\ResolveMatches.cs
//
// summary:	Implements the resolve matches class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A resolve matches. </summary>
    internal class ResolveMatches : Hello
    {
        /// <summary>   Default constructor. </summary>
        public ResolveMatches() : base(Resources.ResolveMatches) { }
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public ResolveMatches(XDocument doc) : base(doc)
        {
        }

    }
}
