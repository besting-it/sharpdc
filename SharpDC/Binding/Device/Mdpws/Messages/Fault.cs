﻿// file:	Binding\Device\Mdpws\Messages\Fault.cs
//
// summary:	Implements the fault class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A fault. </summary>
    internal class Fault : Message
    {
        /// <summary>   Default constructor. </summary>
        public Fault() : base(Resources.SoapFault) { }
        
        /// <summary>   Gets or sets the fault string. </summary>
        ///
        /// <value> The fault string. </value>
        public string FaultString
        {
            get => GetBodyElement("faultstring").Value;
            set => GetBodyElement("faultstring").Value = value;
        }        
    }
}
