﻿// file:	Binding\Device\Mdpws\Messages\Bye.cs
//
// summary:	Implements the bye class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A bye. </summary>
    internal class Bye : DiscoveryMessage
    {
        /// <summary>   Default constructor. </summary>
        public Bye() : base(Resources.Bye, true) { }
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public Bye(XDocument doc) : base(doc)
        {
        } 
        
    }
}