﻿// file:	Binding\Device\Mdpws\Messages\Subscribe.cs
//
// summary:	Implements the subscribe class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A subscribe. </summary>
    internal class Subscribe : Message
    {
        /// <summary>   Default constructor. </summary>
        public Subscribe() : base(Resources.Subscribe, true) { }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public Subscribe(XDocument doc) : base(doc)
        {
        } 
        
        /// <summary>   Gets or sets the notify to address. </summary>
        ///
        /// <value> The notify to address. </value>
        public string NotifyToAddress
        {
            get => ((XElement)GetBodyElement("NotifyTo").FirstNode).Value;
            set => ((XElement)GetBodyElement("NotifyTo").FirstNode).Value = value;
        }  
        
        /// <summary>   Gets or sets the notify to address. </summary>
        ///
        /// <value> The notify to address. </value>
        public string EndToAddress
        {
            get => ((XElement)GetBodyElement("EndTo").FirstNode).Value;
            set => ((XElement)GetBodyElement("EndTo").FirstNode).Value = value;
        }  

        /// <summary>   Gets or sets the expires. </summary>
        ///
        /// <value> The expires. </value>
        public string Expires
        {
            get => GetBodyElement("Expires").Value;
            set => GetBodyElement("Expires").Value = value;
        } 
        
        /// <summary>   Gets or sets the filter. </summary>
        ///
        /// <value> The filter. </value>
        public string Filter
        {
            get => GetBodyElement("Filter").Value;
            set => GetBodyElement("Filter").Value = value;
        }

    }
}
