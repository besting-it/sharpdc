﻿// file:	Binding\Device\Mdpws\Messages\Resolve.cs
//
// summary:	Implements the resolve class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A resolve. </summary>
    internal class Resolve : Message
    {
        /// <summary>   Default constructor. </summary>
        public Resolve() : base(Resources.Resolve, true) { }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="xmlContent">   The XML content. </param>
        public Resolve(string xmlContent) : base(xmlContent) { }
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public Resolve(XDocument doc) : base(doc)
        {
        }        

        /// <summary>   Gets or sets the epr. </summary>
        ///
        /// <value> The epr. </value>
        public string Epr 
        { 
            get => GetBodyElement("Address").Value;
            set => GetBodyElement("Address").Value = value;
        }
        
    }
}
