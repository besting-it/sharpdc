﻿// file:	Binding\Device\Mdpws\Messages\Invoke.cs
//
// summary:	Implements the invoke class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   An invoke. </summary>
    public class Invoke : Message
    {
        /// <summary>   Default constructor. </summary>
        public Invoke() : base(Resources.Invoke, true) { }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="xmlContent">   The XML content. </param>
        public Invoke(string xmlContent) : base(xmlContent)
        {
        }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public Invoke(XDocument doc) : base(doc)
        {
        }

        /// <summary>   Gets or sets the body XML. </summary>
        ///
        /// <value> The body XML. </value>
        public string BodyXml
        {
            get => DataModelPart.ToString();
            set => Body.Add(XElement.Parse(value));
        }

        /// <summary>   Adds to the header. </summary>
        ///
        /// <param name="el">   The el. </param>
        public void AddToHeader(XElement el) => Header.Add(el);
    }
}
