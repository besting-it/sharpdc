﻿// file:	Binding\Device\Mdpws\Messages\ProbeMatches.cs
//
// summary:	Implements the probe matches class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A probe matches. </summary>
    internal class ProbeMatches : Hello
    {
        /// <summary>   Default constructor. </summary>
        public ProbeMatches() : base(Resources.ProbeMatches) { }
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public ProbeMatches(XDocument doc) : base(doc)
        {
        }        

    }
}
