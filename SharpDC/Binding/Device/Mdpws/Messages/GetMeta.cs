﻿// file:	Binding\Device\Mdpws\Messages\GetMeta.cs
//
// summary:	Implements the get meta class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A get meta. </summary>
    internal class GetMeta : Message
    {
        /// <summary>   Default constructor. </summary>
        public GetMeta() : base(Resources.GetMetadata, true) { }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public GetMeta(XDocument doc) : base(doc)
        {
        } 
        
    }
}
