﻿// file:	Binding\Device\Mdpws\Messages\GetMetaResponse.cs
//
// summary:	Implements the get meta response class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A get meta response. </summary>
    internal class GetMetaResponse : GetResponse
    {
        /// <summary>   The streaming template. </summary>
        private readonly XElement? streamingTemplate;
        /// <summary>   True if streaming section added. </summary>
        private bool streamingSectionAdded;

        /// <summary>   Default constructor. </summary>
        public GetMetaResponse() : base(Resources.GetMetadataResponse)
        {
            PrepareTemplates();
            var parent = GetBodyElement("Metadata");
            var section = parent.Descendants().First(d =>
                d.Name.LocalName == "MetadataSection" &&
                d.HasAttributes &&
                d.Attributes().Any((a => a.Value == WsConstants.WaveformService)));
            streamingTemplate = section;
            streamingTemplate.Remove();
            streamingSectionAdded = false;
        }
        
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public GetMetaResponse(XDocument doc) : base(doc)
        {
        } 
        
        /// <summary>   Gets or sets the wsdl location. </summary>
        ///
        /// <value> The wsdl location. </value>
        public string WsdlLocation
        {
            get => GetBodyElement("Location").Value;
            set => GetBodyElement("Location").Value = value;
        }

        /// <summary>   Adds a hosted section. </summary>
        ///
        /// <param name="addr">         The address. </param>
        /// <param name="serviceTypes"> List of types of the services. </param>
        /// <param name="serviceId">    Identifier for the service. </param>
        public void AddHostedSection(string addr, IEnumerable<string> serviceTypes, string serviceId) =>
            base.AddHostedSection(new[] { addr }, serviceTypes, serviceId);
        
        /// <summary>   Gets or sets the UDP stream address. </summary>
        ///
        /// <value> The stream address. </value>
        public string StreamAddress
        {
            get => GetBodyElement("StreamAddress").Value;
            private set => GetBodyElement("StreamAddress").Value = value;
        }

        /// <summary>   Sets UDP stream address. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// 
        /// <param name="adr">  The address. </param>
        public void SetStreamAddress(string adr)
        {
            if (streamingTemplate == null)
                throw new InvalidOperationException("Streaming template not initialized.");
            if (!streamingSectionAdded)
            {
                var newStreamingSection = new XElement(streamingTemplate);
                GetBodyElement("Metadata").Add(newStreamingSection);
                streamingSectionAdded = true;                
            }
            StreamAddress = adr;
        }

    }
}
