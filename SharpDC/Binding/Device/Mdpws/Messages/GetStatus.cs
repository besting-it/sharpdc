﻿// file:	Binding\Device\Mdpws\Messages\GetStatus.cs
//
// summary:	Implements the get status class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using SharpDC.Properties;

namespace SharpDC.Binding.Device.Mdpws.Messages
{
    /// <summary>   A get status. </summary>
    internal class GetStatus : Message
    {
        /// <summary>   Default constructor. </summary>
        public GetStatus() : base(Resources.GetStatus) { }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="doc">  The document. </param>
        public GetStatus(XDocument doc) : base(doc)
        {
        }

    }
}
