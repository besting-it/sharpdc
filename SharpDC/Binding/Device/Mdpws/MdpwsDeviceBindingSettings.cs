﻿// file:	Binding\Device\Mdpws\MdpwsDeviceBindingSettings.cs
//
// summary:	Implements the mdpws device binding settings class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using SharpDC.Binding.Device.Mdpws.Http;

namespace SharpDC.Binding.Device.Mdpws
{
    /// <summary>   The mdpws device binding settings. </summary>
    public class MdpwsDeviceBindingSettings : IDeviceBindingSettings
    {
        /// <summary>   The IPv6 all. </summary>
        public const string Ipv6All = "::";
        /// <summary>   The IPv4 all. </summary>
        public const string Ipv4All = "0.0.0.0";

        /// <summary>   Gets or sets the bind IP. </summary>
        ///
        /// <value> The bind IP. </value>
        public string BindIp { get; set; } = Ipv4All;
        
        /// <summary>   Gets or sets the port streaming. </summary>
        ///
        /// <value> The port streaming. </value>
        public int PortStreaming { get; set; } = 5555;
        
        /// <summary>   Gets or sets a value indicating whether the multicast bind all. </summary>
        ///
        /// <value> True if multicast bind all, false if not. </value>
        public bool MulticastBindAll { get; set; } = true;
        
        /// <summary>   Gets or sets the UDP timeout milliseconds. </summary>
        ///
        /// <value> The UDP timeout milliseconds. </value>
        public int UdpTimeoutMs { get; set; } = 250;

        /// <summary>   Gets or sets the server configuration. </summary>
        ///
        /// <value> The server configuration. </value>
        public HttpServerConfig ServerConfig { get; set; } = new HttpServerConfig() { Port = 6464 };

        /// <summary>   Gets or sets the client configuration. </summary>
        ///
        /// <value> The client configuration. </value>
        public HttpClientConfig ClientConfig { get; set; } = new HttpClientConfig();

    }
    
}