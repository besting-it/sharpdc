﻿// file:	Binding\Device\IDeviceBinding.cs
//
// summary:	Declares the IDeviceBinding interface
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System.Threading.Tasks;
using SharpDC.Provider;

namespace SharpDC.Binding.Device
{
    /// <summary>   Interface for device binding base. </summary>
    public interface IDeviceBindingBase 
    {
        /// <summary>   Gets a value indicating whether this object is initialized. </summary>
        ///
        /// <value> True if this object is initialized, false if not. </value>
        bool IsInitialized { get; }
        /// <summary>   De initialize. </summary>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task DeInitialize();
        /// <summary>   Gets or sets the get binding. </summary>
        ///
        /// <value> The get binding. </value>
        IProviderGetOperationBinding GetBinding { get; set; }
        /// <summary>   Gets or sets the event binding. </summary>
        ///
        /// <value> The event binding. </value>
        IProviderEventSourceBinding EventBinding { get; set; }
        /// <summary>   Gets or sets the set binding. </summary>
        ///
        /// <value> The set binding. </value>
        IProviderSetOperationBinding SetBinding { get; set; }
    }
    
    /// <summary>   Interface for device binding. </summary>
    ///
    /// <typeparam name="SettingsType"> Type of the settings type. </typeparam>
    public interface IDeviceBinding<SettingsType> : IDeviceBindingBase where SettingsType : IDeviceBindingSettings
    {
        /// <summary>   Initializes this object. </summary>
        ///
        /// <param name="settings"> Options for controlling the operation. </param>
        /// <param name="endpoint"> The endpoint. </param>
        /// <param name="data">     The data. </param>
        /// <param name="epr">      The epr. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task Initialize(SettingsType settings, ISdcEndpoint endpoint, SdcProviderData data, string epr);
    }
}
