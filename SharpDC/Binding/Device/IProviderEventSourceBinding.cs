﻿// file:	Binding\Device\IProviderEventSourceBinding.cs
//
// summary:	Declares the IProviderEventSourceBinding interface
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using SharpDC.Provider;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SharpDC.Binding.Device
{
    /// <summary>   Interface for provider event source binding. </summary>
    public interface IProviderEventSourceBinding
    {
        /// <summary>   Handles the episodic metric event described by state. </summary>
        ///
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task HandleEpisodicMetricEvent(AbstractMetricState state);
        /// <summary>   Handles the periodic metric event described by state. </summary>
        ///
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task HandlePeriodicMetricEvent(AbstractMetricState state);
        /// <summary>   Handles the episodic alert event described by state. </summary>
        ///
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task HandleEpisodicAlertEvent(AbstractAlertState state);
        /// <summary>   Handles the periodic alert event described by state. </summary>
        ///
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task HandlePeriodicAlertEvent(AbstractAlertState state);
        /// <summary>   Handles the operation invoked event. </summary>
        ///
        /// <param name="oic">              The oic. </param>
        /// <param name="invocationState">  State of the invocation. </param>
        /// <param name="errorMsg">         Message describing the error. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task HandleOperationInvokedEvent(OperationInvocationContext oic, InvocationState invocationState, string? errorMsg);
        /// <summary>   Handles the episodic context described by state. </summary>
        ///
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task HandleEpisodicContext(AbstractContextState state);
        /// <summary>   Handles the periodic context described by state. </summary>
        ///
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task HandlePeriodicContext(AbstractContextState state);
        /// <summary>   Handles the stream described by state. </summary>
        ///
        /// <param name="state">    The state. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task HandleStream(RealTimeSampleArrayMetricState state);
        /// <summary>   Fire episodic metric event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task FireEpisodicMetricEventReport(EpisodicMetricReport report);
        /// <summary>   Fire episodic context event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task FireEpisodicContextEventReport(EpisodicContextReport report);
        /// <summary>   Fire episodic alert event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task FireEpisodicAlertEventReport(EpisodicAlertReport report);
        /// <summary>   Fire operation invoked event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task FireOperationInvokedEventReport(OperationInvokedReport report);
        /// <summary>   Fire periodic metric event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task FirePeriodicMetricEventReport(PeriodicMetricReport report);
        /// <summary>   Fire periodic context event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task FirePeriodicContextEventReport(PeriodicContextReport report);
        /// <summary>   Fire periodic alert event report. </summary>
        ///
        /// <param name="report">   The report. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task FirePeriodicAlertEventReport(PeriodicAlertReport report);
        /// <summary>   Sends a stream. </summary>
        ///
        /// <param name="stream">   The stream. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        Task SendStream(WaveformStream stream);
    }
}
