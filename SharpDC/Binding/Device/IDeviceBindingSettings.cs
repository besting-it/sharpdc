﻿// file:	Binding\Device\IDeviceBindingSettings.cs
//
// summary:	Declares the IDeviceBindingSettings interface
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

namespace SharpDC.Binding.Device
{
    /// <summary>   Interface for device binding settings. </summary>
    public interface IDeviceBindingSettings
    {
    }
}