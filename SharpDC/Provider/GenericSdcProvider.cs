﻿// file:	Provider\GenericSdcProvider.cs
//
// summary:	Implements the generic sdc provider class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using SharpDC.Binding.Device;

namespace SharpDC.Provider
{
    /// <summary>
    /// Event args for the state updated events
    /// </summary>
    public class StatesUpdatedEventArgs : EventArgs
    {
        public IEnumerable<AbstractState> States { get; set; } = Enumerable.Empty<AbstractState>();
    }
    
    /// <summary>   Interface for generic sdc provider. </summary>
    public interface IGenericSdcProvider
    {
        /// <summary>   Gets the handlers. </summary>
        ///
        /// <value> The handlers. </value>
        IReadOnlyCollection<(string Handle, Type RequestType, Func<SetRequest, Task<InvocationState>> Handler)> Handlers
        {
            get;
        }

        /// <summary>
        /// Async event handler for state updated events.
        /// </summary>
        event Func<object, StatesUpdatedEventArgs, Task> StatesUpdated;
    }
    
    /// <summary>   A generic sdc provider. </summary>
    ///
    /// <typeparam name="DeviceBindingType">            Type of the device binding type. </typeparam>
    /// <typeparam name="DeviceBindingSettingsType">    Type of the device binding settings type. </typeparam>
    public class GenericSdcProvider<DeviceBindingType, DeviceBindingSettingsType> : IGenericSdcProvider, IAsyncDisposable, ISdcEndpoint 
        where DeviceBindingType : IDeviceBinding<DeviceBindingSettingsType> 
        where DeviceBindingSettingsType : IDeviceBindingSettings
    {
        /// <summary>   Gets the binding. </summary>
        ///
        /// <value> The binding. </value>
        public DeviceBindingType Binding { get; }
        /// <summary>   Gets options for controlling the operation. </summary>
        ///
        /// <value> The settings. </value>
        public DeviceBindingSettingsType Settings { get; }
        
        /// <summary>   The handlers. </summary>
        private readonly ConcurrentBag<(string Handle, Type RequestType, Func<SetRequest, Task<InvocationState>> Handler)> handlers 
            = new ConcurrentBag<(string Handle, Type RequestType, Func<SetRequest, Task<InvocationState>> Handler)>();
        
        /// <summary>   The post startup tasks. </summary>
        private readonly ConcurrentBag<Func<Task>> postStartupTasks = new ConcurrentBag<Func<Task>>();        

        /// <summary>   True if is disposed, false if not. </summary>
        private bool isDisposed;
        /// <summary>   Gets the data. </summary>
        ///
        /// <value> The data. </value>
        internal SdcProviderData Data { get; }

        /// <summary>   Gets or sets the handle of the sco. </summary>
        ///
        /// <value> The sco handle. </value>
        public string ScoHandle { get; set; } = "sco";
        
        /// <summary>   Gets the handlers. </summary>
        ///
        /// <value> The handlers. </value>
        public IReadOnlyCollection<(string Handle, Type RequestType, Func<SetRequest, Task<InvocationState>> Handler)> Handlers => handlers;

        /// <summary>
        /// Raised whenever states are updated.
        /// </summary>
        public event Func<object, StatesUpdatedEventArgs, Task> StatesUpdated;

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="endpointReference">    The endpoint reference. </param>
        /// <param name="binding">              The binding. </param>
        /// <param name="settings">             Options for controlling the operation. </param>
        public GenericSdcProvider(string endpointReference, DeviceBindingType binding, DeviceBindingSettingsType settings)
        {
            Data = new SdcProviderData()
            {
                EndpointReference = endpointReference
            };
            Binding = binding;
            Settings = settings;
            SequenceId = new Guid().ToString();
            StatesUpdated = (o, args) => Task.CompletedTask;
        }

        /// <summary>   Adds a handler. </summary>
        /// <param name="handle">  The handle. </param>
        /// <param name="requestType">  The request type. </param>
        /// <param name="handler">  The handler. </param>
        public void AddHandler(string handle, Type requestType, Func<SetRequest, Task<InvocationState>> handler)
        {
            handlers.Add((handle, requestType, handler));
        }
        
        /// <summary>   Adds a post startup task. </summary>
        /// <param name="task">  The task. </param>
        public void AddPostStartupTask(Func<Task> task)
        {
            postStartupTasks.Add(task);
        }        

        /// <summary>   Clears the handlers. </summary>
        public void ClearHandlers() => handlers.Clear();
        
        /// <summary>   Prepares this object for use. </summary>
        ///
        /// <exception cref="ObjectDisposedException">  Thrown when a supplied object has been disposed. </exception>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task Startup()
        {
            if (isDisposed)
                throw new ObjectDisposedException("Provider is disposed.");
            await Binding.Initialize(Settings, this, Data, EndpointReference).ConfigureAwait(false);
            foreach (var task in postStartupTasks)
                await task().ConfigureAwait(false);
        } 

        /// <summary>   Shuts down this object and frees any resources it is using. </summary>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task Shutdown()
        {
            if (isDisposed)
                return;
            await Binding.DeInitialize().ConfigureAwait(false);
            ClearHandlers();
            postStartupTasks.Clear();
            Data.ClearDescriptors();
            Data.ClearStates();
            isDisposed = true;
        }

        /// <summary>
        /// Updates states, i.e. replaces states which have the same descriptor handle. Calling this
        /// function on a provider will updates states locally whereas calling this function on a
        /// consumer will updates states remotely, i.e. on the remote provider.
        /// </summary>
        ///
        /// <param name="states">   Enumerable of states to update. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task UpdateStates(IEnumerable<AbstractState> states)
        {
            var statesToReplace = states.ToList();
            Data.ReplaceStates(statesToReplace);
            foreach (var state in statesToReplace)
            {
                switch (state)
                {
                    case RealTimeSampleArrayMetricState sam:  // Must be checked first since inherits AbstractMetricState
                        await Binding.EventBinding.HandleStream(sam).ConfigureAwait(false); 
                        break;                      
                    case AbstractMetricState ams:
                        await Binding.EventBinding.HandleEpisodicMetricEvent(ams).ConfigureAwait(false);
                        break;
                    case AbstractAlertState aas:
                        await Binding.EventBinding.HandleEpisodicAlertEvent(aas).ConfigureAwait(false);
                        break;
                    case AbstractContextState acs:
                        await Binding.EventBinding.HandleEpisodicContext(acs).ConfigureAwait(false);
                        break;
                }
            }
            Interlocked.Increment(ref mdibVersion);
            var invocationList = StatesUpdated.GetInvocationList();
            await Task.WhenAll(invocationList.Select(i => 
                ((Func<object, StatesUpdatedEventArgs, Task>)i)(this, new StatesUpdatedEventArgs()
                {
                    States = statesToReplace
                }))).ConfigureAwait(false);
        }

        /// <summary>
        /// Updates state's value, i.e. sets the numeric metric value of the state with the same
        /// descriptor handle. Calling this function on a provider will updates states locally whereas
        /// calling this function on a consumer will updates states remotely, i.e. on the remote provider.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">    Passed when updating of the state's value failed. </exception>
        ///
        /// <param name="descriptorHandle"> The descriptor handle. </param>
        /// <param name="value">            The new value. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task UpdateValue(string descriptorHandle, decimal value)
        {
            var state = await this.GetState<NumericMetricState>(descriptorHandle);
            state.MetricValue.Value = Convert.ToDecimal(value);
            await UpdateStates(new [] { state }).ConfigureAwait(false);
        }

        /// <summary>
        /// Updates state's string, i.e. sets the numeric string value of the state with the same
        /// descriptor handle. Calling this function on a provider will updates states locally whereas
        /// calling this function on a consumer will updates states remotely, i.e. on the remote provider.
        /// </summary>
        ///
        /// <exception cref="InvalidOperationException">    Passed when updating of the state's value failed. </exception>
        ///
        /// <param name="descriptorHandle"> The descriptor handle. </param>
        /// <param name="value">            The new string. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        public async Task UpdateString(string descriptorHandle, string value)
        {
            var state = await this.GetState<StringMetricState>(descriptorHandle);
            if (state == null)
                throw new InvalidOperationException($"Failed updating string value, descriptor handle: {descriptorHandle}.");
            state.MetricValue.Value = value;
            await UpdateStates(new [] { state }).ConfigureAwait(false);
        }

        /// <summary>   Returns the endpoint reference. </summary>
        ///
        /// <value> The endpoint reference. </value>
        public string EndpointReference => Data.EndpointReference;

        /// <summary>   Returns the Mdib version. </summary>
        ///
        /// <value> The mdib version. </value>
        public long MdibVersion => Interlocked.Read(ref mdibVersion);
        /// <summary>   The mdib version. </summary>
        long mdibVersion;
        
        /// <summary>   Returns the sequence Id. </summary>
        ///
        /// <value> The identifier of the sequence. </value>
        public string SequenceId { get; set; }

        /// <summary>   Returns the device binding base. </summary>
        ///
        /// <value> The device binding. </value>
        public IDeviceBindingBase DeviceBinding => Binding;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources asynchronously.
        /// </summary>
        ///
        /// <returns>   A task that represents the asynchronous dispose operation. </returns>
        public async ValueTask DisposeAsync() => await Shutdown().ConfigureAwait(false);

        /// <summary>   Returns a clone of the current description. </summary>
        ///
        /// <returns>   The description. </returns>
        public Task<MdDescription> GetDescription()
        {
            var descr = new MdDescription
            {
                DescriptionVersion = (ulong)Data.DescriptorVersion,
                Mds = Data.GetDescriptors()
            };
            return Task.FromResult(descr);
        }

        /// <summary>   Returns a clone of the states. </summary>
        ///
        /// <param name="descriptorHandles">    Enumeration of handles (optional) </param>
        ///
        /// <returns>   The states. </returns>
        public Task<MdState> GetStates(IEnumerable<string> descriptorHandles)
        {
            var states = new MdState
            {
                StateVersion = (ulong)Data.StateVersion
            };
            var enumerable = descriptorHandles as string[] ?? descriptorHandles.ToArray();
            if (!enumerable.Any())
                states.State = Data.GetStates();
            else
                states.State = Data.GetStates().Where(state => enumerable.Any(handle => handle == state.DescriptorHandle)).ToList();
            return Task.FromResult(states);
        }

        /// <summary>   Adds an mds descriptor. </summary>
        ///
        /// <param name="mds">              The mds descriptor. </param>
        /// <param name="mapMdpwsMetadata"> (Optional) True to map mds metadata to mdpws metadata. </param>
        public void AddMds(MdsDescriptor mds, bool mapMdpwsMetadata = true)
        {
            Data.AddDescriptor(mds);
            if (mapMdpwsMetadata)
            {
                var meta = mds.MetaData;
                var dpwsMeta = Data.Meta;
                if (meta.Manufacturer.Any())
                    dpwsMeta.Manufacturer = meta.Manufacturer.First().Value;
                if (meta.ModelName.Any())
                    dpwsMeta.ModelName = meta.ModelName.First().Value;
                if (meta.ModelNumber != null)
                    dpwsMeta.ModelNumber = meta.ModelNumber;
                if (meta.ModelName.Any())
                    dpwsMeta.FriendlyName = meta.ModelName.First().Value;
                if (meta.SerialNumber.Any())
                    dpwsMeta.SerialNumber = meta.SerialNumber.First();
                Data.Meta = dpwsMeta;
            }
            Interlocked.Increment(ref mdibVersion);
        }

        /// <summary>   Replaces an mds descriptor. </summary>
        ///
        /// <param name="mds">  The mds descriptor. </param>
        public void UpdateMds(MdsDescriptor mds)
        {
            RemoveMds(mds.Handle);
            AddMds(mds);
        }

        /// <summary>   Removes an mds descriptor. </summary>
        ///
        /// <param name="handle">   The handle of the descriptor. </param>
        public void RemoveMds(string handle)
        {
            Data.RemoveDescriptor(handle);
            Interlocked.Increment(ref mdibVersion);
        }

        /// <summary>   Adds a state. </summary>
        ///
        /// <param name="state">    The state. </param>
        public void AddState(AbstractState state)
        {
            Data.AddState(state);
            Interlocked.Increment(ref mdibVersion);
        }

        /// <summary>
        /// Programmatically creates operation descriptors and states for given descriptors. Note that handles for
        /// operations are created automatically. The handles matches the ones of the descriptors and
        /// have suffixes matching the name of the provided OperationType, e.g. 'handleSetValue', where
        /// 'handle' is the descriptor handle and OperationType is 'SetValue'. This method creates an Sco
        /// node, if needed.
        /// </summary>
        ///
        /// <typeparam name="OperationType">    Type of the operation type. </typeparam>
        /// <param name="mds">          The Mds. </param>
        /// <param name="descriptors">  The descriptors. </param>
        ///
        /// <returns>
        /// IEnumerable of tuples containing clones (OperationDescriptor, OperationState). In case of
        /// activate, the first item is the provided descriptor.
        /// </returns>
        ///
        /// <exception cref="InvalidOperationException">    Passed when wrong descriptor type found. </exception>   
        public IEnumerable<(AbstractOperationDescriptor OperationDescriptor, AbstractOperationState OperationState)>
            CreateSetOperations<OperationType>(MdsDescriptor mds, IEnumerable<AbstractDescriptor> descriptors)
            where OperationType : AbstractSet
        {
            return CreateSetOperations<OperationType>(mds, descriptors, Enumerable.Empty<SafetyReqType>());
        }

        /// <summary>
        /// Programmatically creates operation descriptors and states for given descriptors. Note that handles for
        /// operations are created automatically. The handles matches the ones of the descriptors and
        /// have suffixes matching the name of the provided OperationType, e.g. 'handleSetValue', where
        /// 'handle' is the descriptor handle and OperationType is 'SetValue'. This method creates an Sco
        /// node, if needed.
        /// </summary>
        ///
        /// <typeparam name="OperationType">    Type of the operation type. </typeparam>
        /// <param name="mds">          The Mds. </param>
        /// <param name="descriptors">  The descriptors. </param>
        /// <param name="safetyReqs">   SafetyReq types. </param>
        ///
        /// <returns>
        /// IEnumerable of tuples containing clones (OperationDescriptor, OperationState). In case of
        /// activate, the first item is the provided descriptor.
        /// </returns>
        ///
        /// <exception cref="InvalidOperationException">    Passed when wrong descriptor type found. </exception>
        /// <exception cref="ArgumentException">    Passed when enumerable counts don't match. </exception>
        public IEnumerable<(AbstractOperationDescriptor OperationDescriptor, AbstractOperationState OperationState)> CreateSetOperations<OperationType>(MdsDescriptor mds, 
            IEnumerable<AbstractDescriptor> descriptors, IEnumerable<SafetyReqType> safetyReqs) where OperationType : AbstractSet
        {
            mds.Sco ??= new ScoDescriptor() { Handle = ScoHandle, Type = new CodedValue() { Code = "0"}};
            var operations = mds.Sco.Operation;
            var retList = new List<(AbstractOperationDescriptor OperationDescriptor, AbstractOperationState OperationState)>();
            var safetyReqTypes = safetyReqs.ToList();
            var abstractDescriptors = descriptors.ToList();
            if (safetyReqTypes.Any() && abstractDescriptors.Count != safetyReqTypes.Count)
                throw new ArgumentException("SafetyReq count must match descriptor count");
            var safetyEnum = safetyReqTypes.GetEnumerator();
            foreach (var nextOp in abstractDescriptors)
            {
                var (operationDescriptor, operationState) = CreateSetOperation<OperationType>(nextOp);
                if (safetyEnum.MoveNext())
                {
                    var nextSafetyReq = safetyEnum.Current;
                    if (nextSafetyReq == null)
                        throw new NullReferenceException("Safety requirement is null");
                    var ext = new ExtensionType();
                    var xml = new XmlDocument();
                    xml.LoadXml(nextSafetyReq.Serialize());
                    ext.Any.Add(xml.DocumentElement);
                    operationDescriptor.Extension = ext;
                }
                operations.Add(operationDescriptor);
                AddState(operationState);
                retList.Add((operationDescriptor.Clone(), operationState.Clone()));
            }
            safetyEnum.Dispose();
            UpdateMds(mds);
            return retList;
        }

        /// <summary>   Creates set operation. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <typeparam name="OperationType">    Type of the operation type. </typeparam>
        /// <param name="descriptor">   The descriptor. </param>
        ///
        /// <returns>   The new set operation. </returns>
        private static (AbstractOperationDescriptor OperationDescriptor, AbstractOperationState OperationState) CreateSetOperation<OperationType>(AbstractDescriptor descriptor) 
            where OperationType : AbstractSet
        {
            if (typeof(OperationType) == typeof(SetValue))
            {
                return (new SetValueOperationDescriptor() { Handle = descriptor.Handle + nameof(SetValue), 
                        OperationTarget = descriptor.Handle, Type = new CodedValue() { Code = "0"} }, 
                    new SetValueOperationState() { DescriptorHandle = descriptor.Handle + nameof(SetValue)});
            }
            if (typeof(OperationType) == typeof(SetString))
            {
                return (new SetStringOperationDescriptor() { Handle = descriptor.Handle + nameof(SetString), 
                        OperationTarget = descriptor.Handle, Type = new CodedValue() { Code = "0"} }, 
                    new SetStringOperationState() { DescriptorHandle = descriptor.Handle + nameof(SetString) });
            }
            if (typeof(OperationType) == typeof(SetMetricState))
            {
                return (new SetMetricStateOperationDescriptor(){ Handle = descriptor.Handle + nameof(SetMetricState), 
                        OperationTarget = descriptor.Handle, Type = new CodedValue() { Code = "0"} },
                    new SetMetricStateOperationState() { DescriptorHandle = descriptor.Handle + nameof(SetMetricState) });
            }
            if (typeof(OperationType) == typeof(SetContextState))
            {
                return (new SetContextStateOperationDescriptor(){ Handle = descriptor.Handle + nameof(SetContextState), 
                        OperationTarget = descriptor.Handle, Type = new CodedValue() { Code = "0"} },
                    new SetContextStateOperationState() { DescriptorHandle = descriptor.Handle + nameof(SetContextState) });
            }
            if (typeof(OperationType) == typeof(SetAlertState))
            {
                return (new SetAlertStateOperationDescriptor(){ Handle = descriptor.Handle + nameof(SetAlertState), 
                        OperationTarget = descriptor.Handle, Type = new CodedValue() { Code = "0"} },
                    new SetAlertStateOperationState() { DescriptorHandle = descriptor.Handle + nameof(SetAlertState) });
            }            
            if (typeof(OperationType) == typeof(Activate))
            {
                // In case of activate, the descriptor provided is already the ActivateOperationDescriptor
                if (!(descriptor is ActivateOperationDescriptor aod))
                    throw new InvalidOperationException($"Descriptor must be of type {nameof(ActivateOperationDescriptor)}");
                return (aod, new ActivateOperationState() { DescriptorHandle = descriptor.Handle});
            }
            throw new InvalidOperationException("Descriptor type not supported for creating a set operation.");
        }
    }

}
