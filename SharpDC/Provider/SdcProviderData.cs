﻿// file:	Provider\SdcProviderData.cs
//
// summary:	Implements the sdc provider data class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using SharpDC.Binding.Device.Mdpws;
using SharpDC.Misc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;

namespace SharpDC.Provider
{
    /// <summary>   A sdc provider data. </summary>
    public class SdcProviderData : DefaultPropertyChanged
    {

        /// <summary>   Default constructor. </summary>
        public SdcProviderData()
        {
            Descriptors.ListChanged += (sender, args) => Interlocked.Increment(ref descriptorVersion);
        }

        /// <summary>   Gets or sets the endpoint reference. </summary>
        ///
        /// <value> The endpoint reference. </value>
        internal string EndpointReference { get => endpointReference; set => this.Set(value, ref endpointReference); }
        /// <summary>   The endpoint reference. </summary>
        private string endpointReference = string.Empty;

        /// <summary>   Gets the application sequence. </summary>
        ///
        /// <value> The application sequence. </value>
        internal AppSequence AppSequence { get; } = new AppSequence();

        /// <summary>   Gets or sets the metadata version. </summary>
        ///
        /// <value> The metadata version. </value>
        internal int MetadataVersion { get => metadataVersion; set => this.Set(value, ref metadataVersion); }
        /// <summary>   The metadata version. </summary>
        private int metadataVersion;

        /// <summary>   Gets or sets the xaddrs. </summary>
        ///
        /// <value> The xaddrs. </value>
        internal string Xaddrs { get => xaddrs; set => this.Set(value, ref xaddrs); }
        /// <summary>   The xaddrs. </summary>
        private string xaddrs = string.Empty;

        /// <summary>   Gets or sets the port. </summary>
        ///
        /// <value> The port. </value>
        internal int Port { get => port; set => this.Set(value, ref port); }
        /// <summary>   The port. </summary>
        private int port;

        /// <summary>   Gets or sets the meta. </summary>
        ///
        /// <value> The meta. </value>
        internal DpwsMetadata Meta { get => new DpwsMetadata(meta); set => this.Set(new DpwsMetadata(value), ref meta); }
        /// <summary>   The meta. </summary>
        private DpwsMetadata meta = new DpwsMetadata();

        /// <summary>   Gets the descriptors. </summary>
        ///
        /// <value> The descriptors. </value>
        private BindingList<MdsDescriptor> Descriptors { get; } = new BindingList<MdsDescriptor>();

        /// <summary>   Gets the states. </summary>
        ///
        /// <value> The states. </value>
        private Dictionary<string, AbstractState> States { get; } = new Dictionary<string, AbstractState>();

        /// <summary>   Adds a state. </summary>
        ///
        /// <param name="state">    The state. </param>
        internal void AddState(AbstractState state)
        {
            lock (States)
            {
                States.Add(state.DescriptorHandle, state.Clone());
                Interlocked.Increment(ref stateVersion);
            }
        }
        
        /// <summary>   Replace states. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="statesToReplace">  The states to replace. </param>
        internal void ReplaceStates(IEnumerable<AbstractState> statesToReplace)
        {
            lock (States)
            {
                foreach (var state in statesToReplace)
                {
                    if (States.ContainsKey(state.DescriptorHandle))
                    {
                        var clone = state.Clone();
                        clone.StateVersion++;
                        States[state.DescriptorHandle] = clone;
                    }
                    else
                        throw new InvalidOperationException("Failed to update state with handle: " + state.DescriptorHandle);
                    Interlocked.Increment(ref stateVersion);
                }
            }
        }        

        /// <summary>   Gets the states. </summary>
        ///
        /// <returns>   The states. </returns>
        internal List<AbstractState> GetStates()
        {
            lock (States)
            {
                return States.Values.Select(s => s.Clone()).ToList();
            }
        }

        /// <summary>   Clears the states. </summary>
        internal void ClearStates()
        {
            lock (States)
            {
                States.Clear();
            }
        }

        /// <summary>   Adds a descriptor. </summary>
        ///
        /// <param name="descriptor">   The descriptor. </param>
        internal void AddDescriptor(MdsDescriptor descriptor)
        {
            lock (Descriptors)
            {
                Descriptors.Add(descriptor.Clone());
            }
        }
        
        /// <summary>   Removes the descriptor described by handle. </summary>
        ///
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        ///
        /// <param name="handle">   The handle. </param>
        internal void RemoveDescriptor(string handle)
        {
            lock (Descriptors)
            {
                var descr = Descriptors.FirstOrDefault(d => d.Handle == handle);
                if (descr == null)
                    throw new InvalidOperationException("Failed removing mds descriptor.");
                Descriptors.Remove(descr);
            }
        }        

        /// <summary>   Gets the descriptors. </summary>
        ///
        /// <returns>   The descriptors. </returns>
        internal List<MdsDescriptor> GetDescriptors()
        {
            lock (Descriptors)
            {
                return Descriptors.Select(s => s.Clone()).ToList();
            }
        }

        /// <summary>   Clears the descriptors. </summary>
        internal void ClearDescriptors()
        {
            lock (Descriptors)
            {
                Descriptors.Clear();
            }
        }

        /// <summary>   Gets or sets the descriptor version. </summary>
        ///
        /// <value> The descriptor version. </value>
        internal long DescriptorVersion { get => Interlocked.Read(ref descriptorVersion); set => Interlocked.Exchange(ref descriptorVersion, value); }
        /// <summary>   The descriptor version. </summary>
        long descriptorVersion;

        /// <summary>   Gets or sets the state version. </summary>
        ///
        /// <value> The state version. </value>
        internal long StateVersion { get => Interlocked.Read(ref stateVersion); set => Interlocked.Exchange(ref stateVersion, value); }
        /// <summary>   The state version. </summary>
        long stateVersion;
        
        /// <summary>   Gets or sets the port streaming. </summary>
        ///
        /// <value> The port streaming. </value>
        public int PortStreaming { get => portStreaming; set => this.Set(value, ref portStreaming); }
        /// <summary>   The port streaming. </summary>
        private int portStreaming;
        
        /// <summary>   Gets a value indicating whether address is IPv6. </summary>
        ///
        /// <value> True if ipv6, false if not. </value>
        public bool IsIpV6 => Xaddrs.Contains("[");
        
        /// <summary>   Gets a value indicating whether address is secure. </summary>
        ///
        /// <value> True if secure, false if not. </value>
        public bool IsSecure => Xaddrs.Contains("https");        

    }

    /// <summary>   The dpws metadata. </summary>
    public class DpwsMetadata
    {
        /// <summary>   The default manufacturer. </summary>
        private const string DefaultManufacturer = "Unknown manufacturer";
        /// <summary>   The default model name. </summary>
        private const string DefaultModelName = "SharpDC device";
        /// <summary>   The default model number. </summary>
        private const string DefaultModelNumber = "1";
        /// <summary>   The default friendly name. </summary>
        private const string DefaultFriendlyName = "SharpDC";
        /// <summary>   The default serial number. </summary>
        private const string DefaultSerialNumber = "1";

        /// <summary>   Default constructor. </summary>
        internal DpwsMetadata() { }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="other">    The other. </param>
        public DpwsMetadata(DpwsMetadata other)
        {
            Manufacturer = other.Manufacturer;
            ModelName = other.ModelName;
            ModelNumber = other.ModelNumber;
            FriendlyName = other.FriendlyName;
            SerialNumber = other.SerialNumber;
        }

        /// <summary>   Gets or sets the manufacturer. </summary>
        ///
        /// <value> The manufacturer. </value>
        public string Manufacturer { get; set; } = DefaultManufacturer;
        /// <summary>   Gets or sets the name of the model. </summary>
        ///
        /// <value> The name of the model. </value>
        public string ModelName { get; set; } = DefaultModelName;
        /// <summary>   Gets or sets the model number. </summary>
        ///
        /// <value> The model number. </value>
        public string ModelNumber { get; set; } = DefaultModelNumber;
        /// <summary>   Gets or sets the name of the friendly. </summary>
        ///
        /// <value> The name of the friendly. </value>
        public string FriendlyName { get; set; } = DefaultFriendlyName;
        /// <summary>   Gets or sets the serial number. </summary>
        ///
        /// <value> The serial number. </value>
        public string SerialNumber { get; set; } = DefaultSerialNumber;
    }
}