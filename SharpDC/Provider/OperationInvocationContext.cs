﻿// file:	Provider\OperationInvocationContext.cs
//
// summary:	Implements the operation invocation context class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Collections.Generic;
using System.Text;

namespace SharpDC.Provider
{
    /// <summary>   An operation invocation context. </summary>
    public class OperationInvocationContext
    {
        /// <summary>   Gets or sets the handle of the operation. </summary>
        ///
        /// <value> The operation handle. </value>
        public string OperationHandle { get; set; }
        /// <summary>   Gets or sets the operation target. </summary>
        ///
        /// <value> The operation target. </value>
        public string OperationTarget { get; set; }
        /// <summary>   Gets or sets the identifier of the transaction. </summary>
        ///
        /// <value> The identifier of the transaction. </value>
        public long TransactionId { get; set; }

        /// <summary>   Constructor. </summary>
        ///
        /// <param name="operationHandle">  Handle of the operation. </param>
        /// <param name="operationTarget">  The operation target. </param>
        /// <param name="transactionId">    Identifier for the transaction. </param>
        public OperationInvocationContext(string operationHandle, string operationTarget, long transactionId)
        {
            OperationHandle = operationHandle;
            OperationTarget = operationTarget;
            TransactionId = transactionId;
        }
    }
}
