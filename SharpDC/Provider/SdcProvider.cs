﻿// file:	Provider\SdcProvider.cs
//
// summary:	Implements the sdc provider class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using SharpDC.Binding.Device.Mdpws;

namespace SharpDC.Provider
{
    /// <summary>   An sdc provider. </summary>
    public class SdcProvider : GenericSdcProvider<MdpwsDeviceBinding, MdpwsDeviceBindingSettings>
    {
        /// <summary>   Constructor. </summary>
        ///
        /// <param name="endpointReference">    The endpoint reference. </param>
        public SdcProvider(string endpointReference) : base(endpointReference, new MdpwsDeviceBinding(), new MdpwsDeviceBindingSettings())
        {
        } 
    }
}