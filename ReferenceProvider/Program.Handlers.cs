﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SharpDC;
using SharpDC.Binding.Device;
using SharpDC.Provider;

namespace ReferenceProvider
{
    partial class Program
    {
        /// <summary>   The random number generator. </summary>
        private static Random rnd = new Random();
        
        /// <summary>
        /// Initialize handler callbacks and simulators
        /// </summary>
        /// <param name="provider"> The provider. </param>
        /// <param name="cts"> A cancellation token. </param>
        private static async Task Initialize(SdcProvider provider, CancellationTokenSource cts)
        {
            // Simple states
            await CreateNumeric("numeric.ch0.vmd0", provider, cts, DefaultHandleNumeric, false).ConfigureAwait(false);
            await CreateEnumString("enumstring.ch0.vmd0", provider, cts, DefaultHandleString).ConfigureAwait(false);
            await CreateString("string.ch0.vmd0", provider, cts, DefaultHandleString).ConfigureAwait(false);
            
            await CreateNumeric("numeric.ch1.vmd0", provider, cts, DefaultHandleNumeric, true).ConfigureAwait(false);
            await CreateEnumString("enumstring2.ch0.vmd0", provider, cts, DefaultHandleString).ConfigureAwait(false);
            await CreateString("string2.ch0.vmd1", provider, cts, DefaultHandleString).ConfigureAwait(false);
            
            await CreateNumeric("numeric.ch0.vmd1", provider, cts, DefaultHandleNumeric, false).ConfigureAwait(false);
            await CreateEnumString("enumstring.ch0.vmd1", provider, cts, DefaultHandleString).ConfigureAwait(false);
            await CreateString("string.ch0.vmd1", provider, cts, DefaultHandleString).ConfigureAwait(false);
            
            await CreateEnumString("DN_METRIC", provider, cts, DefaultHandleString).ConfigureAwait(false);
            
            // Contexts
            await CreatePatientContext("PC.mds0", provider, cts, DefaultHandleContext).ConfigureAwait(false);
            await CreateLocationContext("LC.mds0", provider, cts, DefaultHandleContext).ConfigureAwait(false);
            
            // Waveform
            var descr = await provider.GetMetricDescriptor<RealTimeSampleArrayMetricDescriptor>("rtsa.ch0.vmd0");
            StartWaveformSimulation(provider, cts.Token, descr.Handle, 60, 80, descr.Resolution);
            
            // Alerts
            await CreateAlertCondition("ac0.vmd0.mds0", provider, cts, DefaultHandleAlert).ConfigureAwait(false);
            await CreateAlertCondition("ac0.mds0", provider, cts, DefaultHandleAlert).ConfigureAwait(false);
            await CreateAlertSignal("as0.vmd0.mds0", provider, cts, DefaultHandleAlert).ConfigureAwait(false);
            await CreateAlertSignal("as0.mds0", provider, cts, DefaultHandleAlert).ConfigureAwait(false);
            StartAlertSimulation(provider, cts.Token, "ac0.vmd0.mds0", "as0.vmd0.mds0");
            
            // Activates
            provider.AddHandler("AP__ON", typeof(Activate), DefaultHandleActivate);
            provider.AddHandler("AP__CANCEL", typeof(Activate), DefaultHandleActivate);
            provider.AddHandler("actop.vmd1_sco_0", typeof(Activate), DefaultHandleActivate);
            provider.AddHandler("actop.mds0_sco_0", typeof(Activate), DefaultHandleActivate);
        }

        private static async Task CreateNumeric(string handle, SdcProvider provider, CancellationTokenSource cts,
            Func<SetRequest, Task<InvocationState>> handler, bool simulate)
        {
            var descr = await provider.GetMetricDescriptor<NumericMetricDescriptor>(handle);
            provider.AddHandler(descr.Handle, typeof(SetValue), handler);
            provider.AddHandler(descr.Handle, typeof(SetMetricState), DefaultHandleMetricState);
            var tr = descr.TechnicalRange.First();
            provider.AddPostStartupTask(async () => await provider.UpdateValue(descr.Handle, tr.Lower).ConfigureAwait(false));
            if (simulate)
                StartNumericSimulation(provider, cts.Token, descr.Handle, tr.Lower, tr.Upper, descr.Resolution); 
        }
        
        private static async Task CreateEnumString(string handle, SdcProvider provider, CancellationTokenSource cts,
            Func<SetRequest, Task<InvocationState>> handler)
        {
            var descr = await provider.GetMetricDescriptor<EnumStringMetricDescriptor>(handle);
            provider.AddHandler(descr.Handle, typeof(SetString), handler);
            provider.AddHandler(descr.Handle, typeof(SetMetricState), DefaultHandleMetricState);
            var av = descr.AllowedValue.First();
            provider.AddPostStartupTask(async () => await provider.UpdateString(descr.Handle, av.Value).ConfigureAwait(false));
        }

        private static async Task CreateString(string handle, SdcProvider provider, CancellationTokenSource cts,
            Func<SetRequest, Task<InvocationState>> handler)
        {
            var descr = await provider.GetMetricDescriptor<StringMetricDescriptor>(handle);
            provider.AddHandler(descr.Handle, typeof(SetString), handler);
            provider.AddHandler(descr.Handle, typeof(SetMetricState), DefaultHandleMetricState);            
            provider.AddPostStartupTask(async () => await provider.UpdateString(descr.Handle, string.Empty).ConfigureAwait(false));
        }
        
        private static async Task CreatePatientContext(string handle, SdcProvider provider, CancellationTokenSource cts,
            Func<SetRequest, Task<InvocationState>> handler)
        {
            var descr = await provider.GetContextDescriptor<PatientContextDescriptor>(handle);
            provider.AddHandler(descr.Handle, typeof(SetContextState), handler);
        }
        
        private static async Task CreateLocationContext(string handle, SdcProvider provider, CancellationTokenSource cts,
            Func<SetRequest, Task<InvocationState>> handler)
        {
            var descr = await provider.GetContextDescriptor<LocationContextDescriptor>(handle);
            provider.AddHandler(descr.Handle, typeof(SetContextState), handler);
        }      
        
        private static async Task CreateAlertCondition(string handle, SdcProvider provider, CancellationTokenSource cts,
            Func<SetRequest, Task<InvocationState>> handler)
        {
            var descr = await provider.GetAlertConditionDescriptor(handle);
            provider.AddHandler(descr.Handle, typeof(SetAlertState), handler);
        } 
        
        private static async Task CreateAlertSignal(string handle, SdcProvider provider, CancellationTokenSource cts,
            Func<SetRequest, Task<InvocationState>> handler)
        {
            var descr = await provider.GetAlertSignalDescriptor(handle);
            provider.AddHandler(descr.Handle, typeof(SetAlertState), handler);
        }   
        
        private static async Task<InvocationState> DefaultHandleMetricState(SetRequest request)
        {
            var requestedStates = request.Get<SetMetricState>().ProposedMetricState;
            await request.Endpoint.UpdateStates(requestedStates).ConfigureAwait(false);
            return InvocationState.Fin;
        }        

        private static async Task<InvocationState> DefaultHandleNumeric(SetRequest request)
        {
            var requestedValue = request.Get<SetValue>().RequestedNumericValue;
            await request.Endpoint.UpdateValue(request.Context.OperationTarget, requestedValue).ConfigureAwait(false);
            return InvocationState.Fin;
        }
        
        private static async Task<InvocationState> DefaultHandleString(SetRequest request)
        {
            var requestedValue = request.Get<SetString>().RequestedStringValue;
            await request.Endpoint.UpdateString(request.Context.OperationTarget, requestedValue).ConfigureAwait(false);
            return InvocationState.Fin;
        }  
        
        private static async Task<InvocationState> DefaultHandleContext(SetRequest request)
        {
            var requestedValue = request.Get<SetContextState>().ProposedContextState;
            await request.Endpoint.UpdateStates(requestedValue);
            return InvocationState.Fin;
        }       
        
        private static async Task<InvocationState> DefaultHandleAlert(SetRequest request)
        {
            var requestedValue = request.Get<SetAlertState>().ProposedAlertState;
            await request.Endpoint.UpdateState(requestedValue);
            return InvocationState.Fin;
        }       
        
        private static Task<InvocationState> DefaultHandleActivate(SetRequest request)
        {
            return Task.FromResult(InvocationState.Fin);
        }   

    }
}