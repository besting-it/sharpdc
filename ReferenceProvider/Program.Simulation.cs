﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SharpDC;
using SharpDC.Provider;

namespace ReferenceProvider
{
    partial class Program
    {

        static async void StartNumericSimulation(SdcProvider provider, CancellationToken token, 
            string handle, decimal minValue, decimal maxValue, decimal resolution, int minDelay = 3000, int maxDelay = 5000)
        {
            var lastValue = (double)minValue + rnd.NextDouble() * (double)(maxValue - minValue); 
            try
            {
                while (true)
                {
                    await Task.Delay(rnd.Next(minDelay, maxDelay), token).ConfigureAwait(false);
                    token.ThrowIfCancellationRequested();
                    double next = rnd.NextDouble();
                    lastValue += next > 0.5 ? next : -next;
                    var value = Math.Round(Math.Min((double)maxValue, Math.Max(lastValue, (double)minValue)), 
                        (int)Math.Log10(1.0 / (double)resolution));
                    Console.WriteLine($@"Setting value {value} for metric {handle}");
                    await provider.UpdateValue(handle, (decimal)value).ConfigureAwait(false);
                    lastValue = value;
                }
            }
            catch (OperationCanceledException)
            {
            }
            catch (Exception e)
            {
                Console.WriteLine($@"Numerical simulation stopped: {e.Message}");
            }
        }
        
        static async void StartWaveformSimulation(SdcProvider provider, CancellationToken token, 
            string handle, decimal minValue, decimal maxValue, decimal resolution, int minDelay = 3000, int maxDelay = 5000, int count = 8)
        {
            var lastValue = (double)minValue + rnd.NextDouble() * (double)(maxValue - minValue); 
            try
            {
                while (true)
                {
                    var state = await provider.GetState<RealTimeSampleArrayMetricState>(handle).ConfigureAwait(false);
                    if (state.MetricValue.Samples.Any())
                        lastValue = (double)state.MetricValue.Samples.Last();
                    state.MetricValue.Samples = new List<decimal>();
                    await Task.Delay(rnd.Next(minDelay, maxDelay), token).ConfigureAwait(false);
                    token.ThrowIfCancellationRequested();
                    for (int i = 0; i < count; i++)
                    {
                        double next = rnd.NextDouble();
                        lastValue += next > 0.5 ? next : -next;
                        var value = Math.Round(Math.Min((double)maxValue, Math.Max(lastValue, (double)minValue)), 
                            (int)Math.Log10(1.0 / (double)resolution));
                        state.MetricValue.Samples.Add((decimal)value);
                    }
                    Console.WriteLine($@"Updating {count} stream samples for metric {handle}");
                    await provider.UpdateState(state).ConfigureAwait(false);
                }
            }
            catch (OperationCanceledException)
            {
            }
            catch (Exception e)
            {
                Console.WriteLine($@"Waveform simulation stopped: {e.Message}");
            }
        }
        
        static async void StartAlertSimulation(SdcProvider provider, CancellationToken token, 
            string conditionStateHandle, string signalStateHandle, int minDelay = 1500, int maxDelay = 2500)
        {
            try
            {
                while (true)
                {
                    await Task.Delay(rnd.Next(minDelay, maxDelay), token).ConfigureAwait(false);
                    token.ThrowIfCancellationRequested();
                    var condState = await provider.GetState<AlertConditionState>(conditionStateHandle)
                        .ConfigureAwait(false);
                    var signalState = await provider.GetState<AlertSignalState>(signalStateHandle)
                        .ConfigureAwait(false);
                    // Toggle state
                    condState.Presence = !condState.Presence;
                    if (condState.Presence)
                    {
                        switch (signalState.Presence)
                        {
                            case AlertSignalPresence.Off:
                                signalState.Presence = AlertSignalPresence.On;
                                break;
                        }
                    }
                    else
                    {
                        signalState.Presence = AlertSignalPresence.Off;
                    }
                    Console.WriteLine($@"Updating alerts for handles {conditionStateHandle}, {signalStateHandle}: condition presence = {condState.Presence}, signal presence = {signalState.Presence}");
                    await provider.UpdateStates(new AbstractAlertState[] { condState, signalState }).ConfigureAwait(false);                    
                }
            }
            catch (OperationCanceledException)
            {
            }
            catch (Exception e)
            {
                Console.WriteLine($@"Alert simulation stopped: {e.Message}");
            }
        }                
        
    }
}