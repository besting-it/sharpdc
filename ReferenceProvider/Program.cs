﻿// file:	Program.cs
//
// summary:	Implements the program class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.IO;
using System.Linq;
using System.Security.Authentication;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SharpDC;
using SharpDC.Binding.Device.Mdpws;
using SharpDC.Binding.Device.Mdpws.Http;
using SharpDC.Provider;

namespace ReferenceProvider
{
    /// <summary>   Reference provider.
    /// https://confluence.hl7.org/pages/viewpage.action?pageId=113676845
    /// </summary>
    partial class Program
    {
           
        /// <summary>   Main entry-point for this application. </summary>
        ///
        /// <param name="args"> An array of command-line argument strings. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private static async Task Main(string[] args)
        {
            // Initialize
            SharpDc.Instance.Logger.Level = LogLevel.Trace;
            SharpDc.Instance.SetSchemaValidationMode(SchemaValidationMode.None);
            
            // Init disposable resources
            using var cts = new CancellationTokenSource();
            await using var provider = new SdcProvider("urn:uuid:67e56745-5a1c-4799-9f75-e964bd925e5d");

            // Configure network
            provider.Settings.BindIp = MdpwsDeviceBindingSettings.Ipv4All;
            var httpConfig = provider.Settings.ServerConfig;
            httpConfig.UseHttps = true;
            httpConfig.Port = 6464;
            httpConfig.UseIpv6 = false;
            httpConfig.SslAuthenticationOptions = new SslAuthenticationOptions()
            {
                Protocols = SslProtocols.Tls13
            };
            httpConfig.SslAuthenticationOptions.Certificates.Add(HttpTools.ImportCertificate("SdcTestCert.p12", "1qayxsw2"));
            var clientConfig = provider.Settings.ClientConfig;
            clientConfig.UseHttps = true;
            clientConfig.SslAuthenticationOptions = new SslAuthenticationOptions()
            {
                Protocols = SslProtocols.Tls13
            };

            try
            {
                // Add description
                (MdsDescriptor MdsDescriptor, MdState StateContainer) contents = await GetMdibContents("mdib.xml", 
                    cts.Token).ConfigureAwait(false);
                provider.AddMds(contents.MdsDescriptor);
                // Add states
                foreach (var state in contents.StateContainer.State)
                {
                    provider.AddState(state);
                }
                
                // Handlers and simulators
                await Initialize(provider, cts).ConfigureAwait(false);
                
                // Start provider
                await provider.Startup().ConfigureAwait(false);
            }
            catch (Exception e)
            {
                await Console.Error.WriteLineAsync(e.StackTrace).ConfigureAwait(false);
            }
        
            // Wait for input
            Console.Read();
        
            // Cancel simulation
            cts.Cancel();
        }
        
        
        /// <summary>
        /// Parses mdib from file
        /// </summary>
        /// <param name="fileName"> The filename. </param>
        /// <param name="token"> A cancellation token. </param>
        /// <returns></returns>
        private static async Task<(MdsDescriptor Descriptor, MdState State)> GetMdibContents(string fileName,
            CancellationToken token)
        {
            var xmlText = await File.ReadAllTextAsync(fileName, token).ConfigureAwait(false);
            var mdib = GetMdibResponse.Deserialize(xmlText).Mdib;
            return (mdib.MdDescription.Mds.First(), mdib.MdState);
        }
        
    }
}