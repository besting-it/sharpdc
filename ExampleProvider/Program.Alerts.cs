﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SharpDC;
using SharpDC.Binding.Device;
using SharpDC.Provider;

namespace ExampleProvider
{
    partial class Program
    {

        /// <summary>
        /// Method for checking metric values against ranges defined in limit alerts and triggering alerts.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private static async Task HandleLimitAlerts(object sender, StatesUpdatedEventArgs args)
        {
            var provider = (ISdcEndpoint)sender;
            // Check if any numerical states have been updated. If not, we can skip entirely.
            var numericStates = args.States.OfType<NumericMetricState>().ToList();
            if (!numericStates.Any()) return;
            
            // Try to find referencing limit alert descriptors
            var alertDescriptors = await provider.GetAlertDescriptors().ConfigureAwait(false);
            var descriptors = alertDescriptors.ToList();
            var limits = descriptors.OfType<LimitAlertConditionDescriptor>().Where(d => d.Source.Any()).ToList();
            var signals = descriptors.OfType<AlertSignalDescriptor>().ToList();
            var joinedConditions = Enumerable.Empty<(NumericMetricState state, LimitAlertConditionDescriptor condition)>();
            var sources = limits.SelectMany(l => l.Source);
            foreach (var source in sources)
            {
                joinedConditions = joinedConditions.Concat(numericStates.Join(limits, state => state.DescriptorHandle, 
                    condition => source, (state, condition) => (state, condition)));
            }
            // Check limits of state value against resolved alert condition state and update condition presence and alarm signals, if needed
            foreach (var nextCond in joinedConditions)
            {
                await HandleCondition(nextCond, provider, signals).ConfigureAwait(false);
            }
        }

        private static async Task HandleCondition((NumericMetricState state, LimitAlertConditionDescriptor condition) condition,
            ISdcEndpoint provider, List<AlertSignalDescriptor> signals)
        {
            var val = condition.state.MetricValue.Value;
            var condState = await provider.GetState<LimitAlertConditionState>(condition.condition.Handle)
                .ConfigureAwait(false);
            var prevPresence = condState.Presence;
            // Update condition presence and referenced signals
            condState.Presence = (val < condState.Limits.Lower || val > condState.Limits.Upper);
            if (condState.Presence != prevPresence && condState.ActivationState == AlertActivation.On)
            {
                Console.WriteLine(
                    $@"Alert condition updated, metric {condition.state.DescriptorHandle}, previous presence flag: {prevPresence}, current presence flag: {condState.Presence}");
                await provider.UpdateState(condState).ConfigureAwait(false);
            }

            var refSignals = signals.Where(s => s.ConditionSignaled == condition.condition.Handle);
            foreach (var nextSignal in refSignals)
            {
                var signalState = await provider.GetState<AlertSignalState>(nextSignal.Handle)
                    .ConfigureAwait(false);
                var prevSigPresence = signalState.Presence;
                if (condState.Presence)
                {
                    switch (signalState.Presence)
                    {
                        case AlertSignalPresence.Off:
                            signalState.Presence = AlertSignalPresence.On;
                            break;
                        case AlertSignalPresence.On:
                            if (nextSignal.Latching)
                                signalState.Presence = AlertSignalPresence.Latch;
                            break;
                    }
                }
                else
                {
                    signalState.Presence = AlertSignalPresence.Off;
                }

                // Update alarm signal
                if (signalState.Presence != prevSigPresence && condState.ActivationState == AlertActivation.On)
                {
                    Console.WriteLine(
                        $@"Alert signal updated, previous presence flag: {prevSigPresence}, current presence flag: {signalState.Presence}");
                    await provider.UpdateState(signalState).ConfigureAwait(false);
                }
            }
        }   
        
    }
}