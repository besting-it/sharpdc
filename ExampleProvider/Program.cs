﻿// file:	Program.cs
//
// summary:	Implements the program class
// Copyright (C) Besting IT (info@besting-it.de) - Licensed under GNU GPL Version 3

using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SharpDC;
using SharpDC.Binding.Device.Mdpws;
using SharpDC.Provider;
using Range = SharpDC.Range;

namespace ExampleProvider
{
    /// <summary>   Example SpO2 simulator. </summary>
    partial class Program
    {
        /// <summary>   The handle metric spo2. </summary>
        private static string HandleMetricSpO2 = "spo2";
        /// <summary>   The handle metric pulse. </summary>
        private static string HandleMetricPulse = "pulse";
        
        /// <summary>   The handle spo2 condition. </summary>
        private static string HandleSpO2Condition = "spo2condition";        
        /// <summary>   The handle spo2 signal. </summary>
        private static string HandleSpO2Signal = "spo2signal";        
        /// <summary>   The handle spo2 alert system. </summary>
        private static string HandleSpO2Alert = "spo2alert";        
        

        /// <summary>   The random. </summary>
        private static Random rnd = new Random();
            
        /// <summary>   Main entry-point for this application. </summary>
        ///
        /// <param name="args"> An array of command-line argument strings. </param>
        ///
        /// <returns>   An asynchronous result. </returns>
        private static async Task Main(string[] args)
        {
            // Initialize
            SharpDc.Instance.Logger.Level = LogLevel.Trace;
            SharpDc.Instance.SetSchemaValidationMode(SchemaValidationMode.None);
            
            // Init disposable resources
            using var cts = new CancellationTokenSource();
            await using var provider = new SdcProvider("SharpDcPulsOxiDemo2022");

            provider.Settings.BindIp = MdpwsDeviceBindingSettings.Ipv4All;
            var httpConfig = provider.Settings.ServerConfig;
            httpConfig.Port = 6464;
            httpConfig.UseHttps = false;
            httpConfig.UseIpv6 = false;

            // Add description
            var mds = GetMds();
            provider.AddMds(mds);

            // Add states
            var stateSatO2 = SharpDcToolbox.CreateNumericState(HandleMetricSpO2, 100);
            provider.AddState(stateSatO2);
            var statePulse = SharpDcToolbox.CreateNumericState(HandleMetricPulse, 100);
            provider.AddState(statePulse);
            AddContainmentTreeStates(provider);
            AddAlertStates(provider);
            provider.StatesUpdated += HandleLimitAlerts;

            // Start provider
            await provider.Startup().ConfigureAwait(false);
        
            // Simulate values
            StartSimulation(provider, cts.Token, HandleMetricSpO2, 90, 100, 6000, 10000);
            StartSimulation(provider, cts.Token, HandleMetricPulse, 60, 80);

            // Wait for input
            Console.Read();
        
            // Cancel simulation
            cts.Cancel();
        }

        private static void AddAlertStates(SdcProvider provider)
        {
            var limitAlertConditionState = new LimitAlertConditionState()
            {
                DescriptorHandle = HandleSpO2Condition,
                Limits = new Range()
                {
                    Lower = 95,
                    Upper = 100
                },
                ActivationState = AlertActivation.On
            };
            provider.AddState(limitAlertConditionState);
            var alertSignalState = new AlertSignalState()
            {
                DescriptorHandle = HandleSpO2Signal,
                Presence = AlertSignalPresence.Off,
                ActivationState = AlertActivation.On
            };
            provider.AddState(alertSignalState);
            var alertSystemState = new AlertSystemState()
            {
                DescriptorHandle = HandleSpO2Alert,
                ActivationState = AlertActivation.On
            };
            provider.AddState(alertSystemState);
        }

        /// <summary>   Adds the containment tree states. </summary>
        ///
        /// <param name="provider"> The provider. </param>
        private static void AddContainmentTreeStates(SdcProvider provider)
        {
            var mdsState = new MdsState
            {
                DescriptorHandle = "mds", ActivationState = ComponentActivation.On, OperatingJurisdiction = null
            };
            provider.AddState(mdsState);
            var vmdState = new VmdState
            {
                DescriptorHandle = "vmd", ActivationState = ComponentActivation.On, OperatingJurisdiction = null
            };
            provider.AddState(vmdState);
            var chnSatO2State = new ChannelState
            {
                DescriptorHandle = "chnSatO2", ActivationState = ComponentActivation.On
            };
            provider.AddState(chnSatO2State);
            var chnPulseState = new ChannelState
            {
                DescriptorHandle = "chnPulse", ActivationState = ComponentActivation.On
            };
            provider.AddState(chnPulseState);
        }

        /// <summary>   Gets the mds. </summary>
        ///
        /// <returns>   The mds. </returns>
        private static MdsDescriptor GetMds()
        {
            var mds = new MdsDescriptor
            {
                Handle = "mds", Type = SharpDcToolbox.GenerateCodedValue(1, 4105, "MDC_DEV_ANALY_SAT_O2_MDS")
            };
            mds.MetaData.ModelName.Add(new LocalizedText() { Value = "SharpDcSpO2Demo", Lang = "en-US"} );
            mds.AlertSystem = null;
            var vmd = new VmdDescriptor
            {
                Handle = "vmd",
                Type = SharpDcToolbox.GenerateCodedValue(1, 4106, "MDC_DEV_ANALY_SAT_O2_VMD"),
                AlertSystem = CreateSatO2AlertSystem(),
                Sco = null
            };
            mds.Sco = null;
            mds.SystemContext = null;
            mds.Clock = null;

            var chnSatO2 = new ChannelDescriptor
            {
                Handle = "chnSatO2", Type = SharpDcToolbox.GenerateCodedValue(1, 4107, "MDC_DEV_ANALY_SAT_O2_CHAN")
            };
            var chnPulse = new ChannelDescriptor
            {
                Handle = "chnPulse", Type = SharpDcToolbox.GenerateCodedValue(1, 5139, "MDC_DEV_PULS_CHAN")
            };

            var nmdSatO2 = new NumericMetricDescriptor
            {
                Handle = HandleMetricSpO2,
                MetricCategory = MetricCategory.Msrmt,
                MetricAvailability = MetricAvailability.Cont,
                Type = SharpDcToolbox.GenerateCodedValue(2, 19384, "MDC_PULS_OXIM_SAT_O2"),
                Unit = SharpDcToolbox.GenerateCodedValue(4, 544, "MDC_DIM_PERCENT"),
                Resolution = Decimal.One
            };
            chnSatO2.Metric.Add(nmdSatO2);

            var nmdPulse = new NumericMetricDescriptor
            {
                MetricCategory = MetricCategory.Msrmt,
                MetricAvailability = MetricAvailability.Cont,
                Type = SharpDcToolbox.GenerateCodedValue(2, 18458, "MDC_PULS_OXIM_PULS_RATE"),
                Unit = SharpDcToolbox.GenerateCodedValue(4, 2720, "MDC_DIM_BEAT_PER_MIN"),
                Handle = HandleMetricPulse,
                Resolution = Decimal.One
            };
            chnPulse.Metric.Add(nmdPulse);

            vmd.Channel.Add(chnSatO2);
            vmd.Channel.Add(chnPulse);
            mds.Vmd.Add(vmd);
            return mds;
        }

        private static AlertSystemDescriptor CreateSatO2AlertSystem()
        {
            var limitAlertCondition = new LimitAlertConditionDescriptor()
            {
                Handle = HandleSpO2Condition,
                Type = SharpDcToolbox.GenerateCodedValue(3, 66, "MDC_EVT_LO_VAL_LT_LIM"),
                Kind = AlertConditionKind.Phy,
                Priority = AlertConditionPriority.Hi,
            };
            // Reference source descriptor handle of the value descriptor (SpO2 metric descriptor handle)
            limitAlertCondition.Source.Add(HandleMetricSpO2);
            var causeInfo = new CauseInfo
            {
                RemedyInfo = new RemedyInfo()
            };
            limitAlertCondition.CauseInfo.Add(causeInfo);
            limitAlertCondition.MaxLimits = new Range()
            {
                Lower = 0,
                Upper = 100
            };

            var limitAlertSignal = new AlertSignalDescriptor
            {
                Handle = HandleSpO2Signal,
                Type = SharpDcToolbox.GenerateCodedValue(3, 66, "MDC_EVT_LO_VAL_LT_LIM"),
                Manifestation = AlertSignalManifestation.Aud,
                Latching = true,
                // Reference the actual condition which eventually leads to this alarm signal
                ConditionSignaled = limitAlertCondition.Handle
            };

            var alertSystem = new AlertSystemDescriptor()
            {
                Handle = HandleSpO2Alert,
                Type = SharpDcToolbox.GenerateCodedValue(3),
            };
            alertSystem.AlertCondition.Add(limitAlertCondition);
            alertSystem.AlertSignal.Add(limitAlertSignal);
            return alertSystem;
        }
    }
}