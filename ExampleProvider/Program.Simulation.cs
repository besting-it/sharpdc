﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using SharpDC;
using SharpDC.Binding.Device;
using SharpDC.Provider;

namespace ExampleProvider
{
    partial class Program
    {

        /// <summary>   Starts a simulation. </summary>
        ///
        /// <param name="provider"> The provider. </param>
        /// <param name="token">    A token that allows processing to be cancelled. </param>
        /// <param name="handle">   The handle. </param>
        /// <param name="minValue"> The minimum value. </param>
        /// <param name="maxValue"> The maximum value. </param>
        /// <param name="minDelay"> (Optional) The minimum delay. </param>
        /// <param name="maxDelay"> (Optional) The maximum delay. </param>
        static async void StartSimulation(SdcProvider provider, CancellationToken token, 
            string handle, int minValue, int maxValue, int minDelay = 3000, int maxDelay = 5000)
        {
            double lastValue = rnd.Next(minValue, maxValue);
            try
            {
                while (true)
                {
                    await Task.Delay(rnd.Next(minDelay, maxDelay), token).ConfigureAwait(false);
                    token.ThrowIfCancellationRequested();
                    lastValue += rnd.NextDouble() >= 0.5 ? 1 : -1;
                    int value = (int)Math.Min(maxValue, Math.Max(lastValue, minValue));
                    Console.WriteLine($@"Setting value {value} for metric {handle}");
                    await provider.UpdateValue(handle, value).ConfigureAwait(false);
                    lastValue = value;
                }
            }
            catch (OperationCanceledException)
            {
            }
            catch (Exception e)
            {
                Console.WriteLine($@"Simulation stopped: {e.Message}");
            }
        }     
        
    }
}